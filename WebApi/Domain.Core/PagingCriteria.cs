﻿using Infraestructure.CrossCutting.Utilities.Enumerations;

namespace Domain.Core
{
    public class PagingCriteria
    {
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public string SortColumn { get; set; }
        public PagingDirectionEnum SortOrder { get; set; }
    }
}
