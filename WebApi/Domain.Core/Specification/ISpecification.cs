﻿using System;
using System.Linq.Expressions;

namespace Domain.Core.Specification
{
    public interface ISpecification<T>
        where T : class
    {
        Expression<Func<T, bool>> SatisfiedBy();
    }
}
