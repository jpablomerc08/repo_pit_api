﻿using System;
using System.Linq.Expressions;

namespace Domain.Core.Specification
{
    public sealed class TrueSpecification<T> : Specification<T>
        where T : class
    {
        public override Expression<Func<T, bool>> SatisfiedBy()
        {
            const bool result = true;

            Expression<Func<T, bool>> trueExpression = t => result;

            return trueExpression;
        }
    }
}
