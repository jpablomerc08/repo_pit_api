﻿using System;
using System.Linq.Expressions;

namespace Domain.Core.Specification
{
    public abstract class Specification<T> : ISpecification<T>
        where T : class
    {
        public abstract Expression<Func<T, bool>> SatisfiedBy();

        #region Override Operators
        public static Specification<T> operator &(Specification<T> leftSideSpecification, Specification<T> rightSideSpecification)
        {
            return new AndSpecification<T>(leftSideSpecification, rightSideSpecification);
        }

        public static Specification<T> operator |(Specification<T> leftSideSpecification, Specification<T> rightSideSpecification)
        {
            return new OrSpecification<T>(leftSideSpecification, rightSideSpecification);
        }

        public static Specification<T> operator !(Specification<T> originalSpecification)
        {
            return new NotSpecification<T>(originalSpecification);
        }

        public static bool operator false(Specification<T> specification)
        {
            return false;
        }

        public static bool operator true(Specification<T> specification)
        {
            return true;
        }
        #endregion
    }
}
