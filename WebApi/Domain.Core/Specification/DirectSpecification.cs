﻿using System;
using System.Linq.Expressions;

namespace Domain.Core.Specification
{
    public sealed class DirectSpecification<T> : Specification<T>
        where T : class
    {
        private readonly Expression<Func<T, bool>> matchingCriteria;

        public DirectSpecification(Expression<Func<T, bool>> matchingCriteria)
        {
            this.matchingCriteria = matchingCriteria;
        }

        public override Expression<Func<T, bool>> SatisfiedBy()
        {
            return matchingCriteria;
        }
    }
}
