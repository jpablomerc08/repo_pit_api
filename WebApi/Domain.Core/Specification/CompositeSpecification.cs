﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Core.Specification
{
    public abstract class CompositeSpecification<T> : Specification<T>
        where T : class
    {
        public abstract ISpecification<T> LeftSideSpecification { get; }

        public abstract ISpecification<T> RightSideSpecification { get; }
    }
}
