﻿using Application.MainModule.Contracts.Services.Security;
using AutoMapper;
using Domain.Core.Entities.Security;
using Infraestructure.CrossCutting.Dto.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Net.Mime;
using System.Threading.Tasks;
using WebApi.ViewModels.Security;

namespace WebApi.Controllers
{
    [Authorize(Roles ="Administrador")]
    [Route("api/[controller]")]
    [ApiController]
    public class PerfilesController : ControllerBase
    {
        private readonly IPerfilAppService perfilAppService;
        private readonly IMapper mapper;
        private readonly ILogger<PerfilesController> logger;

        public PerfilesController(IPerfilAppService perfilAppService,
                                  IMapper mapper,
                                  ILogger<PerfilesController> logger)
        {
            this.perfilAppService = perfilAppService;
            this.mapper = mapper;
            this.logger = logger;
        }

        // GET: api/Perfiles
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<IEnumerable<PerfilDto>>> Get()
        {
            try
            {
                return mapper.Map<List<PerfilDto>>(await perfilAppService.ListAllAsync());
            }
            catch (Exception ex)
            {
                logger.LogError($"Error al obtener los registros: {ex.Message}");

                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        // GET: api/Perfiles/5
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<PerfilDto>> Get(int id)
        {
            try
            {
                var entidad = await perfilAppService.FindAsync(id);

                if (entidad == null)
                {
                    return NotFound();
                }

                return mapper.Map<PerfilDto>(entidad);
            }
            catch (Exception ex)
            {
                logger.LogError($"Error al obtener el registro: {ex.Message}");

                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        // POST: api/Perfiles
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<PerfilDto>> Post(PerfilVM vm)
        {
            try
            {
                var entidad = mapper.Map<Perfil>(vm);

                await perfilAppService.AddAsync(entidad);

                var dto = mapper.Map<PerfilDto>(entidad);

                return CreatedAtAction(nameof(Get), new { id = dto.PerfilId }, dto);
            }
            catch (Exception ex)
            {
                logger.LogError($"Error al crear el registro: {ex.Message}");

                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        // PUT: api/Perfiles/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status304NotModified)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<PerfilDto>> Put(int id, PerfilVM vm)
        {
            if (id != vm.PerfilId)
            {
                logger.LogWarning($"No coincide el Id del parámetro con el modelo: {id}!={vm.PerfilId}");

                return BadRequest();
            }

            try
            {
                var entidad = await perfilAppService.FindAsync(id);

                if (entidad == null)
                    return NotFound();

                // Establecer campos editables
                entidad.Nombre = vm.Nombre;

                int r = await perfilAppService.UpdateAsync(entidad);
                if (r > 0)
                    return mapper.Map<PerfilDto>(entidad);

                return NoContent();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                if (! await Exists(id))
                {
                    logger.LogError($"Error de concurrencia, el registro ha sido eliminado después la última vez que se obtuvo con seguimiento de cambios: {ex.Message}");

                    return NotFound();
                }
                else
                {
                    logger.LogError($"Error de concurrencia, el registro ha sido modificado después la última vez que se obtuvo con seguimiento de cambios: {ex.Message}");

                    return StatusCode(StatusCodes.Status304NotModified);
                }
            }
            catch (Exception ex)
            {
                logger.LogError($"Error al actualizar el registro: {ex.Message}");

                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        // DELETE: api/Perfiles/5
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                var entidad = await perfilAppService.FindAsync(id);
                if (entidad == null)
                {
                    return NotFound();
                }

                await perfilAppService.DeleteAsync(entidad);

                return NoContent();
            }
            catch (Exception ex)
            {
                logger.LogError($"Error al eliminar el registro: {ex.Message}");

                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        private async Task<bool> Exists(int id)
        {
            return await perfilAppService.AnyAsync(e => e.PerfilId == id);
        }
    }
}
