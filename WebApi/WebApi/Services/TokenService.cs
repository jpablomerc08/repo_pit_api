﻿using Domain.Core.Entities.Security;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace WebApi.Services
{
    public class TokenService
    {
        public IConfiguration Configuration { get; }

        public TokenService(IConfiguration Configuration)
        {
            this.Configuration = Configuration;
        }

        public string Generar(Usuario usuario)
        {
            try
            {
                var jwtSettings = Configuration.GetSection("JwtSettings");
                var secretKey = jwtSettings.GetValue<string>("SecretKey");
                var minutes = jwtSettings.GetValue<int>("MinutesToExpiration");
                var issuer = jwtSettings.GetValue<string>("Issuer");
                var audience = jwtSettings.GetValue<string>("Audience");

                var key = Encoding.ASCII.GetBytes(secretKey);

                var claims = new List<Claim>();

                claims.Add(new Claim(ClaimTypes.Name, usuario.Username));
                claims.Add(new Claim(ClaimTypes.Email, usuario.Email));
                claims.Add(new Claim(ClaimTypes.Role, usuario.Perfil.Nombre));  

                var token = new JwtSecurityToken(
                    issuer: issuer,
                    audience: audience,
                    claims: claims,
                    notBefore: DateTime.UtcNow,
                    expires: DateTime.UtcNow.AddMinutes(minutes),
                    signingCredentials: new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256)
                );

                // Crear una representación en cadena del JWT
                string r = new JwtSecurityTokenHandler().WriteToken(token);

                return r;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
