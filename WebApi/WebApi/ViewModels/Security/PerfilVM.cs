﻿using System.ComponentModel.DataAnnotations;

namespace WebApi.ViewModels.Security
{
    public class PerfilVM
    {
        public int PerfilId { get; set; }

        [Required(ErrorMessage = "El campo \'{0}\' es requerido")]
        [MaxLength(length: 200, ErrorMessage = "\'{0}\' debe tener como máximo {1} caracteres")]
        public string Nombre { get; set; }
    }
}
