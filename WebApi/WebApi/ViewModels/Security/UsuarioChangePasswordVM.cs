﻿using System.ComponentModel.DataAnnotations;

namespace WebApi.ViewModels.Security
{
    public class UsuarioChangePasswordVM
    {
        public int UsuarioId { get; set; }

        [Required(ErrorMessage = "\'{0}\' es requerido")]
        [MinLength(6, ErrorMessage = "\'{0}\' debe tener al menos {1} caracteres")]
        public string Password { get; set; }
    }
}
