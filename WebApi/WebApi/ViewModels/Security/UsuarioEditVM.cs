﻿using System.ComponentModel.DataAnnotations;

namespace WebApi.ViewModels.Security
{
    public class UsuarioEditVM
    {
        public int UsuarioId { get; set; }

        [Required(ErrorMessage = "El campo \'{0}\' es requerido")]
        [MaxLength(length: 200, ErrorMessage = "\'{0}\' debe tener como máximo {1} caracteres")]
        public string Nombre { get; set; }

        [MaxLength(length: 200, ErrorMessage = "\'{0}\' debe tener como máximo {1} caracteres")]
        public string Apellidos { get; set; }

        [Required(ErrorMessage = "\'{0}\' es requerido")]
        [MaxLength(length: 200, ErrorMessage = "\'{0}\' debe tener como máximo {1} caracteres")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
    }
}
