﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebApi.ViewModels.Covid19
{
    public class CiudadanoTranVM
    {
        [Required(ErrorMessage = "El campo \'{0}\' es requerido")]
        public CiudadanoVM Ciudadano { get; set; }

        [Required(ErrorMessage = "El campo \'{0}\' es requerido")]
        public List<ContactoVM> Contactos { get; set; }

        [Required(ErrorMessage = "El campo \'{0}\' es requerido")]
        public List<DireccionVM> Direcciones { get; set; }

        [Required(ErrorMessage = "El campo \'{0}\' es requerido")]
        public List<SintomaVM> Sintomas { get; set; }

        [Required(ErrorMessage = "El campo \'{0}\' es requerido")]
        public List<SituacionRiesgoVM> SituacionesRiesgo { get; set; }
    }
}
