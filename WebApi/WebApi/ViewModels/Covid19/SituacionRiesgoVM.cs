﻿using System.ComponentModel.DataAnnotations;

namespace WebApi.ViewModels.Covid19
{
    public class SituacionRiesgoVM
    {
        [Required(ErrorMessage = "El campo \'{0}\' es requerido")]
        public int SituacionRiesgoId { get; set; }

        [Required(ErrorMessage = "El campo \'{0}\' es requerido")]
        public int CiudadanoId { get; set; }
    }
}
