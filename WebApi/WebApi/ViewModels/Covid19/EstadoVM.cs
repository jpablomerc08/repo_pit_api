﻿using System.ComponentModel.DataAnnotations;

namespace WebApi.ViewModels.Covid19
{
    public class EstadoVM
    {
        [Required(ErrorMessage = "El campo \'{0}\' es requerido")]
        public int EstadoId { get; set; }

        [Required(ErrorMessage = "El campo \'{0}\' es requerido")]
        [MaxLength(length: 50, ErrorMessage = "\'{0}\' debe tener como máximo {1} caracteres")]
        public string Nombre { get; set; }

        [MaxLength(length: 250, ErrorMessage = "\'{0}\' debe tener como máximo {1} caracteres")]
        public string Descripcion { get; set; }
    }
}
