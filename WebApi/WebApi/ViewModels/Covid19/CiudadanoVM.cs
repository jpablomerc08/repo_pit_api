﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebApi.ViewModels.Covid19
{
    public class CiudadanoVM
    {
        public int CiudadanoId { get; set; }

        [Required(ErrorMessage = "El campo \'{0}\' es requerido")]
        [MaxLength(length: 50, ErrorMessage = "\'{0}\' debe tener como máximo {1} caracteres")]
        public string ApellidoPaterno { get; set; }

        [MaxLength(length: 50, ErrorMessage = "\'{0}\' debe tener como máximo {1} caracteres")]
        public string ApellidoMaterno { get; set; }

        [Required(ErrorMessage = "El campo \'{0}\' es requerido")]
        [MaxLength(length: 50, ErrorMessage = "\'{0}\' debe tener como máximo {1} caracteres")]
        public string Nombre { get; set; }

        [Required(ErrorMessage = "El campo \'{0}\' es requerido")]
        public bool Sexo { get; set; }

        [Required(ErrorMessage = "El campo \'{0}\' es requerido")]
        public int NacionalidadId { get; set; }

        [Required(ErrorMessage = "El campo \'{0}\' es requerido")]
        public int TipoDocumentoIdentidadId { get; set; }

        [Required(ErrorMessage = "El campo \'{0}\' es requerido")]
        [MaxLength(length: 12, ErrorMessage = "\'{0}\' debe tener como máximo {1} caracteres")]
        public string NroDocumentoIdentidad { get; set; }

        [MaxLength(length: 9, ErrorMessage = "\'{0}\' debe tener como máximo {1} caracteres")]
        public string Celular { get; set; }

        [MaxLength(length: 100, ErrorMessage = "\'{0}\' debe tener como máximo {1} caracteres")]
        public string Correo { get; set; }

        [Required(ErrorMessage = "El campo \'{0}\' es requerido")]
        public DateTime FechaNacimiento { get; set; }

        [Required(ErrorMessage = "El campo \'{0}\' es requerido")]
        public int EstadoId { get; set; }
    }
}
