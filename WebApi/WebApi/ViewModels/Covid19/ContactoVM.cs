﻿using System.ComponentModel.DataAnnotations;

namespace WebApi.ViewModels.Covid19
{
    public class ContactoVM
    {
        public int ContactoId { get; set; }
        public int CiudadanoId { get; set; }

        [Required(ErrorMessage = "El campo \'{0}\' es requerido")]
        [MaxLength(length: 200, ErrorMessage = "\'{0}\' debe tener como máximo {1} caracteres")]
        public string NombreContacto { get; set; }

        [Required(ErrorMessage = "El campo \'{0}\' es requerido")]
        [MaxLength(length: 150, ErrorMessage = "\'{0}\' debe tener como máximo {1} caracteres")]
        public string Parentesco { get; set; }

        [MaxLength(length: 9, ErrorMessage = "\'{0}\' debe tener como máximo {1} caracteres")]
        public string Telefono { get; set; }

        [MaxLength(length: 100, ErrorMessage = "\'{0}\' debe tener como máximo {1} caracteres")]
        public string Correo { get; set; }
    }
}
