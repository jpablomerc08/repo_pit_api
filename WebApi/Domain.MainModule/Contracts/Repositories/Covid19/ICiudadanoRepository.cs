﻿using Domain.Core.Contracts.Repositories;
using Domain.Core.Entities.Covid19;
using Infraestructure.CrossCutting.Dto.Covid19.Criteria;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Domain.MainModule.Contracts.Repositories.Covid19
{
    public interface ICiudadanoRepository : IGenericRepository<Ciudadano>
    {
        Task<Ciudadano> ObtenerConDetalleAsync(int CiudadanoId);

        Task<List<Ciudadano>> FiltrarAsync(CiudadanoCriteria c);
    }
}
