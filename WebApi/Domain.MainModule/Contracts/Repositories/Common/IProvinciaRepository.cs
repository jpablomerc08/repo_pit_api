﻿using Domain.Core.Contracts.Repositories;
using Domain.Core.Entities.Common;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Domain.MainModule.Contracts.Repositories.Common
{
    public interface IProvinciaRepository : IGenericRepository<Provincia>
    {
        Task<List<Provincia>> ListarPorDepartamentoAsync(int DepartamentoId);
    }
}
