﻿using Domain.Core.Contracts.Repositories;
using Domain.Core.Entities.Security;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Domain.MainModule.Contracts.Repositories.Security
{
    public interface IUsuarioRepository : IGenericRepository<Usuario>
    {
        Task<IEnumerable<Usuario>> ListAllWithProfileAsync();

        Task<Usuario> FindWithProfileAsync(int Id);

        Task ChangePasswordAsync(Usuario usuario);
        
        Task ChangeProfileAsync(Usuario usuario);

        Task<(bool resultado, Usuario usuario)> CheckPasswordAsync(Usuario usuario);
    }
}
