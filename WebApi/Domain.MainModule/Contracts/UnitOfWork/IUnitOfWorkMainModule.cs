﻿using Domain.Core.Contracts.Repositories;
using Domain.Core.Contracts.UnitOfWork;
using Domain.Core.Entities.Common;
using Domain.Core.Entities.Covid19;
using Domain.Core.Entities.Security;

namespace Domain.MainModule.Contracts.UnitOfWork
{
    public interface IUnitOfWorkMainModule : IUnitOfWork
    {
        #region Security
        IGenericRepository<Perfil> PerfilRepository { get; }

        IGenericRepository<Usuario> UsuarioRepository { get; }
        #endregion

        #region Common
        IGenericRepository<Pais> PaisRepository { get; }

        IGenericRepository<TipoDocumentoIdentidad> TipoDocumentoIdentidadRepository { get; }

        IGenericRepository<Departamento> DepartamentoRepository { get; }

        IGenericRepository<Provincia> ProvinciaRepository { get; }

        IGenericRepository<Distrito> DistritoRepository { get; }
        #endregion

        #region Covid
        IGenericRepository<Ciudadano> CiudadanoRepository { get; }
        #endregion
    }
}
