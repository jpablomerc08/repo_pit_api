﻿using Domain.Core.Contracts.UnitOfWork;

namespace Domain.MainModule.Contracts.UnitOfWork
{
    public interface IUnitOfWorkMainModuleFactory : IUnitOfWorkFactory
    {
    }
}
