﻿using Domain.Core.Contracts.Services;
using Domain.Core.Entities.Covid19;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Domain.MainModule.Contracts.Services.Covid19
{
    public interface IContactoService : IGenericService<Contacto>
    {
        Task<List<Contacto>> ListarPorCiudadanoAsyc(int CiudadanoId);
    }
}
