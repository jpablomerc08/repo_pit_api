﻿using Domain.Core.Contracts.Services;
using Domain.Core.Entities.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.MainModule.Contracts.Services.Common
{
    public interface ITipoDocumentoIdentidadService : IGenericService<TipoDocumentoIdentidad>
    {
    }
}
