﻿using Domain.Core.Contracts.Services;
using Domain.Core.Entities.Common;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Domain.MainModule.Contracts.Services.Common
{
    public interface IDistritoService : IGenericService<Distrito>
    {
        Task<List<Distrito>> ListarPorProvincia(int ProvinciaId);
    }
}
