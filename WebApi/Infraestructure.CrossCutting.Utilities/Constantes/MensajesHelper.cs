﻿namespace Infraestructure.CrossCutting.Utilities.Constantes
{
    public class MensajesHelper
    {
        public static int CodigoRegistroCorrecto = 0;
        public static int CodigoRegistroIncorrecto = 1;

        public static string MensajeRegistroCorrecto = "Se registró correctamente";
        public static string MensajeActualizaCorrecto = "Se actualizó correctamente";
        public static string MensajeEliminaCorrecto = "Se eliminó correctamente";

        public static string danger = "danger";
        public static string sucess = "success";
        public static string warning = "warning";
        public static string info = "info";
        public static string primary = "primary";
        public static string dark = "dark";
    }
}
