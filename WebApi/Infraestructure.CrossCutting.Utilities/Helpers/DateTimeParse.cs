﻿using System;
using System.Globalization;

namespace Infraestructure.CrossCutting.Utilities.Helpers
{
    public class DateTimeParse
    {
        public static string DateTimeToString(DateTime fecha, string formato)
        {
            CultureInfo provider = CultureInfo.CurrentCulture;

            return fecha.ToString(formato, provider);
        }

        public static DateTime StringToDateTime(string sFecha, string formato)
        {
            CultureInfo provider = CultureInfo.CurrentCulture;

            return DateTime.ParseExact(sFecha, formato, provider);
        }
    }
}
