﻿namespace Infraestructure.CrossCutting.Utilities.Enumerations
{
    public enum EstadoOrdenEnum
    {
        PENDIENTE = 1,
        PROCESO = 2,
        CERRADA = 3
    }
}
