﻿namespace Infraestructure.CrossCutting.Utilities.Enumerations
{
    public enum EstadosRegistroEnum
    {
        Activo = 1,
        Eliminado = 0
    }
}
