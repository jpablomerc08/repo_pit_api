﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infraestructure.CrossCutting.Utilities.Enumerations
{
    public enum EstadoUsuarioEnum
    {
        ACTIVO = 1,
        INACTIVO = 2,
        BLOQUEADO = 3
    }
}
