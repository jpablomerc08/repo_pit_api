﻿using Domain.Core.Entities.Covid19;
using Domain.MainModule.Contracts.Repositories.Covid19;
using Infraestructure.Data.Core.Contracts;
using Infraestructure.Data.Core.Repositories;
using Microsoft.EntityFrameworkCore;
using NetTopologySuite.Geometries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Infraestructure.Data.MainModule.Repositories.Covid19
{
    public class DireccionRepository : GenericRepository<Direccion>, IDireccionRepository
    {
        private readonly IDatabaseContext databaseContext;

        public DireccionRepository(IDatabaseContext databaseContext) : base(databaseContext)
        {
            this.databaseContext = databaseContext;
        }

        public async Task<List<Direccion>> ListarPorCiudadanoAsync(int CiudadanoId)
        {
            try
            {
                return await base.QueryableAll()
                    .Where(t => t.CiudadanoId == CiudadanoId)
                    .ToListAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<List<Direccion>> ConfirmadosAsync(Point ubicacion)
        {
            try
            {
                var ciudadano = databaseContext.Set<Ciudadano>();
                var direccion = databaseContext.Set<Direccion>();

                return await (from d in direccion
                                join c in ciudadano on d.CiudadanoId equals c.CiudadanoId
                                where
                                   (ubicacion == null || d.Ubicacion.IsWithinDistance(ubicacion, 400)) // Al menos 400 metros de distancia
                                   && c.EstadoId == 3   // Solo casos confirmados
                                select d).ToListAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
