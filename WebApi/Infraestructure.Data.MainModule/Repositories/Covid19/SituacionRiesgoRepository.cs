﻿using Domain.Core.Entities.Covid19;
using Domain.MainModule.Contracts.Repositories.Covid19;
using Infraestructure.Data.Core.Contracts;
using Infraestructure.Data.Core.Repositories;

namespace Infraestructure.Data.MainModule.Repositories.Covid19
{
    public class SituacionRiesgoRepository : GenericRepository<SituacionRiesgo>,ISituacionRiesgoRepository
    {
        public SituacionRiesgoRepository(IDatabaseContext databaseContext) : base(databaseContext)
        {
        }
    }
}
