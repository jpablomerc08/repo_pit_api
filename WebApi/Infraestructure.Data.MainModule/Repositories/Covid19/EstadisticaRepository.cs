﻿using Domain.Core.Entities.Covid19;
using Domain.MainModule.Contracts.Repositories.Covid19;
using Infraestructure.CrossCutting.Dto.Covid19;
using Infraestructure.CrossCutting.Dto.Covid19.Criteria;
using Infraestructure.Data.Core.Contracts;
using Infraestructure.Data.Core.Repositories;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;

namespace Infraestructure.Data.MainModule.Repositories.Covid19
{
    public class EstadisticaRepository : GenericRepository<Ciudadano>, IEstadisticaRepository
    {
        private readonly IDatabaseContext databaseContext;

        public EstadisticaRepository(IDatabaseContext databaseContext) : base(databaseContext)
        {
            this.databaseContext = databaseContext;
        }

        public async Task<EstadisticaDto> FiltrarAsync(EstadisticaCriteria c)
        {
            try
            {
                EstadisticaDto dto = null;

                using (databaseContext)
                {
                    using (var conexion = databaseContext.Database.GetDbConnection())
                    {
                        using (var comando = databaseContext.Database.GetDbConnection().CreateCommand())
                        {
                            comando.CommandText = "covid.USP_ESTADISTICAS";
                            comando.CommandType = System.Data.CommandType.StoredProcedure;

                            comando.Parameters.Add(new SqlParameter("@PI_FECHA", c.Fecha)); // Fecha obligatoria
                            if (c.DepartamentoId != null)
                                comando.Parameters.Add(new SqlParameter("@PI_DEPARTAMENTO_ID", c.DepartamentoId));
                            if (c.ProvinciaId != null)
                                comando.Parameters.Add(new SqlParameter("@PI_PROVINCIA_ID", c.ProvinciaId));
                            if (c.DistritoId != null)
                                comando.Parameters.Add(new SqlParameter("@PI_DISTRITO_ID", c.DistritoId));

                            if (conexion.State == System.Data.ConnectionState.Closed)
                                conexion.Open();

                            using (var lector = await comando.ExecuteReaderAsync())
                            {
                                while (await lector.ReadAsync())
                                {
                                    dto = new EstadisticaDto
                                    {
                                        Nuevos = int.Parse(lector["Nuevos"].ToString()),
                                        Confirmados = int.Parse(lector["Confirmados"].ToString()),
                                        Recuperados = int.Parse(lector["Recuperados"].ToString()),
                                        Fallecidos = int.Parse(lector["Fallecidos"].ToString())
                                    };
                                }
                            }
                        }
                    }
                }

                return dto;

                /*List<SqlParameter> parameters = new List<SqlParameter>();
                
                if (c.Fecha != null)
                    parameters.Add(new SqlParameter("@PI_FECHA", c.Fecha));
                if (c.DepartamentoId != null)
                    parameters.Add(new SqlParameter("@PI_DEPARTAMENTO_ID", c.DepartamentoId));
                if (c.ProvinciaId != null)
                    parameters.Add(new SqlParameter("@PI_PROVINCIA_ID", c.ProvinciaId));
                if (c.DistritoId != null)
                    parameters.Add(new SqlParameter("@PI_DISTRITO_ID", c.DistritoId));

               var t = databaseContext.RawSqlQuery()

                var r = await databaseContext.Set<Ciudadano>()
                            .FromSqlRaw("EXECUTE covid.USP_ESTADISTICAS", parameters.ToArray())
                            .IgnoreQueryFilters()
                            .ToListAsync();

                var dto = new EstadisticaDto();*/
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
