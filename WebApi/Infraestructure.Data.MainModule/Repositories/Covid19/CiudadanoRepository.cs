﻿using Domain.Core.Entities.Common;
using Domain.Core.Entities.Covid19;
using Domain.MainModule.Contracts.Repositories.Covid19;
using Infraestructure.CrossCutting.Dto.Covid19;
using Infraestructure.CrossCutting.Dto.Covid19.Criteria;
using Infraestructure.Data.Core.Contracts;
using Infraestructure.Data.Core.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Infraestructure.Data.MainModule.Repositories.Covid19
{
    public class CiudadanoRepository : GenericRepository<Ciudadano>, ICiudadanoRepository
    {
        private readonly IDatabaseContext databaseContext;

        public CiudadanoRepository(IDatabaseContext databaseContext) : base(databaseContext)
        {
            this.databaseContext = databaseContext;
        }

        public async Task<List<Ciudadano>> FiltrarAsync(CiudadanoCriteria c)
        {
            try
            {
                var ciudadano = databaseContext.Set<Ciudadano>();
                var tipoDocumento = databaseContext.Set<TipoDocumentoIdentidad>();
                var estado = databaseContext.Set<Estado>();

                return await ciudadano.AsQueryable()
                                .Where(t => (c.Nombre == null || (t.Nombre + " " + t.ApellidoPaterno + (t.ApellidoMaterno ?? "")).Trim().Contains(c.Nombre))
                                            && (c.NroDocumentoIdentidad == null || t.NroDocumentoIdentidad.Contains(c.NroDocumentoIdentidad))
                                            && (c.EstadoId == null || t.EstadoId == c.EstadoId))
                                .Include(t => t.TipoDocumentoIdentidad)
                                .Include(t => t.Estado)
                                .ToListAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<Ciudadano> ObtenerConDetalleAsync(int CiudadanoId)
        {
            try
            {
                return await base.QueryableFiltered(t => t.CiudadanoId == CiudadanoId)
                            .Include(t => t.Contactos)
                            .Include(t => t.Direcciones).ThenInclude(t => t.Distrito).ThenInclude(t => t.Provincia).ThenInclude(t => t.Departamento)
                            .Include(t => t.Sintomas)
                            .Include(t => t.SituacionesRiesgo)
                            .FirstOrDefaultAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
