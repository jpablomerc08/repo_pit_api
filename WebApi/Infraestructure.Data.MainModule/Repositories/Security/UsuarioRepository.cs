﻿using Domain.Core.Entities.Security;
using Domain.MainModule.Contracts.Repositories.Security;
using Infraestructure.CrossCutting.Utilities.Enumerations;
using Infraestructure.Data.Core.Contracts;
using Infraestructure.Data.Core.Repositories;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Infraestructure.Data.MainModule.Repositories.Security
{
    public class UsuarioRepository : GenericRepository<Usuario>, IUsuarioRepository
    {
        private readonly IPasswordHasher<Usuario> passwordHasher;

        public UsuarioRepository(IDatabaseContext databaseContext,
                                 IPasswordHasher<Usuario> passwordHasher) : base(databaseContext)
        {
            this.passwordHasher = passwordHasher;
        }

        public override Usuario Add(Usuario item)
        {
            item.Estado = EstadoUsuarioEnum.ACTIVO;
            item.Password = passwordHasher.HashPassword(item, item.Password);

            return base.Add(item);
        }

        public async Task<IEnumerable<Usuario>> ListAllWithProfileAsync()
        {
            return await base.QueryableAll()
                    .Include(t => t.Perfil)
                    .AsNoTracking()
                    .ToListAsync();
        }

        public async Task<Usuario> FindWithProfileAsync(int Id)
        {
            return await base.QueryableAll()
                         .Include(t => t.Perfil)
                         .AsNoTracking()
                         .SingleOrDefaultAsync(t => t.UsuarioId == Id);
        }

        public async Task ChangePasswordAsync(Usuario usuario)
        {
            var entidad = await FindAsync(usuario.UsuarioId);

            entidad.Password = passwordHasher.HashPassword(entidad, usuario.Password);

            base.Update(entidad);
        }

        public async Task ChangeProfileAsync(Usuario usuario)
        {
            var entidad = await FindAsync(usuario.UsuarioId);

            entidad.PerfilId = usuario.PerfilId;

            base.Update(entidad);
        }

        public async Task<(bool resultado, Usuario usuario)> CheckPasswordAsync(Usuario usuario)
        {
            var entidad = await base.QueryableAll()
                                    .Include(t => t.Perfil)
                                    .FirstOrDefaultAsync(t => t.Username == usuario.Username);

            //var entidad = await FirstOrDefaultAsync(t => t.Username == usuario.Username);

            if (entidad != null)
            {
                var resultado = passwordHasher.VerifyHashedPassword(entidad, entidad.Password, usuario.Password);

                return (resultado == PasswordVerificationResult.Success, entidad);
            }

            return (false, null);
        }
    }
}
