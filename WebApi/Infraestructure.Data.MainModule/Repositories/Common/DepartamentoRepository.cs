﻿using Domain.Core.Entities.Common;
using Domain.MainModule.Contracts.Repositories.Common;
using Infraestructure.Data.Core.Contracts;
using Infraestructure.Data.Core.Repositories;

namespace Infraestructure.Data.MainModule.Repositories.Common
{
    public class DepartamentoRepository : GenericRepository<Departamento>, IDepartamentoRepository
    {
        public DepartamentoRepository(IDatabaseContext databaseContext) : base(databaseContext)
        {
        }
    }
}
