﻿using Domain.Core.Entities.Common;
using Domain.MainModule.Contracts.Repositories.Common;
using Infraestructure.Data.Core.Contracts;
using Infraestructure.Data.Core.Repositories;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Infraestructure.Data.MainModule.Repositories.Common
{
    public class ProvinciaRepository : GenericRepository<Provincia>, IProvinciaRepository
    {
        public ProvinciaRepository(IDatabaseContext databaseContext) : base(databaseContext)
        {
        }

        public async Task<List<Provincia>> ListarPorDepartamentoAsync(int DepartamentoId)
        {
            return await base.QueryableAll()
                    .Where(t => t.DepartamentoId == DepartamentoId)
                    .ToListAsync();
        }
    }
}
