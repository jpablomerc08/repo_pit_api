﻿using Domain.Core.Entities.Common;
using Domain.MainModule.Contracts.Repositories.Common;
using Infraestructure.Data.Core.Contracts;
using Infraestructure.Data.Core.Repositories;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Infraestructure.Data.MainModule.Repositories.Common
{
    public class DistritoRepository : GenericRepository<Distrito>, IDistritoRepository
    {
        public DistritoRepository(IDatabaseContext databaseContext) : base(databaseContext)
        {
        }

        public async Task<List<Distrito>> ListarPorProvincia(int ProvinciaId)
        {
            return await base.QueryableAll()
                        .Where(t => t.ProvinciaId == ProvinciaId)
                        .ToListAsync();
        }
    }
}
