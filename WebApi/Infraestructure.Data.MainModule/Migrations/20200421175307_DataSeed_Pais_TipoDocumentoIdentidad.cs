﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infraestructure.Data.MainModule.Migrations
{
    public partial class DataSeed_Pais_TipoDocumentoIdentidad : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                schema: "common",
                table: "Pais",
                columns: new[] { "PaisId", "CodigoISO", "Eliminado", "Nombre" },
                values: new object[,]
                {
                    { 1, "AF", false, "Afganistán" },
                    { 157, "MS", false, "Montserrat" },
                    { 158, "MZ", false, "Mozambique" },
                    { 159, "MM", false, "Myanmar" },
                    { 160, "NA", false, "Namibia" },
                    { 161, "NR", false, "Nauru" },
                    { 162, "NP", false, "Nepal" },
                    { 163, "NI", false, "Nicaragua" },
                    { 164, "NE", false, "Níger" },
                    { 165, "NG", false, "Nigeria" },
                    { 166, "NU", false, "Niue" },
                    { 167, "NO", false, "Noruega" },
                    { 168, "NC", false, "Nueva Caledonia" },
                    { 156, "MN", false, "Mongolia" },
                    { 169, "NZ", false, "Nueva Zelanda" },
                    { 171, "PK", false, "Pakistán" },
                    { 172, "PW", false, "Palau" },
                    { 173, "PS", false, "Palestina" },
                    { 174, "PA", false, "Panamá" },
                    { 175, "PG", false, "Papúa Nueva Guinea" },
                    { 176, "PY", false, "Paraguay" },
                    { 177, "PE", false, "Perú" },
                    { 178, "PF", false, "Polinesia Francesa" },
                    { 179, "PL", false, "Polonia" },
                    { 180, "PT", false, "Portugal" },
                    { 181, "PR", false, "Puerto Rico" },
                    { 182, "QA", false, "Qatar" },
                    { 170, "OM", false, "Omán" },
                    { 155, "MC", false, "Mónaco" },
                    { 154, "MD", false, "Moldavia" },
                    { 153, "FM", false, "Micronesia" },
                    { 126, "KZ", false, "Kazajstán" },
                    { 127, "KE", false, "Kenia" },
                    { 128, "KG", false, "Kirguistán" },
                    { 129, "KI", false, "Kiribati" },
                    { 130, "KW", false, "Kuwait" },
                    { 131, "LA", false, "Laos" },
                    { 132, "LS", false, "Lesotho" },
                    { 133, "LV", false, "Letonia" },
                    { 134, "LB", false, "Líbano" },
                    { 135, "LR", false, "Liberia" },
                    { 136, "LY", false, "Libia" },
                    { 137, "LI", false, "Liechtenstein" },
                    { 138, "LT", false, "Lituania" },
                    { 139, "LU", false, "Luxemburgo" },
                    { 140, "MO", false, "Macao" },
                    { 141, "MG", false, "Madagascar" },
                    { 142, "MY", false, "Malasia" },
                    { 143, "MW", false, "Malawi" },
                    { 144, "MV", false, "Maldivas" },
                    { 145, "ML", false, "Malí" },
                    { 146, "MT", false, "Malta" },
                    { 147, "MA", false, "Marruecos" },
                    { 148, "MQ", false, "Martinica" },
                    { 149, "MU", false, "Mauricio" },
                    { 150, "MR", false, "Mauritania" },
                    { 151, "YT", false, "Mayotte" },
                    { 152, "MX", false, "México" },
                    { 183, "GB", false, "Reino Unido" },
                    { 125, "JO", false, "Jordania" },
                    { 184, "CF", false, "República Centroafricana" },
                    { 186, "CD", false, "República Democrática del Congo" },
                    { 218, "TW", false, "Taiwán" },
                    { 219, "TZ", false, "Tanzania" },
                    { 220, "TJ", false, "Tayikistán" },
                    { 221, "IO", false, "Territorio Británico del Océano Índico" },
                    { 222, "TF", false, "Territorios Australes Franceses" },
                    { 223, "TL", false, "Timor Oriental" },
                    { 224, "TG", false, "Togo" },
                    { 225, "TK", false, "Tokelau" },
                    { 226, "TO", false, "Tonga" },
                    { 227, "TT", false, "Trinidad y Tobago" },
                    { 228, "TN", false, "Túnez" },
                    { 229, "TM", false, "Turkmenistán" },
                    { 217, "TH", false, "Tailandia" },
                    { 230, "TR", false, "Turquía" },
                    { 232, "UA", false, "Ucrania" },
                    { 233, "UG", false, "Uganda" },
                    { 234, "UY", false, "Uruguay" },
                    { 235, "UZ", false, "Uzbekistán" },
                    { 236, "VU", false, "Vanuatu" },
                    { 237, "VE", false, "Venezuela" },
                    { 238, "VN", false, "Vietnam" },
                    { 239, "WF", false, "Wallis y Futuna" },
                    { 240, "YE", false, "Yemen" },
                    { 241, "DJ", false, "Yibuti" },
                    { 242, "ZM", false, "Zambia" },
                    { 243, "ZW", false, "Zimbawe" },
                    { 231, "TV", false, "Tuvalu" },
                    { 216, "SJ", false, "Svalbard y Jan Mayen" },
                    { 215, "SR", false, "Surinam" },
                    { 214, "CH", false, "Suiza" },
                    { 187, "DO", false, "República Dominicana" },
                    { 188, "RE", false, "Reunión" },
                    { 189, "RW", false, "Ruanda" },
                    { 190, "RO", false, "Rumania" },
                    { 191, "RU", false, "Rusia" },
                    { 192, "EH", false, "Sahara Occidental" },
                    { 193, "WS", false, "Samoa" },
                    { 194, "AS", false, "Samoa Americana" },
                    { 195, "KN", false, "San Cristóbal y Nevis" },
                    { 196, "SM", false, "San Marino" },
                    { 197, "PM", false, "San Pedro y Miquelón" },
                    { 198, "VC", false, "San Vicente y las Granadinas" },
                    { 199, "SH", false, "Santa Helena" },
                    { 200, "LC", false, "Santa Lucía" },
                    { 201, "ST", false, "Santo Tomé y Príncipe" },
                    { 202, "SN", false, "Senegal" },
                    { 203, "CS", false, "Serbia y Montenegro" },
                    { 204, "SC", false, "Seychelles" },
                    { 205, "SL", false, "Sierra Leona" },
                    { 206, "SG", false, "Singapur" },
                    { 207, "SY", false, "Siria" },
                    { 208, "SO", false, "Somalia" },
                    { 209, "LK", false, "Sri Lanka" },
                    { 210, "SZ", false, "Suazilandia" },
                    { 211, "ZA", false, "Sudáfrica" },
                    { 212, "SD", false, "Sudán" },
                    { 213, "SE", false, "Suecia" },
                    { 185, "CZ", false, "República Checa" },
                    { 123, "JP", false, "Japón" },
                    { 124, "JE", false, "Jersey" },
                    { 121, "IT", false, "Italia" },
                    { 33, "BN", false, "Brunéi" },
                    { 34, "BG", false, "Bulgaria" },
                    { 35, "BF", false, "Burkina Faso" },
                    { 36, "BI", false, "Burundi" },
                    { 37, "CV", false, "Cabo Verde" },
                    { 38, "KH", false, "Camboya" },
                    { 39, "CM", false, "Camerún" },
                    { 40, "CA", false, "Canadá" },
                    { 41, "TD", false, "Chad" },
                    { 42, "CL", false, "Chile" },
                    { 43, "CN", false, "China" },
                    { 44, "CY", false, "Chipre" },
                    { 45, "VA", false, "Ciudad del Vaticano" },
                    { 46, "CO", false, "Colombia" },
                    { 47, "KM", false, "Comoras" },
                    { 48, "CG", false, "Congo" },
                    { 49, "KP", false, "Corea del Norte" },
                    { 50, "KR", false, "Corea del Sur" },
                    { 51, "CI", false, "Costa de Marfil" },
                    { 52, "CR", false, "Costa Rica" },
                    { 53, "HR", false, "Croacia" },
                    { 54, "CU", false, "Cuba" },
                    { 55, "DK", false, "Dinamarca" },
                    { 56, "DM", false, "Dominica" },
                    { 57, "EC", false, "Ecuador" },
                    { 58, "EG", false, "Egipto" },
                    { 59, "SV", false, "El Salvador" },
                    { 32, "BR", false, "Brasil" },
                    { 122, "JM", false, "Jamaica" },
                    { 31, "BW", false, "Botsuana" },
                    { 29, "BO", false, "Bolivia" },
                    { 2, "AL", false, "Albania" },
                    { 3, "DE", false, "Alemania" },
                    { 4, "AD", false, "Andorra" },
                    { 5, "AO", false, "Angola" },
                    { 6, "AI", false, "Anguilla" },
                    { 7, "AQ", false, "Antártida" },
                    { 8, "AG", false, "Antigua y Barbuda" },
                    { 9, "AN", false, "Antillas Holandesas" },
                    { 10, "SA", false, "Arabia Saudí" },
                    { 11, "DZ", false, "Argelia" },
                    { 12, "AR", false, "Argentina" },
                    { 13, "AM", false, "Armenia" },
                    { 14, "AW", false, "Aruba" },
                    { 15, "MK", false, "ARY Macedonia" },
                    { 16, "AU", false, "Australia" },
                    { 17, "AT", false, "Austria" },
                    { 18, "AZ", false, "Azerbaiyán" },
                    { 19, "BS", false, "Bahamas" },
                    { 20, "BH", false, "Bahréin" },
                    { 21, "BD", false, "Bangladesh" },
                    { 22, "BB", false, "Barbados" },
                    { 23, "BE", false, "Bélgica" },
                    { 24, "BZ", false, "Belice" },
                    { 25, "BJ", false, "Benin" },
                    { 26, "BM", false, "Bermudas" },
                    { 27, "BT", false, "Bhután" },
                    { 28, "BY", false, "Bielorrusia" },
                    { 30, "BA", false, "Bosnia y Herzegovina" },
                    { 61, "ER", false, "Eritrea" },
                    { 60, "AE", false, "Emiratos Árabes Unidos" },
                    { 63, "SI", false, "Eslovenia" },
                    { 95, "ID", false, "Indonesia" },
                    { 96, "IR", false, "Irán" },
                    { 97, "IQ", false, "Iraq" },
                    { 98, "IE", false, "Irlanda" },
                    { 99, "BV", false, "Isla Bouvet" },
                    { 100, "IM", false, "Isla de Man" },
                    { 101, "CX", false, "Isla de Navidad" },
                    { 102, "NF", false, "Isla Norfolk" },
                    { 103, "IS", false, "Islandia" },
                    { 104, "AX", false, "Islas Aland" },
                    { 105, "KY", false, "Islas Caimán" },
                    { 106, "CC", false, "Islas Cocos" },
                    { 62, "SK", false, "Eslovaquia" },
                    { 107, "CK", false, "Islas Cook" },
                    { 109, "GS", false, "Islas Georgias del Sur y Sandwich del Sur" },
                    { 110, "HM", false, "Islas Heard y McDonald" },
                    { 111, "FK", false, "Islas Malvinas" },
                    { 112, "MP", false, "Islas Marianas del Norte" },
                    { 113, "MH", false, "Islas Marshall" },
                    { 114, "PN", false, "Islas Pitcairn" },
                    { 115, "SB", false, "Islas Salomón" },
                    { 116, "TC", false, "Islas Turcas y Caicos" },
                    { 117, "UM", false, "Islas ultramarinas de Estados Unidos" },
                    { 118, "VG", false, "Islas Vírgenes Británicas" },
                    { 119, "VI", false, "Islas Vírgenes de los Estados Unidos" },
                    { 120, "IL", false, "Israel" },
                    { 108, "FO", false, "Islas Feroe" },
                    { 93, "HU", false, "Hungría" },
                    { 94, "IN", false, "India" },
                    { 91, "HN", false, "Honduras" },
                    { 64, "ES", false, "España" },
                    { 65, "US", false, "Estados Unidos" },
                    { 66, "EE", false, "Estonia" },
                    { 67, "ET", false, "Etiopía" },
                    { 68, "PH", false, "Filipinas" },
                    { 69, "FI", false, "Finlandia" },
                    { 70, "FJ", false, "Fiyi" },
                    { 71, "FR", false, "Francia" },
                    { 72, "GA", false, "Gabón" },
                    { 73, "GM", false, "Gambia" },
                    { 74, "GE", false, "Georgia" },
                    { 92, "HK", false, "Hong Kong" },
                    { 76, "GI", false, "Gibraltar" },
                    { 75, "GH", false, "Ghana" },
                    { 78, "GR", false, "Grecia" },
                    { 90, "NL", false, "Holanda o Países Bajos" },
                    { 89, "HT", false, "Haití" },
                    { 88, "GY", false, "Guyana" },
                    { 77, "GD", false, "Granada" },
                    { 86, "GQ", false, "Guinea Ecuatorial" },
                    { 85, "GN", false, "Guinea" },
                    { 87, "GW", false, "Guinea-Bissau" },
                    { 83, "GF", false, "Guayana Francesa" },
                    { 82, "GT", false, "Guatemala" },
                    { 81, "GU", false, "Guam" },
                    { 80, "GP", false, "Guadalupe" },
                    { 79, "GL", false, "Groenlandia" },
                    { 84, "GG", false, "Guernesey" }
                });

            migrationBuilder.InsertData(
                schema: "common",
                table: "TipoDocumento",
                columns: new[] { "TipoDocumentoIdentidadId", "DescripcionCorta", "DescripcionLarga", "Eliminado" },
                values: new object[,]
                {
                    { 1, "L.E / DNI", "LIBRETA ELECTORAL O DNI", false },
                    { 2, "CARNET EXT.", "CARNET DE EXTRANJERIA", false },
                    { 3, "PASAPORTE", "PASAPORTE", false }
                });

            migrationBuilder.InsertData(
                schema: "seguridad",
                table: "Perfil",
                columns: new[] { "PerfilId", "Audit_FechaActualizacion", "Audit_UsuarioActualizacion", "Eliminado", "Nombre" },
                values: new object[,]
                {
                    { 1, null, null, false, "Administrador" },
                    { 2, null, null, false, "Usuario" }
                });

            migrationBuilder.InsertData(
                schema: "seguridad",
                table: "Usuario",
                columns: new[] { "PerfilId", "ApellidoMaterno", "ApellidoPaterno", "Audit_FechaActualizacion", "Audit_UsuarioActualizacion", "Eliminado", "Email", "Estado", "Nombre", "Password", "Telefono", "Username", "UsuarioId" },
                values: new object[] { 1, "Alarcón", "Aldaz", null, null, false, "eduar2083@gmail.com", 1, "Edinson", "AQAAAAEAACcQAAAAEKwW3DjQgKbTDRIvgWMWqmuGfF3zOjcoOv+2bYWfh++g1FrDJbjOyRWdXjVzYRBSgw==", "999888777", "admin", 1 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 1);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 2);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 3);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 4);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 5);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 6);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 7);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 8);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 9);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 10);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 11);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 12);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 13);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 14);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 15);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 16);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 17);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 18);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 19);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 20);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 21);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 22);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 23);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 24);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 25);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 26);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 27);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 28);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 29);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 30);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 31);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 32);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 33);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 34);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 35);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 36);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 37);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 38);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 39);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 40);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 41);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 42);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 43);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 44);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 45);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 46);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 47);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 48);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 49);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 50);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 51);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 52);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 53);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 54);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 55);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 56);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 57);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 58);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 59);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 60);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 61);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 62);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 63);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 64);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 65);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 66);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 67);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 68);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 69);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 70);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 71);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 72);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 73);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 74);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 75);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 76);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 77);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 78);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 79);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 80);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 81);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 82);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 83);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 84);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 85);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 86);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 87);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 88);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 89);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 90);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 91);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 92);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 93);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 94);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 95);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 96);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 97);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 98);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 99);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 100);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 101);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 102);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 103);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 104);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 105);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 106);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 107);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 108);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 109);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 110);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 111);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 112);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 113);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 114);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 115);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 116);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 117);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 118);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 119);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 120);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 121);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 122);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 123);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 124);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 125);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 126);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 127);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 128);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 129);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 130);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 131);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 132);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 133);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 134);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 135);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 136);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 137);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 138);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 139);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 140);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 141);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 142);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 143);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 144);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 145);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 146);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 147);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 148);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 149);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 150);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 151);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 152);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 153);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 154);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 155);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 156);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 157);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 158);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 159);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 160);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 161);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 162);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 163);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 164);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 165);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 166);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 167);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 168);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 169);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 170);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 171);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 172);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 173);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 174);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 175);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 176);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 177);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 178);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 179);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 180);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 181);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 182);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 183);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 184);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 185);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 186);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 187);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 188);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 189);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 190);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 191);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 192);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 193);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 194);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 195);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 196);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 197);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 198);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 199);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 200);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 201);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 202);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 203);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 204);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 205);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 206);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 207);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 208);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 209);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 210);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 211);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 212);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 213);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 214);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 215);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 216);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 217);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 218);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 219);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 220);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 221);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 222);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 223);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 224);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 225);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 226);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 227);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 228);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 229);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 230);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 231);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 232);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 233);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 234);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 235);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 236);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 237);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 238);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 239);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 240);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 241);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 242);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Pais",
                keyColumn: "PaisId",
                keyValue: 243);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "TipoDocumento",
                keyColumn: "TipoDocumentoIdentidadId",
                keyValue: 1);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "TipoDocumento",
                keyColumn: "TipoDocumentoIdentidadId",
                keyValue: 2);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "TipoDocumento",
                keyColumn: "TipoDocumentoIdentidadId",
                keyValue: 3);

            migrationBuilder.DeleteData(
                schema: "seguridad",
                table: "Perfil",
                keyColumn: "PerfilId",
                keyValue: 2);

            migrationBuilder.DeleteData(
                schema: "seguridad",
                table: "Usuario",
                keyColumn: "PerfilId",
                keyValue: 1);

            migrationBuilder.DeleteData(
                schema: "seguridad",
                table: "Perfil",
                keyColumn: "PerfilId",
                keyValue: 1);
        }
    }
}
