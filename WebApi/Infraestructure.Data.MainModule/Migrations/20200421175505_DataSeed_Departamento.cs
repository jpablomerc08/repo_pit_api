﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infraestructure.Data.MainModule.Migrations
{
    public partial class DataSeed_Departamento : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                schema: "common",
                table: "Departamento",
                columns: new[] { "DepartamentoId", "Eliminado", "Nombre", "Ubigeo" },
                values: new object[,]
                {
                    { 1, false, "Amazonas", "01" },
                    { 23, false, "Tacna", "23" },
                    { 22, false, "San Martín", "22" },
                    { 21, false, "Puno", "21" },
                    { 20, false, "Piura", "20" },
                    { 19, false, "Pasco", "19" },
                    { 18, false, "Moquegua", "18" },
                    { 17, false, "Madre de Dios", "17" },
                    { 16, false, "Loreto", "16" },
                    { 15, false, "Lima", "15" },
                    { 14, false, "Lambayeque", "14" },
                    { 24, false, "Tumbes", "24" },
                    { 13, false, "La Libertad", "13" },
                    { 11, false, "Ica", "11" },
                    { 10, false, "Huánuco", "10" },
                    { 9, false, "Huancavelica", "09" },
                    { 8, false, "Cusco", "08" },
                    { 7, false, "Callao", "07" },
                    { 6, false, "Cajamarca", "06" },
                    { 5, false, "Ayacucho", "05" },
                    { 4, false, "Arequipa", "04" },
                    { 3, false, "Apurímac", "03" },
                    { 2, false, "Áncash", "02" },
                    { 12, false, "Junín", "12" },
                    { 25, false, "Ucayali", "25" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                schema: "common",
                table: "Departamento",
                keyColumn: "DepartamentoId",
                keyValue: 1);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Departamento",
                keyColumn: "DepartamentoId",
                keyValue: 2);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Departamento",
                keyColumn: "DepartamentoId",
                keyValue: 3);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Departamento",
                keyColumn: "DepartamentoId",
                keyValue: 4);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Departamento",
                keyColumn: "DepartamentoId",
                keyValue: 5);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Departamento",
                keyColumn: "DepartamentoId",
                keyValue: 6);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Departamento",
                keyColumn: "DepartamentoId",
                keyValue: 7);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Departamento",
                keyColumn: "DepartamentoId",
                keyValue: 8);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Departamento",
                keyColumn: "DepartamentoId",
                keyValue: 9);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Departamento",
                keyColumn: "DepartamentoId",
                keyValue: 10);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Departamento",
                keyColumn: "DepartamentoId",
                keyValue: 11);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Departamento",
                keyColumn: "DepartamentoId",
                keyValue: 12);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Departamento",
                keyColumn: "DepartamentoId",
                keyValue: 13);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Departamento",
                keyColumn: "DepartamentoId",
                keyValue: 14);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Departamento",
                keyColumn: "DepartamentoId",
                keyValue: 15);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Departamento",
                keyColumn: "DepartamentoId",
                keyValue: 16);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Departamento",
                keyColumn: "DepartamentoId",
                keyValue: 17);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Departamento",
                keyColumn: "DepartamentoId",
                keyValue: 18);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Departamento",
                keyColumn: "DepartamentoId",
                keyValue: 19);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Departamento",
                keyColumn: "DepartamentoId",
                keyValue: 20);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Departamento",
                keyColumn: "DepartamentoId",
                keyValue: 21);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Departamento",
                keyColumn: "DepartamentoId",
                keyValue: 22);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Departamento",
                keyColumn: "DepartamentoId",
                keyValue: 23);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Departamento",
                keyColumn: "DepartamentoId",
                keyValue: 24);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Departamento",
                keyColumn: "DepartamentoId",
                keyValue: 25);
        }
    }
}
