﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infraestructure.Data.MainModule.Migrations
{
    public partial class DataSeed_Distritos : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                schema: "common",
                table: "Distrito",
                columns: new[] { "DistritoId", "Eliminado", "Nombre", "ProvinciaId", "Ubigeo" },
                values: new object[,]
                {
                    { 1, false, "Chachapoyas", 1, "010101" },
                    { 1257, false, "Saña", 125, "140115" },
                    { 1256, false, "Santa Rosa", 125, "140114" },
                    { 1255, false, "Reque", 125, "140113" },
                    { 1254, false, "Pimentel", 125, "140112" },
                    { 1253, false, "Picsi", 125, "140111" },
                    { 1252, false, "Oyotun", 125, "140110" },
                    { 1251, false, "Nueva Arica", 125, "140109" },
                    { 1250, false, "Monsefu", 125, "140108" },
                    { 1249, false, "Lagunas", 125, "140107" },
                    { 1248, false, "La Victoria", 125, "140106" },
                    { 1247, false, "José Leonardo Ortiz", 125, "140105" },
                    { 1246, false, "Eten Puerto", 125, "140104" },
                    { 1245, false, "Eten", 125, "140103" },
                    { 1244, false, "Chongoyape", 125, "140102" },
                    { 1243, false, "Chiclayo", 125, "140101" },
                    { 1242, false, "Guadalupito", 124, "131203" },
                    { 1241, false, "Chao", 124, "131202" },
                    { 1240, false, "Viru", 124, "131201" },
                    { 1239, false, "Sayapullo", 123, "131104" },
                    { 1238, false, "Marmot", 123, "131103" },
                    { 1237, false, "Lucma", 123, "131102" },
                    { 1236, false, "Cascas", 123, "131101" },
                    { 1235, false, "Sitabamba", 122, "131008" },
                    { 1234, false, "Santa Cruz de Chuca", 122, "131007" },
                    { 1233, false, "Quiruvilca", 122, "131006" },
                    { 1258, false, "Cayalti", 125, "140116" },
                    { 1232, false, "Mollepata", 122, "131005" },
                    { 1259, false, "Patapo", 125, "140117" },
                    { 1261, false, "Pucala", 125, "140119" },
                    { 1286, false, "Carabayllo", 128, "150106" },
                    { 1285, false, "Breña", 128, "150105" },
                    { 1284, false, "Barranco", 128, "150104" },
                    { 1283, false, "Ate", 128, "150103" },
                    { 1282, false, "Ancón", 128, "150102" },
                    { 1281, false, "Lima", 128, "150101" },
                    { 1280, false, "Tucume", 127, "140312" },
                    { 1279, false, "San José", 127, "140311" },
                    { 1278, false, "Salas", 127, "140310" },
                    { 1277, false, "Pacora", 127, "140309" },
                    { 1276, false, "Olmos", 127, "140308" },
                    { 1275, false, "Motupe", 127, "140307" },
                    { 1274, false, "Morrope", 127, "140306" },
                    { 1273, false, "Mochumi", 127, "140305" },
                    { 1272, false, "Jayanca", 127, "140304" },
                    { 1271, false, "Illimo", 127, "140303" },
                    { 1270, false, "Chochope", 127, "140302" },
                    { 1269, false, "Lambayeque", 127, "140301" },
                    { 1268, false, "Pueblo Nuevo", 126, "140206" },
                    { 1267, false, "Pitipo", 126, "140205" },
                    { 1266, false, "Manuel Antonio Mesones Muro", 126, "140204" },
                    { 1265, false, "Incahuasi", 126, "140203" },
                    { 1264, false, "Cañaris", 126, "140202" },
                    { 1263, false, "Ferreñafe", 126, "140201" },
                    { 1262, false, "Tuman", 125, "140120" },
                    { 1260, false, "Pomalca", 125, "140118" },
                    { 1231, false, "Mollebamba", 122, "131004" },
                    { 1230, false, "Cachicadan", 122, "131003" },
                    { 1229, false, "Angasmarca", 122, "131002" },
                    { 1198, false, "Paranday", 118, "130610" },
                    { 1197, false, "Mache", 118, "130608" },
                    { 1196, false, "La Cuesta", 118, "130606" },
                    { 1195, false, "Huaranchal", 118, "130605" },
                    { 1194, false, "Charat", 118, "130604" },
                    { 1193, false, "Agallpampa", 118, "130602" },
                    { 1192, false, "Otuzco", 118, "130601" },
                    { 1191, false, "Huaso", 117, "130504" },
                    { 1190, false, "Carabamba", 117, "130503" },
                    { 1189, false, "Calamarca", 117, "130502" },
                    { 1188, false, "Julcan", 117, "130501" },
                    { 1187, false, "Pueblo Nuevo", 116, "130403" },
                    { 1186, false, "Pacanga", 116, "130402" },
                    { 1185, false, "Chepen", 116, "130401" },
                    { 1184, false, "Ucuncha", 115, "130306" },
                    { 1183, false, "Uchumarca", 115, "130305" },
                    { 1182, false, "Longotea", 115, "130304" },
                    { 1181, false, "Condormarca", 115, "130303" },
                    { 1180, false, "Bambamarca", 115, "130302" },
                    { 1179, false, "Bolívar", 115, "130301" },
                    { 1178, false, "Casa Grande", 114, "130208" },
                    { 1177, false, "Santiago de Cao", 114, "130207" },
                    { 1176, false, "Rázuri", 114, "130206" },
                    { 1175, false, "Paijan", 114, "130205" },
                    { 1174, false, "Magdalena de Cao", 114, "130204" },
                    { 1199, false, "Salpo", 118, "130611" },
                    { 1200, false, "Sinsicap", 118, "130613" },
                    { 1201, false, "Usquil", 118, "130614" },
                    { 1202, false, "San Pedro de Lloc", 119, "130701" },
                    { 1228, false, "Santiago de Chuco", 122, "131001" },
                    { 1227, false, "Sartimbamba", 121, "130908" },
                    { 1226, false, "Sarin", 121, "130907" },
                    { 1225, false, "Sanagoran", 121, "130906" },
                    { 1224, false, "Marcabal", 121, "130905" },
                    { 1223, false, "Curgos", 121, "130904" },
                    { 1222, false, "Cochorco", 121, "130903" },
                    { 1221, false, "Chugay", 121, "130902" },
                    { 1220, false, "Huamachuco", 121, "130901" },
                    { 1219, false, "Urpay", 120, "130813" },
                    { 1218, false, "Taurija", 120, "130812" },
                    { 1217, false, "Santiago de Challas", 120, "130811" },
                    { 1287, false, "Chaclacayo", 128, "150107" },
                    { 1216, false, "Pias", 120, "130810" },
                    { 1214, false, "Parcoy", 120, "130808" },
                    { 1213, false, "Ongon", 120, "130807" },
                    { 1212, false, "Huayo", 120, "130806" },
                    { 1211, false, "Huaylillas", 120, "130805" },
                    { 1210, false, "Huancaspata", 120, "130804" },
                    { 1209, false, "Chillia", 120, "130803" },
                    { 1208, false, "Buldibuyo", 120, "130802" },
                    { 1207, false, "Tayabamba", 120, "130801" },
                    { 1206, false, "San José", 119, "130705" },
                    { 1205, false, "Pacasmayo", 119, "130704" },
                    { 1204, false, "Jequetepeque", 119, "130703" },
                    { 1203, false, "Guadalupe", 119, "130702" },
                    { 1215, false, "Pataz", 120, "130809" },
                    { 1173, false, "Chocope", 114, "130203" },
                    { 1288, false, "Chorrillos", 128, "150108" },
                    { 1290, false, "Comas", 128, "150110" },
                    { 1374, false, "Cuenca", 134, "150706" },
                    { 1373, false, "Chicla", 134, "150705" },
                    { 1372, false, "Carampoma", 134, "150704" },
                    { 1371, false, "Callahuanca", 134, "150703" },
                    { 1370, false, "Antioquia", 134, "150702" },
                    { 1369, false, "Matucana", 134, "150701" },
                    { 1368, false, "Veintisiete de Noviembre", 133, "150612" },
                    { 1367, false, "Sumbilca", 133, "150611" },
                    { 1366, false, "Santa Cruz de Andamarca", 133, "150610" },
                    { 1365, false, "San Miguel de Acos", 133, "150609" },
                    { 1364, false, "Pacaraos", 133, "150608" },
                    { 1363, false, "Lampian", 133, "150607" },
                    { 1362, false, "Ihuari", 133, "150606" },
                    { 1361, false, "Chancay", 133, "150605" },
                    { 1360, false, "Aucallama", 133, "150604" },
                    { 1359, false, "Atavillos Bajo", 133, "150603" },
                    { 1358, false, "Atavillos Alto", 133, "150602" },
                    { 1357, false, "Huaral", 133, "150601" },
                    { 1356, false, "Zúñiga", 132, "150516" },
                    { 1355, false, "Santa Cruz de Flores", 132, "150515" },
                    { 1354, false, "San Luis", 132, "150514" },
                    { 1353, false, "San Antonio", 132, "150513" },
                    { 1352, false, "Quilmana", 132, "150512" },
                    { 1351, false, "Pacaran", 132, "150511" },
                    { 1350, false, "Nuevo Imperial", 132, "150510" },
                    { 1375, false, "Huachupampa", 134, "150707" },
                    { 1349, false, "Mala", 132, "150509" },
                    { 1376, false, "Huanza", 134, "150708" },
                    { 1378, false, "Lahuaytambo", 134, "150710" },
                    { 1403, false, "Caleta de Carquin", 135, "150803" },
                    { 1402, false, "Ambar", 135, "150802" },
                    { 1401, false, "Huacho", 135, "150801" },
                    { 1400, false, "Surco", 134, "150732" },
                    { 1399, false, "Santo Domingo de Los Olleros", 134, "150731" },
                    { 1398, false, "Santiago de Tuna", 134, "150730" },
                    { 1397, false, "Santiago de Anchucaya", 134, "150729" },
                    { 1396, false, "Santa Eulalia", 134, "150728" },
                    { 1395, false, "Santa Cruz de Cocachacra", 134, "150727" },
                    { 1394, false, "Sangallaya", 134, "150726" },
                    { 1393, false, "San Pedro de Huancayre", 134, "150725" },
                    { 1392, false, "San Pedro de Casta", 134, "150724" },
                    { 1391, false, "San Mateo de Otao", 134, "150723" },
                    { 1390, false, "San Mateo", 134, "150722" },
                    { 1389, false, "San Lorenzo de Quinti", 134, "150721" },
                    { 1388, false, "San Juan de Tantaranche", 134, "150720" },
                    { 1387, false, "San Juan de Iris", 134, "150719" },
                    { 1386, false, "San Damian", 134, "150718" },
                    { 1385, false, "San Bartolomé", 134, "150717" },
                    { 1384, false, "San Antonio", 134, "150716" },
                    { 1383, false, "San Andrés de Tupicocha", 134, "150715" },
                    { 1382, false, "Ricardo Palma", 134, "150714" },
                    { 1381, false, "Mariatana", 134, "150713" },
                    { 1380, false, "Laraos", 134, "150712" },
                    { 1379, false, "Langa", 134, "150711" },
                    { 1377, false, "Huarochiri", 134, "150709" },
                    { 1348, false, "Lunahuana", 132, "150508" },
                    { 1347, false, "Imperial", 132, "150507" },
                    { 1346, false, "Coayllo", 132, "150506" },
                    { 1315, false, "San Martín de Porres", 128, "150135" },
                    { 1314, false, "San Luis", 128, "150134" },
                    { 1313, false, "San Juan de Miraflores", 128, "150133" },
                    { 1312, false, "San Juan de Lurigancho", 128, "150132" },
                    { 1311, false, "San Isidro", 128, "150131" },
                    { 1310, false, "San Borja", 128, "150130" },
                    { 1309, false, "San Bartolo", 128, "150129" },
                    { 1308, false, "Rímac", 128, "150128" },
                    { 1307, false, "Punta Negra", 128, "150127" },
                    { 1306, false, "Punta Hermosa", 128, "150126" },
                    { 1305, false, "Puente Piedra", 128, "150125" },
                    { 1304, false, "Pucusana", 128, "150124" },
                    { 1303, false, "Pachacamac", 128, "150123" },
                    { 1302, false, "Miraflores", 128, "150122" },
                    { 1301, false, "Pueblo Libre", 128, "150121" },
                    { 1300, false, "Magdalena del Mar", 128, "150120" },
                    { 1299, false, "Lurin", 128, "150119" },
                    { 1298, false, "Lurigancho", 128, "150118" },
                    { 1297, false, "Los Olivos", 128, "150117" },
                    { 1296, false, "Lince", 128, "150116" },
                    { 1295, false, "La Victoria", 128, "150115" },
                    { 1294, false, "La Molina", 128, "150114" },
                    { 1293, false, "Jesús María", 128, "150113" },
                    { 1292, false, "Independencia", 128, "150112" },
                    { 1291, false, "El Agustino", 128, "150111" },
                    { 1316, false, "San Miguel", 128, "150136" },
                    { 1317, false, "Santa Anita", 128, "150137" },
                    { 1318, false, "Santa María del Mar", 128, "150138" },
                    { 1319, false, "Santa Rosa", 128, "150139" },
                    { 1345, false, "Chilca", 132, "150505" },
                    { 1344, false, "Cerro Azul", 132, "150504" },
                    { 1343, false, "Calango", 132, "150503" },
                    { 1342, false, "Asia", 132, "150502" },
                    { 1341, false, "San Vicente de Cañete", 132, "150501" },
                    { 1340, false, "Santa Rosa de Quives", 131, "150407" },
                    { 1339, false, "San Buenaventura", 131, "150406" },
                    { 1338, false, "Lachaqui", 131, "150405" },
                    { 1337, false, "Huaros", 131, "150404" },
                    { 1336, false, "Huamantanga", 131, "150403" },
                    { 1335, false, "Arahuay", 131, "150402" },
                    { 1334, false, "Canta", 131, "150401" },
                    { 1289, false, "Cieneguilla", 128, "150109" },
                    { 1333, false, "Manas", 130, "150305" },
                    { 1331, false, "Gorgor", 130, "150303" },
                    { 1330, false, "Copa", 130, "150302" },
                    { 1329, false, "Cajatambo", 130, "150301" },
                    { 1328, false, "Supe Puerto", 129, "150205" },
                    { 1327, false, "Supe", 129, "150204" },
                    { 1326, false, "Pativilca", 129, "150203" },
                    { 1325, false, "Paramonga", 129, "150202" },
                    { 1324, false, "Barranca", 129, "150201" },
                    { 1323, false, "Villa María del Triunfo", 128, "150143" },
                    { 1322, false, "Villa El Salvador", 128, "150142" },
                    { 1321, false, "Surquillo", 128, "150141" },
                    { 1320, false, "Santiago de Surco", 128, "150140" },
                    { 1332, false, "Huancapon", 130, "150304" },
                    { 1404, false, "Checras", 135, "150804" },
                    { 1172, false, "Chicama", 114, "130202" },
                    { 1170, false, "Victor Larco Herrera", 113, "130111" },
                    { 1023, false, "Palpa", 102, "110401" },
                    { 1022, false, "Vista Alegre", 101, "110305" },
                    { 1021, false, "Marcona", 101, "110304" },
                    { 1020, false, "El Ingenio", 101, "110303" },
                    { 1019, false, "Changuillo", 101, "110302" },
                    { 1018, false, "Nasca", 101, "110301" },
                    { 1017, false, "Tambo de Mora", 100, "110211" },
                    { 1016, false, "Sunampe", 100, "110210" },
                    { 1015, false, "San Pedro de Huacarpana", 100, "110209" },
                    { 1014, false, "San Juan de Yanac", 100, "110208" },
                    { 1013, false, "Pueblo Nuevo", 100, "110207" },
                    { 1012, false, "Grocio Prado", 100, "110206" },
                    { 1011, false, "El Carmen", 100, "110205" },
                    { 1010, false, "Chincha Baja", 100, "110204" },
                    { 1009, false, "Chavin", 100, "110203" },
                    { 1008, false, "Alto Laran", 100, "110202" },
                    { 1007, false, "Chincha Alta", 100, "110201" },
                    { 1006, false, "Yauca del Rosario", 99, "110114" },
                    { 1005, false, "Tate", 99, "110113" },
                    { 1004, false, "Subtanjalla", 99, "110112" },
                    { 1003, false, "Santiago", 99, "110111" },
                    { 1002, false, "San Juan Bautista", 99, "110110" },
                    { 1001, false, "San José de Los Molinos", 99, "110109" },
                    { 1000, false, "Salas", 99, "110108" },
                    { 999, false, "Pueblo Nuevo", 99, "110107" },
                    { 1024, false, "Llipata", 102, "110402" },
                    { 998, false, "Parcona", 99, "110106" },
                    { 1025, false, "Río Grande", 102, "110403" },
                    { 1027, false, "Tibillo", 102, "110405" },
                    { 1052, false, "Pariahuanca", 104, "120124" },
                    { 1051, false, "Ingenio", 104, "120122" },
                    { 1050, false, "Huayucachi", 104, "120121" },
                    { 1049, false, "Huasicancha", 104, "120120" },
                    { 1048, false, "Huancan", 104, "120119" },
                    { 1047, false, "Hualhuas", 104, "120117" },
                    { 1046, false, "Huacrapuquio", 104, "120116" },
                    { 1045, false, "El Tambo", 104, "120114" },
                    { 1044, false, "Cullhuas", 104, "120113" },
                    { 1043, false, "Colca", 104, "120112" },
                    { 1042, false, "Chupuro", 104, "120111" },
                    { 1041, false, "Chongos Alto", 104, "120108" },
                    { 1040, false, "Chilca", 104, "120107" },
                    { 1039, false, "Chicche", 104, "120106" },
                    { 1038, false, "Chacapampa", 104, "120105" },
                    { 1037, false, "Carhuacallanga", 104, "120104" },
                    { 1036, false, "Huancayo", 104, "120101" },
                    { 1035, false, "Tupac Amaru Inca", 103, "110508" },
                    { 1034, false, "San Clemente", 103, "110507" },
                    { 1033, false, "San Andrés", 103, "110506" },
                    { 1032, false, "Paracas", 103, "110505" },
                    { 1031, false, "Independencia", 103, "110504" },
                    { 1030, false, "Humay", 103, "110503" },
                    { 1029, false, "Huancano", 103, "110502" },
                    { 1028, false, "Pisco", 103, "110501" },
                    { 1026, false, "Santa Cruz", 102, "110404" },
                    { 997, false, "Pachacutec", 99, "110105" },
                    { 996, false, "Ocucaje", 99, "110104" },
                    { 995, false, "Los Aquijes", 99, "110103" },
                    { 964, false, "Huacrachuco", 94, "100701" },
                    { 963, false, "Santo Domingo de Anda", 93, "100610" },
                    { 962, false, "Pueblo Nuevo", 93, "100609" },
                    { 961, false, "Castillo Grande", 93, "100608" },
                    { 960, false, "Pucayacu", 93, "100607" },
                    { 959, false, "Mariano Damaso Beraun", 93, "100606" },
                    { 958, false, "Luyando", 93, "100605" },
                    { 957, false, "José Crespo y Castillo", 93, "100604" },
                    { 956, false, "Hermílio Valdizan", 93, "100603" },
                    { 955, false, "Daniel Alomía Robles", 93, "100602" },
                    { 954, false, "Rupa-Rupa", 93, "100601" },
                    { 953, false, "Tantamayo", 92, "100511" },
                    { 952, false, "Singa", 92, "100510" },
                    { 951, false, "Puños", 92, "100509" },
                    { 950, false, "Punchao", 92, "100508" },
                    { 949, false, "Monzón", 92, "100507" },
                    { 948, false, "Miraflores", 92, "100506" },
                    { 947, false, "Jircan", 92, "100505" },
                    { 946, false, "Jacas Grande", 92, "100504" },
                    { 945, false, "Chavín de Pariarca", 92, "100503" },
                    { 944, false, "Arancay", 92, "100502" },
                    { 943, false, "Llata", 92, "100501" },
                    { 942, false, "Pinra", 91, "100404" },
                    { 941, false, "Cochabamba", 91, "100403" },
                    { 940, false, "Canchabamba", 91, "100402" },
                    { 965, false, "Cholon", 94, "100702" },
                    { 966, false, "San Buenaventura", 94, "100703" },
                    { 967, false, "La Morada", 94, "100704" },
                    { 968, false, "Santa Rosa de Alto Yanajanca", 94, "100705" },
                    { 994, false, "La Tinguiña", 99, "110102" },
                    { 993, false, "Ica", 99, "110101" },
                    { 992, false, "Choras", 98, "101108" },
                    { 991, false, "Pampamarca", 98, "101107" },
                    { 990, false, "Obas", 98, "101106" },
                    { 989, false, "Jacas Chico", 98, "101105" },
                    { 988, false, "Aparicio Pomares", 98, "101104" },
                    { 987, false, "Chacabamba", 98, "101103" },
                    { 986, false, "Cahuac", 98, "101102" },
                    { 985, false, "Chavinillo", 98, "101101" },
                    { 984, false, "San Miguel de Cauri", 97, "101007" },
                    { 983, false, "San Francisco de Asís", 97, "101006" },
                    { 1053, false, "Pilcomayo", 104, "120125" },
                    { 982, false, "Rondos", 97, "101005" },
                    { 980, false, "Jivia", 97, "101003" },
                    { 979, false, "Baños", 97, "101002" },
                    { 978, false, "Jesús", 97, "101001" },
                    { 977, false, "Yuyapichis", 96, "100905" },
                    { 976, false, "Tournavista", 96, "100904" },
                    { 975, false, "Honoria", 96, "100903" },
                    { 974, false, "Codo del Pozuzo", 96, "100902" },
                    { 973, false, "Puerto Inca", 96, "100901" },
                    { 972, false, "Umari", 95, "100804" },
                    { 971, false, "Molino", 95, "100803" },
                    { 970, false, "Chaglla", 95, "100802" },
                    { 969, false, "Panao", 95, "100801" },
                    { 981, false, "Queropalca", 97, "101004" },
                    { 1171, false, "Ascope", 114, "130201" },
                    { 1054, false, "Pucara", 104, "120126" },
                    { 1056, false, "Quilcas", 104, "120128" },
                    { 1140, false, "Tapo", 110, "120709" },
                    { 1139, false, "San Pedro de Cajas", 110, "120708" },
                    { 1138, false, "Palcamayo", 110, "120707" },
                    { 1137, false, "Palca", 110, "120706" },
                    { 1136, false, "La Unión", 110, "120705" },
                    { 1135, false, "Huasahuasi", 110, "120704" },
                    { 1134, false, "Huaricolca", 110, "120703" },
                    { 1133, false, "Acobamba", 110, "120702" },
                    { 1132, false, "Tarma", 110, "120701" },
                    { 1131, false, "Vizcatan del Ene", 109, "120609" },
                    { 1130, false, "Río Tambo", 109, "120608" },
                    { 1129, false, "Río Negro", 109, "120607" },
                    { 1128, false, "Pangoa", 109, "120606" },
                    { 1127, false, "Pampa Hermosa", 109, "120605" },
                    { 1126, false, "Mazamari", 109, "120604" },
                    { 1125, false, "Llaylla", 109, "120603" },
                    { 1124, false, "Coviriali", 109, "120602" },
                    { 1123, false, "Satipo", 109, "120601" },
                    { 1122, false, "Ulcumayo", 108, "120504" },
                    { 1121, false, "Ondores", 108, "120503" },
                    { 1120, false, "Carhuamayo", 108, "120502" },
                    { 1119, false, "Junin", 108, "120501" },
                    { 1118, false, "Yauyos", 107, "120434" },
                    { 1117, false, "Yauli", 107, "120433" },
                    { 1116, false, "Tunan Marca", 107, "120432" },
                    { 1141, false, "La Oroya", 111, "120801" },
                    { 1115, false, "Sincos", 107, "120431" },
                    { 1142, false, "Chacapalpa", 111, "120802" },
                    { 1144, false, "Marcapomacocha", 111, "120804" },
                    { 1169, false, "Simbal", 113, "130110" },
                    { 1168, false, "Salaverry", 113, "130109" },
                    { 1167, false, "Poroto", 113, "130108" },
                    { 1166, false, "Moche", 113, "130107" },
                    { 1165, false, "Laredo", 113, "130106" },
                    { 1164, false, "La Esperanza", 113, "130105" },
                    { 1163, false, "Huanchaco", 113, "130104" },
                    { 1162, false, "Florencia de Mora", 113, "130103" },
                    { 1161, false, "El Porvenir", 113, "130102" },
                    { 1160, false, "Trujillo", 113, "130101" },
                    { 1159, false, "Yanacancha", 112, "120909" },
                    { 1158, false, "Tres de Diciembre", 112, "120908" },
                    { 1157, false, "San Juan de Jarpa", 112, "120907" },
                    { 1156, false, "San Juan de Iscos", 112, "120906" },
                    { 1155, false, "Huamancaca Chico", 112, "120905" },
                    { 1154, false, "Huachac", 112, "120904" },
                    { 1153, false, "Chongos Bajo", 112, "120903" },
                    { 1152, false, "Ahuac", 112, "120902" },
                    { 1151, false, "Chupaca", 112, "120901" },
                    { 1150, false, "Yauli", 111, "120810" },
                    { 1149, false, "Suitucancha", 111, "120809" },
                    { 1148, false, "Santa Rosa de Sacco", 111, "120808" },
                    { 1147, false, "Santa Bárbara de Carhuacayan", 111, "120807" },
                    { 1146, false, "Paccha", 111, "120806" },
                    { 1145, false, "Morococha", 111, "120805" },
                    { 1143, false, "Huay-Huay", 111, "120803" },
                    { 1114, false, "Sausa", 107, "120430" },
                    { 1113, false, "San Pedro de Chunan", 107, "120429" },
                    { 1112, false, "San Lorenzo", 107, "120428" },
                    { 1081, false, "Pichanaqui", 106, "120303" },
                    { 1080, false, "Perene", 106, "120302" },
                    { 1079, false, "Chanchamayo", 106, "120301" },
                    { 1078, false, "Santa Rosa de Ocopa", 105, "120215" },
                    { 1077, false, "San José de Quero", 105, "120214" },
                    { 1076, false, "Orcotuna", 105, "120213" },
                    { 1075, false, "Nueve de Julio", 105, "120212" },
                    { 1074, false, "Mito", 105, "120211" },
                    { 1073, false, "Matahuasi", 105, "120210" }
                });

            migrationBuilder.InsertData(
                schema: "common",
                table: "Distrito",
                columns: new[] { "DistritoId", "Eliminado", "Nombre", "ProvinciaId", "Ubigeo" },
                values: new object[,]
                {
                    { 1072, false, "Mariscal Castilla", 105, "120209" },
                    { 1071, false, "Manzanares", 105, "120208" },
                    { 1070, false, "Heroínas Toledo", 105, "120207" },
                    { 1069, false, "Comas", 105, "120206" },
                    { 1068, false, "Cochas", 105, "120205" },
                    { 1067, false, "Chambara", 105, "120204" },
                    { 1066, false, "Andamarca", 105, "120203" },
                    { 1065, false, "Aco", 105, "120202" },
                    { 1064, false, "Concepción", 105, "120201" },
                    { 1063, false, "Viques", 104, "120136" },
                    { 1062, false, "Santo Domingo de Acobamba", 104, "120135" },
                    { 1061, false, "Sicaya", 104, "120134" },
                    { 1060, false, "Sapallanga", 104, "120133" },
                    { 1059, false, "Saño", 104, "120132" },
                    { 1058, false, "San Jerónimo de Tunan", 104, "120130" },
                    { 1057, false, "San Agustín", 104, "120129" },
                    { 1082, false, "San Luis de Shuaro", 106, "120304" },
                    { 1083, false, "San Ramón", 106, "120305" },
                    { 1084, false, "Vitoc", 106, "120306" },
                    { 1085, false, "Jauja", 107, "120401" },
                    { 1111, false, "Ricran", 107, "120427" },
                    { 1110, false, "Pomacancha", 107, "120426" },
                    { 1109, false, "Parco", 107, "120425" },
                    { 1108, false, "Pancan", 107, "120424" },
                    { 1107, false, "Paccha", 107, "120423" },
                    { 1106, false, "Paca", 107, "120422" },
                    { 1105, false, "Muquiyauyo", 107, "120421" },
                    { 1104, false, "Muqui", 107, "120420" },
                    { 1103, false, "Monobamba", 107, "120419" },
                    { 1102, false, "Molinos", 107, "120418" },
                    { 1101, false, "Masma Chicche", 107, "120417" },
                    { 1100, false, "Masma", 107, "120416" },
                    { 1055, false, "Quichuay", 104, "120127" },
                    { 1099, false, "Marco", 107, "120415" },
                    { 1097, false, "Leonor Ordóñez", 107, "120413" },
                    { 1096, false, "Julcán", 107, "120412" },
                    { 1095, false, "Janjaillo", 107, "120411" },
                    { 1094, false, "Huertas", 107, "120410" },
                    { 1093, false, "Huaripampa", 107, "120409" },
                    { 1092, false, "Huamali", 107, "120408" },
                    { 1091, false, "El Mantaro", 107, "120407" },
                    { 1090, false, "Curicaca", 107, "120406" },
                    { 1089, false, "Canchayllo", 107, "120405" },
                    { 1088, false, "Ataura", 107, "120404" },
                    { 1087, false, "Apata", 107, "120403" },
                    { 1086, false, "Acolla", 107, "120402" },
                    { 1098, false, "Llocllapampa", 107, "120414" },
                    { 1405, false, "Hualmay", 135, "150805" },
                    { 1406, false, "Huaura", 135, "150806" },
                    { 1407, false, "Leoncio Prado", 135, "150807" },
                    { 1726, false, "Patambuco", 174, "211204" },
                    { 1725, false, "Limbani", 174, "211203" },
                    { 1724, false, "Cuyocuyo", 174, "211202" },
                    { 1723, false, "Sandia", 174, "211201" },
                    { 1722, false, "San Miguel", 173, "211105" },
                    { 1721, false, "Caracoto", 173, "211104" },
                    { 1720, false, "Cabanillas", 173, "211103" },
                    { 1719, false, "Cabana", 173, "211102" },
                    { 1718, false, "Juliaca", 173, "211101" },
                    { 1717, false, "Sina", 172, "211005" },
                    { 1716, false, "Quilcapuncu", 172, "211004" },
                    { 1715, false, "Pedro Vilca Apaza", 172, "211003" },
                    { 1714, false, "Ananea", 172, "211002" },
                    { 1713, false, "Putina", 172, "211001" },
                    { 1712, false, "Tilali", 171, "210904" },
                    { 1711, false, "Huayrapata", 171, "210903" },
                    { 1710, false, "Conima", 171, "210902" },
                    { 1709, false, "Moho", 171, "210901" },
                    { 1708, false, "Umachiri", 170, "210809" },
                    { 1707, false, "Santa Rosa", 170, "210808" },
                    { 1706, false, "Orurillo", 170, "210807" },
                    { 1705, false, "Nuñoa", 170, "210806" },
                    { 1704, false, "Macari", 170, "210805" },
                    { 1703, false, "Llalli", 170, "210804" },
                    { 1702, false, "Cupi", 170, "210803" },
                    { 1727, false, "Phara", 174, "211205" },
                    { 1701, false, "Antauta", 170, "210802" },
                    { 1728, false, "Quiaca", 174, "211206" },
                    { 1730, false, "Yanahuaya", 174, "211208" },
                    { 1755, false, "Santa Rosa", 178, "220304" },
                    { 1754, false, "San Martín", 178, "220303" },
                    { 1753, false, "Agua Blanca", 178, "220302" },
                    { 1752, false, "San José de Sisa", 178, "220301" },
                    { 1751, false, "San Rafael", 177, "220206" },
                    { 1750, false, "San Pablo", 177, "220205" },
                    { 1749, false, "Huallaga", 177, "220204" },
                    { 1748, false, "Bajo Biavo", 177, "220203" },
                    { 1747, false, "Alto Biavo", 177, "220202" },
                    { 1746, false, "Bellavista", 177, "220201" },
                    { 1745, false, "Yantalo", 176, "220106" },
                    { 1744, false, "Soritor", 176, "220105" },
                    { 1743, false, "Jepelacio", 176, "220104" },
                    { 1742, false, "Habana", 176, "220103" },
                    { 1741, false, "Calzada", 176, "220102" },
                    { 1740, false, "Moyobamba", 176, "220101" },
                    { 1739, false, "Unicachi", 175, "211307" },
                    { 1738, false, "Tinicachi", 175, "211306" },
                    { 1737, false, "Ollaraya", 175, "211305" },
                    { 1736, false, "Cuturapi", 175, "211304" },
                    { 1735, false, "Copani", 175, "211303" },
                    { 1734, false, "Anapia", 175, "211302" },
                    { 1733, false, "Yunguyo", 175, "211301" },
                    { 1732, false, "San Pedro de Putina Punco", 174, "211210" },
                    { 1731, false, "Alto Inambari", 174, "211209" },
                    { 1729, false, "San Juan del Oro", 174, "211207" },
                    { 1700, false, "Ayaviri", 170, "210801" },
                    { 1699, false, "Vilavila", 169, "210710" },
                    { 1698, false, "Santa Lucia", 169, "210709" },
                    { 1667, false, "Ollachea", 165, "210308" },
                    { 1666, false, "Ituata", 165, "210307" },
                    { 1665, false, "Crucero", 165, "210306" },
                    { 1664, false, "Corani", 165, "210305" },
                    { 1663, false, "Coasa", 165, "210304" },
                    { 1662, false, "Ayapata", 165, "210303" },
                    { 1661, false, "Ajoyani", 165, "210302" },
                    { 1660, false, "Macusani", 165, "210301" },
                    { 1659, false, "Tirapata", 164, "210215" },
                    { 1658, false, "Santiago de Pupuja", 164, "210214" },
                    { 1657, false, "San Juan de Salinas", 164, "210213" },
                    { 1656, false, "San José", 164, "210212" },
                    { 1655, false, "San Anton", 164, "210211" },
                    { 1654, false, "Saman", 164, "210210" },
                    { 1653, false, "Potoni", 164, "210209" },
                    { 1652, false, "Muñani", 164, "210208" },
                    { 1651, false, "José Domingo Choquehuanca", 164, "210207" },
                    { 1650, false, "Chupa", 164, "210206" },
                    { 1649, false, "Caminaca", 164, "210205" },
                    { 1648, false, "Asillo", 164, "210204" },
                    { 1647, false, "Arapa", 164, "210203" },
                    { 1646, false, "Achaya", 164, "210202" },
                    { 1645, false, "Azángaro", 164, "210201" },
                    { 1644, false, "Vilque", 163, "210115" },
                    { 1643, false, "Tiquillaca", 163, "210114" },
                    { 1668, false, "San Gaban", 165, "210309" },
                    { 1669, false, "Usicayos", 165, "210310" },
                    { 1670, false, "Juli", 166, "210401" },
                    { 1671, false, "Desaguadero", 166, "210402" },
                    { 1697, false, "Pucara", 169, "210708" },
                    { 1696, false, "Paratia", 169, "210707" },
                    { 1695, false, "Palca", 169, "210706" },
                    { 1694, false, "Ocuviri", 169, "210705" },
                    { 1693, false, "Nicasio", 169, "210704" },
                    { 1692, false, "Calapuja", 169, "210703" },
                    { 1691, false, "Cabanilla", 169, "210702" },
                    { 1690, false, "Lampa", 169, "210701" },
                    { 1689, false, "Vilque Chico", 168, "210608" },
                    { 1688, false, "Taraco", 168, "210607" },
                    { 1687, false, "Rosaspata", 168, "210606" },
                    { 1686, false, "Pusi", 168, "210605" },
                    { 1756, false, "Shatoja", 178, "220305" },
                    { 1685, false, "Inchupalla", 168, "210604" },
                    { 1683, false, "Cojata", 168, "210602" },
                    { 1682, false, "Huancane", 168, "210601" },
                    { 1681, false, "Conduriri", 167, "210505" },
                    { 1680, false, "Santa Rosa", 167, "210504" },
                    { 1679, false, "Pilcuyo", 167, "210503" },
                    { 1678, false, "Capazo", 167, "210502" },
                    { 1677, false, "Ilave", 167, "210501" },
                    { 1676, false, "Zepita", 166, "210407" },
                    { 1675, false, "Pomata", 166, "210406" },
                    { 1674, false, "Pisacoma", 166, "210405" },
                    { 1673, false, "Kelluyo", 166, "210404" },
                    { 1672, false, "Huacullani", 166, "210403" },
                    { 1684, false, "Huatasani", 168, "210603" },
                    { 1642, false, "San Antonio", 163, "210113" },
                    { 1757, false, "Saposoa", 179, "220401" },
                    { 1759, false, "El Eslabón", 179, "220403" },
                    { 1843, false, "Tarucachi", 189, "230407" },
                    { 1842, false, "Susapaya", 189, "230406" },
                    { 1841, false, "Sitajara", 189, "230405" },
                    { 1840, false, "Estique-Pampa", 189, "230404" },
                    { 1839, false, "Estique", 189, "230403" },
                    { 1838, false, "Héroes Albarracín", 189, "230402" },
                    { 1837, false, "Tarata", 189, "230401" },
                    { 1836, false, "Ite", 188, "230303" },
                    { 1835, false, "Ilabaya", 188, "230302" },
                    { 1834, false, "Locumba", 188, "230301" },
                    { 1833, false, "Quilahuani", 187, "230206" },
                    { 1832, false, "Huanuara", 187, "230205" },
                    { 1831, false, "Curibaya", 187, "230204" },
                    { 1830, false, "Camilaca", 187, "230203" },
                    { 1829, false, "Cairani", 187, "230202" },
                    { 1828, false, "Candarave", 187, "230201" },
                    { 1827, false, "La Yarada los Palos", 186, "230111" },
                    { 1826, false, "Coronel Gregorio Albarracín Lanchipa", 186, "230110" },
                    { 1825, false, "Sama", 186, "230109" },
                    { 1824, false, "Pocollay", 186, "230108" },
                    { 1823, false, "Palca", 186, "230107" },
                    { 1822, false, "Pachia", 186, "230106" },
                    { 1821, false, "Inclan", 186, "230105" },
                    { 1820, false, "Ciudad Nueva", 186, "230104" },
                    { 1819, false, "Calana", 186, "230103" },
                    { 1844, false, "Ticaco", 189, "230408" },
                    { 1818, false, "Alto de la Alianza", 186, "230102" },
                    { 1845, false, "Tumbes", 190, "240101" },
                    { 1847, false, "La Cruz", 190, "240103" },
                    { 1872, false, "Neshuya", 195, "250304" },
                    { 1871, false, "Curimana", 195, "250303" },
                    { 1870, false, "Irazola", 195, "250302" },
                    { 1869, false, "Padre Abad", 195, "250301" },
                    { 1868, false, "Yurua", 194, "250204" },
                    { 1867, false, "Tahuania", 194, "250203" },
                    { 1866, false, "Sepahua", 194, "250202" },
                    { 1865, false, "Raymondi", 194, "250201" },
                    { 1864, false, "Manantay", 193, "250107" },
                    { 1863, false, "Nueva Requena", 193, "250106" },
                    { 1862, false, "Yarinacocha", 193, "250105" },
                    { 1861, false, "Masisea", 193, "250104" },
                    { 1860, false, "Iparia", 193, "250103" },
                    { 1859, false, "Campoverde", 193, "250102" },
                    { 1858, false, "Calleria", 193, "250101" },
                    { 1857, false, "Papayal", 192, "240304" },
                    { 1856, false, "Matapalo", 192, "240303" },
                    { 1855, false, "Aguas Verdes", 192, "240302" },
                    { 1854, false, "Zarumilla", 192, "240301" },
                    { 1853, false, "Canoas de Punta Sal", 191, "240203" },
                    { 1852, false, "Casitas", 191, "240202" },
                    { 1851, false, "Zorritos", 191, "240201" },
                    { 1850, false, "San Juan de la Virgen", 190, "240106" },
                    { 1849, false, "San Jacinto", 190, "240105" },
                    { 1848, false, "Pampas de Hospital", 190, "240104" },
                    { 1846, false, "Corrales", 190, "240102" },
                    { 1817, false, "Tacna", 186, "230101" },
                    { 1816, false, "Uchiza", 185, "221005" },
                    { 1815, false, "Shunte", 185, "221004" },
                    { 1784, false, "San Cristóbal", 182, "220706" },
                    { 1783, false, "Pucacaca", 182, "220705" },
                    { 1782, false, "Pilluana", 182, "220704" },
                    { 1781, false, "Caspisapa", 182, "220703" },
                    { 1780, false, "Buenos Aires", 182, "220702" },
                    { 1779, false, "Picota", 182, "220701" },
                    { 1778, false, "Pajarillo", 181, "220605" },
                    { 1777, false, "Pachiza", 181, "220604" },
                    { 1776, false, "Huicungo", 181, "220603" },
                    { 1775, false, "Campanilla", 181, "220602" },
                    { 1774, false, "Juanjuí", 181, "220601" },
                    { 1773, false, "Zapatero", 180, "220511" },
                    { 1772, false, "Tabalosos", 180, "220510" },
                    { 1771, false, "Shanao", 180, "220509" },
                    { 1770, false, "San Roque de Cumbaza", 180, "220508" },
                    { 1769, false, "Rumisapa", 180, "220507" },
                    { 1768, false, "Pinto Recodo", 180, "220506" },
                    { 1767, false, "Cuñumbuqui", 180, "220505" },
                    { 1766, false, "Caynarachi", 180, "220504" },
                    { 1765, false, "Barranquita", 180, "220503" },
                    { 1764, false, "Alonso de Alvarado", 180, "220502" },
                    { 1763, false, "Lamas", 180, "220501" },
                    { 1762, false, "Tingo de Saposoa", 179, "220406" },
                    { 1761, false, "Sacanche", 179, "220405" },
                    { 1760, false, "Piscoyacu", 179, "220404" },
                    { 1785, false, "San Hilarión", 182, "220707" },
                    { 1786, false, "Shamboyacu", 182, "220708" },
                    { 1787, false, "Tingo de Ponasa", 182, "220709" },
                    { 1788, false, "Tres Unidos", 182, "220710" },
                    { 1814, false, "Polvora", 185, "221003" },
                    { 1813, false, "Nuevo Progreso", 185, "221002" },
                    { 1812, false, "Tocache", 185, "221001" },
                    { 1811, false, "Shapaja", 184, "220914" },
                    { 1810, false, "Sauce", 184, "220913" },
                    { 1809, false, "San Antonio", 184, "220912" },
                    { 1808, false, "Papaplaya", 184, "220911" },
                    { 1807, false, "Morales", 184, "220910" },
                    { 1806, false, "La Banda de Shilcayo", 184, "220909" },
                    { 1805, false, "Juan Guerra", 184, "220908" },
                    { 1804, false, "Huimbayoc", 184, "220907" },
                    { 1803, false, "El Porvenir", 184, "220906" },
                    { 1758, false, "Alto Saposoa", 179, "220402" },
                    { 1802, false, "Chipurana", 184, "220905" },
                    { 1800, false, "Cacatachi", 184, "220903" },
                    { 1799, false, "Alberto Leveau", 184, "220902" },
                    { 1798, false, "Tarapoto", 184, "220901" },
                    { 1797, false, "Yuracyacu", 183, "220809" },
                    { 1796, false, "Yorongos", 183, "220808" },
                    { 1795, false, "San Fernando", 183, "220807" },
                    { 1794, false, "Posic", 183, "220806" },
                    { 1793, false, "Pardo Miguel", 183, "220805" },
                    { 1792, false, "Nueva Cajamarca", 183, "220804" },
                    { 1791, false, "Elías Soplin Vargas", 183, "220803" },
                    { 1790, false, "Awajun", 183, "220802" },
                    { 1789, false, "Rioja", 183, "220801" },
                    { 1801, false, "Chazuta", 184, "220904" },
                    { 1641, false, "Plateria", 163, "210112" },
                    { 1640, false, "Pichacani", 163, "210111" },
                    { 1639, false, "Paucarcolla", 163, "210110" },
                    { 1491, false, "Padre Márquez", 143, "160603" },
                    { 1490, false, "Inahuaya", 143, "160602" },
                    { 1489, false, "Contamana", 143, "160601" },
                    { 1488, false, "Yaquerana", 142, "160511" },
                    { 1487, false, "Jenaro Herrera", 142, "160510" },
                    { 1486, false, "Tapiche", 142, "160509" },
                    { 1485, false, "Soplin", 142, "160508" },
                    { 1484, false, "Saquena", 142, "160507" },
                    { 1483, false, "Puinahua", 142, "160506" },
                    { 1482, false, "Maquia", 142, "160505" },
                    { 1481, false, "Emilio San Martín", 142, "160504" },
                    { 1480, false, "Capelo", 142, "160503" },
                    { 1479, false, "Alto Tapiche", 142, "160502" },
                    { 1478, false, "Requena", 142, "160501" },
                    { 1477, false, "San Pablo", 141, "160404" },
                    { 1476, false, "Yavari", 141, "160403" },
                    { 1475, false, "Pebas", 141, "160402" },
                    { 1474, false, "Ramón Castilla", 141, "160401" },
                    { 1473, false, "Urarinas", 140, "160305" },
                    { 1472, false, "Trompeteros", 140, "160304" },
                    { 1471, false, "Tigre", 140, "160303" },
                    { 1470, false, "Parinari", 140, "160302" },
                    { 1469, false, "Nauta", 140, "160301" },
                    { 1468, false, "Teniente Cesar López Rojas", 139, "160211" },
                    { 1467, false, "Santa Cruz", 139, "160210" },
                    { 1492, false, "Pampa Hermosa", 143, "160604" },
                    { 1466, false, "Lagunas", 139, "160206" },
                    { 1493, false, "Sarayacu", 143, "160605" },
                    { 1495, false, "Barranca", 144, "160701" },
                    { 1520, false, "San Cristóbal", 149, "180105" },
                    { 1519, false, "Samegua", 149, "180104" },
                    { 1518, false, "Cuchumbaya", 149, "180103" },
                    { 1517, false, "Carumas", 149, "180102" },
                    { 1516, false, "Moquegua", 149, "180101" },
                    { 1515, false, "Tahuamanu", 148, "170303" },
                    { 1514, false, "Iberia", 148, "170302" },
                    { 1513, false, "Iñapari", 148, "170301" },
                    { 1512, false, "Huepetuhe", 147, "170204" },
                    { 1511, false, "Madre de Dios", 147, "170203" },
                    { 1510, false, "Fitzcarrald", 147, "170202" },
                    { 1509, false, "Manu", 147, "170201" },
                    { 1508, false, "Laberinto", 146, "170104" },
                    { 1507, false, "Las Piedras", 146, "170103" },
                    { 1506, false, "Inambari", 146, "170102" },
                    { 1505, false, "Tambopata", 146, "170101" },
                    { 1504, false, "Yaguas", 145, "160804" },
                    { 1503, false, "Teniente Manuel Clavero", 145, "160803" },
                    { 1502, false, "Rosa Panduro", 145, "160802" },
                    { 1501, false, "Putumayo", 145, "160801" },
                    { 1500, false, "Andoas", 144, "160706" },
                    { 1499, false, "Pastaza", 144, "160705" },
                    { 1498, false, "Morona", 144, "160704" },
                    { 1497, false, "Manseriche", 144, "160703" },
                    { 1496, false, "Cahuapanas", 144, "160702" },
                    { 1494, false, "Vargas Guerra", 143, "160606" },
                    { 1465, false, "Jeberos", 139, "160205" },
                    { 1464, false, "Balsapuerto", 139, "160202" },
                    { 1463, false, "Yurimaguas", 139, "160201" },
                    { 1432, false, "Huancaya", 137, "151014" },
                    { 1431, false, "Huampara", 137, "151013" },
                    { 1430, false, "Hongos", 137, "151012" },
                    { 1429, false, "Colonia", 137, "151011" },
                    { 1428, false, "Cochas", 137, "151010" },
                    { 1427, false, "Chocos", 137, "151009" },
                    { 1426, false, "Catahuasi", 137, "151008" },
                    { 1425, false, "Carania", 137, "151007" },
                    { 1424, false, "Cacra", 137, "151006" },
                    { 1423, false, "Azángaro", 137, "151005" },
                    { 1422, false, "Ayaviri", 137, "151004" },
                    { 1421, false, "Allauca", 137, "151003" },
                    { 1420, false, "Alis", 137, "151002" },
                    { 1419, false, "Yauyos", 137, "151001" },
                    { 1418, false, "Pachangara", 136, "150906" },
                    { 1417, false, "Navan", 136, "150905" },
                    { 1416, false, "Cochamarca", 136, "150904" },
                    { 1415, false, "Caujul", 136, "150903" },
                    { 1414, false, "Andajes", 136, "150902" },
                    { 1413, false, "Oyon", 136, "150901" },
                    { 1412, false, "Vegueta", 135, "150812" },
                    { 1411, false, "Sayan", 135, "150811" },
                    { 1410, false, "Santa María", 135, "150810" },
                    { 1409, false, "Santa Leonor", 135, "150809" },
                    { 1408, false, "Paccho", 135, "150808" },
                    { 1433, false, "Huangascar", 137, "151015" },
                    { 1434, false, "Huantan", 137, "151016" },
                    { 1435, false, "Huañec", 137, "151017" },
                    { 1436, false, "Laraos", 137, "151018" },
                    { 1462, false, "San Juan Bautista", 138, "160113" },
                    { 1461, false, "Belén", 138, "160112" },
                    { 1460, false, "Torres Causana", 138, "160110" },
                    { 1459, false, "Punchana", 138, "160108" },
                    { 1458, false, "Napo", 138, "160107" },
                    { 1457, false, "Mazan", 138, "160106" },
                    { 1456, false, "Las Amazonas", 138, "160105" },
                    { 1455, false, "Indiana", 138, "160104" },
                    { 1454, false, "Fernando Lores", 138, "160103" },
                    { 1453, false, "Alto Nanay", 138, "160102" },
                    { 1452, false, "Iquitos", 138, "160101" },
                    { 1451, false, "Vitis", 137, "151033" },
                    { 1521, false, "Torata", 149, "180106" },
                    { 1450, false, "Viñac", 137, "151032" },
                    { 1448, false, "Tomas", 137, "151030" },
                    { 1447, false, "Tauripampa", 137, "151029" },
                    { 1446, false, "Tanta", 137, "151028" },
                    { 1445, false, "San Pedro de Pilas", 137, "151027" },
                    { 1444, false, "San Joaquín", 137, "151026" },
                    { 1443, false, "Quinocay", 137, "151025" },
                    { 1442, false, "Quinches", 137, "151024" },
                    { 1441, false, "Putinza", 137, "151023" },
                    { 1440, false, "Omas", 137, "151022" },
                    { 1439, false, "Miraflores", 137, "151021" },
                    { 1438, false, "Madean", 137, "151020" },
                    { 1437, false, "Lincha", 137, "151019" },
                    { 1449, false, "Tupe", 137, "151031" },
                    { 1522, false, "Omate", 150, "180201" },
                    { 1523, false, "Chojata", 150, "180202" },
                    { 1524, false, "Coalaque", 150, "180203" },
                    { 1608, false, "Tamarindo", 159, "200506" },
                    { 1607, false, "La Huaca", 159, "200505" },
                    { 1606, false, "Colan", 159, "200504" },
                    { 1605, false, "Arenal", 159, "200503" },
                    { 1604, false, "Amotape", 159, "200502" },
                    { 1603, false, "Paita", 159, "200501" },
                    { 1602, false, "Yamango", 158, "200410" },
                    { 1601, false, "Santo Domingo", 158, "200409" },
                    { 1600, false, "Santa Catalina de Mossa", 158, "200408" },
                    { 1599, false, "San Juan de Bigote", 158, "200407" },
                    { 1598, false, "Salitral", 158, "200406" },
                    { 1597, false, "Morropon", 158, "200405" },
                    { 1596, false, "La Matanza", 158, "200404" },
                    { 1595, false, "Chalaco", 158, "200403" },
                    { 1594, false, "Buenos Aires", 158, "200402" },
                    { 1593, false, "Chulucanas", 158, "200401" },
                    { 1592, false, "Sondorillo", 157, "200308" },
                    { 1591, false, "Sondor", 157, "200307" }
                });

            migrationBuilder.InsertData(
                schema: "common",
                table: "Distrito",
                columns: new[] { "DistritoId", "Eliminado", "Nombre", "ProvinciaId", "Ubigeo" },
                values: new object[,]
                {
                    { 1590, false, "San Miguel de El Faique", 157, "200306" },
                    { 1589, false, "Lalaquiz", 157, "200305" },
                    { 1588, false, "Huarmaca", 157, "200304" },
                    { 1587, false, "El Carmen de la Frontera", 157, "200303" },
                    { 1586, false, "Canchaque", 157, "200302" },
                    { 1585, false, "Huancabamba", 157, "200301" },
                    { 1584, false, "Suyo", 156, "200210" },
                    { 1609, false, "Vichayal", 159, "200507" },
                    { 1610, false, "Sullana", 160, "200601" },
                    { 1611, false, "Bellavista", 160, "200602" },
                    { 1612, false, "Ignacio Escudero", 160, "200603" },
                    { 1638, false, "Mañazo", 163, "210109" },
                    { 1637, false, "Huata", 163, "210108" },
                    { 1636, false, "Coata", 163, "210107" },
                    { 1635, false, "Chucuito", 163, "210106" },
                    { 1634, false, "Capachica", 163, "210105" },
                    { 1633, false, "Atuncolla", 163, "210104" },
                    { 1632, false, "Amantani", 163, "210103" },
                    { 1631, false, "Acora", 163, "210102" },
                    { 1630, false, "Puno", 163, "210101" },
                    { 1629, false, "Rinconada Llicuar", 162, "200806" },
                    { 1628, false, "Vice", 162, "200805" },
                    { 1627, false, "Cristo Nos Valga", 162, "200804" },
                    { 1583, false, "Sicchez", 156, "200209" },
                    { 1626, false, "Bernal", 162, "200803" },
                    { 1624, false, "Sechura", 162, "200801" },
                    { 1623, false, "Mancora", 161, "200706" },
                    { 1622, false, "Los Organos", 161, "200705" },
                    { 1621, false, "Lobitos", 161, "200704" },
                    { 1620, false, "La Brea", 161, "200703" },
                    { 1619, false, "El Alto", 161, "200702" },
                    { 1618, false, "Pariñas", 161, "200701" },
                    { 1617, false, "Salitral", 160, "200608" },
                    { 1616, false, "Querecotillo", 160, "200607" },
                    { 1615, false, "Miguel Checa", 160, "200606" },
                    { 1614, false, "Marcavelica", 160, "200605" },
                    { 1613, false, "Lancones", 160, "200604" },
                    { 1625, false, "Bellavista de la Unión", 162, "200802" },
                    { 939, false, "Huacaybamba", 91, "100401" },
                    { 1582, false, "Sapillica", 156, "200208" },
                    { 1580, false, "Pacaipampa", 156, "200206" },
                    { 1549, false, "Yanahuanca", 153, "190201" },
                    { 1548, false, "Yanacancha", 152, "190113" },
                    { 1547, false, "Vicco", 152, "190112" },
                    { 1546, false, "Tinyahuarco", 152, "190111" },
                    { 1545, false, "Ticlacayan", 152, "190110" },
                    { 1544, false, "Simon Bolívar", 152, "190109" },
                    { 1543, false, "San Francisco de Asís de Yarusyacan", 152, "190108" },
                    { 1542, false, "Paucartambo", 152, "190107" },
                    { 1541, false, "Pallanchacra", 152, "190106" },
                    { 1540, false, "Ninacaca", 152, "190105" },
                    { 1539, false, "Huayllay", 152, "190104" },
                    { 1538, false, "Huariaca", 152, "190103" },
                    { 1537, false, "Huachon", 152, "190102" },
                    { 1536, false, "Chaupimarca", 152, "190101" },
                    { 1535, false, "Pacocha", 151, "180303" },
                    { 1534, false, "El Algarrobal", 151, "180302" },
                    { 1533, false, "Ilo", 151, "180301" },
                    { 1532, false, "Yunga", 150, "180211" },
                    { 1531, false, "Ubinas", 150, "180210" },
                    { 1530, false, "Quinistaquillas", 150, "180209" },
                    { 1529, false, "Puquina", 150, "180208" },
                    { 1528, false, "Matalaque", 150, "180207" },
                    { 1527, false, "Lloque", 150, "180206" },
                    { 1526, false, "La Capilla", 150, "180205" },
                    { 1525, false, "Ichuña", 150, "180204" },
                    { 1550, false, "Chacayan", 153, "190202" },
                    { 1551, false, "Goyllarisquizga", 153, "190203" },
                    { 1552, false, "Paucar", 153, "190204" },
                    { 1553, false, "San Pedro de Pillao", 153, "190205" },
                    { 1579, false, "Montero", 156, "200205" },
                    { 1578, false, "Lagunas", 156, "200204" },
                    { 1577, false, "Jilili", 156, "200203" },
                    { 1576, false, "Frias", 156, "200202" },
                    { 1575, false, "Ayabaca", 156, "200201" },
                    { 1574, false, "Veintiseis de Octubre", 155, "200115" },
                    { 1573, false, "Tambo Grande", 155, "200114" },
                    { 1572, false, "Las Lomas", 155, "200111" },
                    { 1571, false, "La Unión", 155, "200110" },
                    { 1570, false, "La Arena", 155, "200109" },
                    { 1569, false, "El Tallan", 155, "200108" },
                    { 1568, false, "Cura Mori", 155, "200107" },
                    { 1581, false, "Paimas", 156, "200207" },
                    { 1567, false, "Catacaos", 155, "200105" },
                    { 1565, false, "Piura", 155, "200101" },
                    { 1564, false, "Constitución", 154, "190308" },
                    { 1563, false, "Villa Rica", 154, "190307" },
                    { 1562, false, "Puerto Bermúdez", 154, "190306" },
                    { 1561, false, "Pozuzo", 154, "190305" },
                    { 1560, false, "Palcazu", 154, "190304" },
                    { 1559, false, "Huancabamba", 154, "190303" },
                    { 1558, false, "Chontabamba", 154, "190302" },
                    { 1557, false, "Oxapampa", 154, "190301" },
                    { 1556, false, "Vilcabamba", 153, "190208" },
                    { 1555, false, "Tapuc", 153, "190207" },
                    { 1554, false, "Santa Ana de Tusi", 153, "190206" },
                    { 1566, false, "Castilla", 155, "200104" },
                    { 938, false, "Yanas", 90, "100323" },
                    { 937, false, "Sillapata", 90, "100322" },
                    { 936, false, "Shunqui", 90, "100321" },
                    { 320, false, "Los Chankas", 33, "030611" },
                    { 319, false, "El Porvenir", 33, "030610" },
                    { 318, false, "Rocchacc", 33, "030609" },
                    { 317, false, "Ranracancha", 33, "030608" },
                    { 316, false, "Uranmarca", 33, "030607" },
                    { 315, false, "Ongoy", 33, "030606" },
                    { 314, false, "Ocobamba", 33, "030605" },
                    { 313, false, "Huaccana", 33, "030604" },
                    { 312, false, "Cocharcas", 33, "030603" },
                    { 311, false, "Anco_Huallo", 33, "030602" },
                    { 310, false, "Chincheros", 33, "030601" },
                    { 309, false, "Challhuahuacho", 32, "030506" },
                    { 308, false, "Mara", 32, "030505" },
                    { 307, false, "Haquira", 32, "030504" },
                    { 306, false, "Coyllurqui", 32, "030503" },
                    { 305, false, "Cotabambas", 32, "030502" },
                    { 304, false, "Tambobamba", 32, "030501" },
                    { 303, false, "Yanaca", 31, "030417" },
                    { 302, false, "Toraya", 31, "030416" },
                    { 301, false, "Tintay", 31, "030415" },
                    { 300, false, "Tapairihua", 31, "030414" },
                    { 299, false, "Soraya", 31, "030413" },
                    { 298, false, "Sañayca", 31, "030412" },
                    { 297, false, "San Juan de Chacña", 31, "030411" },
                    { 296, false, "Pocohuanca", 31, "030410" },
                    { 321, false, "Chuquibambilla", 34, "030701" },
                    { 295, false, "Lucre", 31, "030409" },
                    { 322, false, "Curpahuasi", 34, "030702" },
                    { 324, false, "Huayllati", 34, "030704" },
                    { 349, false, "Quequeña", 35, "040115" },
                    { 348, false, "Polobaya", 35, "040114" },
                    { 347, false, "Pocsi", 35, "040113" },
                    { 346, false, "Paucarpata", 35, "040112" },
                    { 345, false, "Mollebaya", 35, "040111" },
                    { 344, false, "Miraflores", 35, "040110" },
                    { 343, false, "Mariano Melgar", 35, "040109" },
                    { 342, false, "La Joya", 35, "040108" },
                    { 341, false, "Jacobo Hunter", 35, "040107" },
                    { 340, false, "Chiguata", 35, "040106" },
                    { 339, false, "Characato", 35, "040105" },
                    { 338, false, "Cerro Colorado", 35, "040104" },
                    { 337, false, "Cayma", 35, "040103" },
                    { 336, false, "Alto Selva Alegre", 35, "040102" },
                    { 335, false, "Arequipa", 35, "040101" },
                    { 334, false, "Curasco", 34, "030714" },
                    { 333, false, "Virundo", 34, "030713" },
                    { 332, false, "Vilcabamba", 34, "030712" },
                    { 331, false, "Turpay", 34, "030711" },
                    { 330, false, "Santa Rosa", 34, "030710" },
                    { 329, false, "San Antonio", 34, "030709" },
                    { 328, false, "Progreso", 34, "030708" },
                    { 327, false, "Pataypampa", 34, "030707" },
                    { 326, false, "Micaela Bastidas", 34, "030706" },
                    { 325, false, "Mamara", 34, "030705" },
                    { 323, false, "Gamarra", 34, "030703" },
                    { 294, false, "Justo Apu Sahuaraura", 31, "030408" },
                    { 293, false, "Ihuayllo", 31, "030407" },
                    { 292, false, "Cotaruse", 31, "030406" },
                    { 261, false, "Andarapa", 29, "030202" },
                    { 260, false, "Andahuaylas", 29, "030201" },
                    { 259, false, "Tamburco", 28, "030109" },
                    { 258, false, "San Pedro de Cachora", 28, "030108" },
                    { 257, false, "Pichirhua", 28, "030107" },
                    { 256, false, "Lambrama", 28, "030106" },
                    { 255, false, "Huanipaca", 28, "030105" },
                    { 254, false, "Curahuasi", 28, "030104" },
                    { 253, false, "Circa", 28, "030103" },
                    { 252, false, "Chacoche", 28, "030102" },
                    { 251, false, "Abancay", 28, "030101" },
                    { 250, false, "Yanama", 27, "022008" },
                    { 249, false, "Shupluy", 27, "022007" },
                    { 248, false, "Ranrahirca", 27, "022006" },
                    { 247, false, "Quillo", 27, "022005" },
                    { 246, false, "Matacoto", 27, "022004" },
                    { 245, false, "Mancos", 27, "022003" },
                    { 244, false, "Cascapara", 27, "022002" },
                    { 243, false, "Yungay", 27, "022001" },
                    { 242, false, "Sicsibamba", 26, "021910" },
                    { 241, false, "San Juan", 26, "021909" },
                    { 240, false, "Ragash", 26, "021908" },
                    { 239, false, "Quiches", 26, "021907" },
                    { 238, false, "Huayllabamba", 26, "021906" },
                    { 237, false, "Chingalpo", 26, "021905" },
                    { 262, false, "Chiara", 29, "030203" },
                    { 263, false, "Huancarama", 29, "030204" },
                    { 264, false, "Huancaray", 29, "030205" },
                    { 265, false, "Huayana", 29, "030206" },
                    { 291, false, "Colcabamba", 31, "030405" },
                    { 290, false, "Chapimarca", 31, "030404" },
                    { 289, false, "Caraybamba", 31, "030403" },
                    { 288, false, "Capaya", 31, "030402" },
                    { 287, false, "Chalhuanca", 31, "030401" },
                    { 286, false, "Sabaino", 30, "030307" },
                    { 285, false, "Pachaconas", 30, "030306" },
                    { 284, false, "Oropesa", 30, "030305" },
                    { 283, false, "Juan Espinoza Medrano", 30, "030304" },
                    { 282, false, "Huaquirca", 30, "030303" },
                    { 281, false, "El Oro", 30, "030302" },
                    { 280, false, "Antabamba", 30, "030301" },
                    { 350, false, "Sabandia", 35, "040116" },
                    { 279, false, "José María Arguedas", 29, "030220" },
                    { 277, false, "Turpo", 29, "030218" },
                    { 276, false, "Tumay Huaraca", 29, "030217" },
                    { 275, false, "Talavera", 29, "030216" },
                    { 274, false, "Santa María de Chicmo", 29, "030215" },
                    { 273, false, "San Miguel de Chaccrampa", 29, "030214" },
                    { 272, false, "San Jerónimo", 29, "030213" },
                    { 271, false, "San Antonio de Cachi", 29, "030212" },
                    { 270, false, "Pomacocha", 29, "030211" },
                    { 269, false, "Pampachiri", 29, "030210" },
                    { 268, false, "Pacucha", 29, "030209" },
                    { 267, false, "Pacobamba", 29, "030208" },
                    { 266, false, "Kishuara", 29, "030207" },
                    { 278, false, "Kaquiabamba", 29, "030219" },
                    { 236, false, "Cashapampa", 26, "021904" },
                    { 351, false, "Sachaca", 35, "040117" },
                    { 353, false, "San Juan de Tarucani", 35, "040119" },
                    { 437, false, "Pampamarca", 42, "040805" },
                    { 436, false, "Huaynacotas", 42, "040804" },
                    { 435, false, "Charcana", 42, "040803" },
                    { 434, false, "Alca", 42, "040802" },
                    { 433, false, "Cotahuasi", 42, "040801" },
                    { 432, false, "Punta de Bombón", 41, "040706" },
                    { 431, false, "Mejia", 41, "040705" },
                    { 430, false, "Islay", 41, "040704" },
                    { 429, false, "Dean Valdivia", 41, "040703" },
                    { 428, false, "Cocachacra", 41, "040702" },
                    { 427, false, "Mollendo", 41, "040701" },
                    { 426, false, "Yanaquihua", 40, "040608" },
                    { 425, false, "Salamanca", 40, "040607" },
                    { 424, false, "Río Grande", 40, "040606" },
                    { 423, false, "Iray", 40, "040605" },
                    { 422, false, "Chichas", 40, "040604" },
                    { 421, false, "Cayarani", 40, "040603" },
                    { 420, false, "Andaray", 40, "040602" },
                    { 419, false, "Chuquibamba", 40, "040601" },
                    { 418, false, "Majes", 39, "040520" },
                    { 417, false, "Yanque", 39, "040519" },
                    { 416, false, "Tuti", 39, "040518" },
                    { 415, false, "Tisco", 39, "040517" },
                    { 414, false, "Tapay", 39, "040516" },
                    { 413, false, "Sibayo", 39, "040515" },
                    { 438, false, "Puyca", 42, "040806" },
                    { 412, false, "San Antonio de Chuca", 39, "040514" },
                    { 439, false, "Quechualla", 42, "040807" },
                    { 441, false, "Tauria", 42, "040809" },
                    { 466, false, "Sancos", 45, "050301" },
                    { 465, false, "Totos", 44, "050206" },
                    { 464, false, "Paras", 44, "050205" },
                    { 463, false, "María Parado de Bellido", 44, "050204" },
                    { 462, false, "Los Morochucos", 44, "050203" },
                    { 461, false, "Chuschi", 44, "050202" },
                    { 460, false, "Cangallo", 44, "050201" },
                    { 459, false, "Andrés Avelino Cáceres Dorregaray", 43, "050116" },
                    { 458, false, "Jesús Nazareno", 43, "050115" },
                    { 457, false, "Vinchos", 43, "050114" },
                    { 456, false, "Tambillo", 43, "050113" },
                    { 455, false, "Socos", 43, "050112" },
                    { 454, false, "Santiago de Pischa", 43, "050111" },
                    { 453, false, "San Juan Bautista", 43, "050110" },
                    { 452, false, "San José de Ticllas", 43, "050109" },
                    { 451, false, "Quinua", 43, "050108" },
                    { 450, false, "Pacaycasa", 43, "050107" },
                    { 449, false, "Ocros", 43, "050106" },
                    { 448, false, "Chiara", 43, "050105" },
                    { 447, false, "Carmen Alto", 43, "050104" },
                    { 446, false, "Acos Vinchos", 43, "050103" },
                    { 445, false, "Acocro", 43, "050102" },
                    { 444, false, "Ayacucho", 43, "050101" },
                    { 443, false, "Toro", 42, "040811" },
                    { 442, false, "Tomepampa", 42, "040810" },
                    { 440, false, "Sayla", 42, "040808" },
                    { 411, false, "Madrigal", 39, "040513" },
                    { 410, false, "Maca", 39, "040512" },
                    { 409, false, "Lluta", 39, "040511" },
                    { 378, false, "Chala", 37, "040307" },
                    { 377, false, "Cahuacho", 37, "040306" },
                    { 376, false, "Bella Unión", 37, "040305" },
                    { 375, false, "Atiquipa", 37, "040304" },
                    { 374, false, "Atico", 37, "040303" },
                    { 373, false, "Acarí", 37, "040302" },
                    { 372, false, "Caravelí", 37, "040301" },
                    { 371, false, "Samuel Pastor", 36, "040208" },
                    { 370, false, "Quilca", 36, "040207" },
                    { 369, false, "Ocoña", 36, "040206" },
                    { 368, false, "Nicolás de Pierola", 36, "040205" },
                    { 367, false, "Mariscal Cáceres", 36, "040204" },
                    { 366, false, "Mariano Nicolás Valcárcel", 36, "040203" },
                    { 365, false, "José María Quimper", 36, "040202" },
                    { 364, false, "Camaná", 36, "040201" },
                    { 363, false, "José Luis Bustamante Y Rivero", 35, "040129" },
                    { 362, false, "Yura", 35, "040128" },
                    { 361, false, "Yarabamba", 35, "040127" },
                    { 360, false, "Yanahuara", 35, "040126" },
                    { 359, false, "Vitor", 35, "040125" },
                    { 358, false, "Uchumayo", 35, "040124" },
                    { 357, false, "Tiabaya", 35, "040123" },
                    { 356, false, "Socabaya", 35, "040122" },
                    { 355, false, "Santa Rita de Siguas", 35, "040121" },
                    { 354, false, "Santa Isabel de Siguas", 35, "040120" },
                    { 379, false, "Chaparra", 37, "040308" },
                    { 380, false, "Huanuhuanu", 37, "040309" },
                    { 381, false, "Jaqui", 37, "040310" },
                    { 382, false, "Lomas", 37, "040311" },
                    { 408, false, "Lari", 39, "040510" },
                    { 407, false, "Ichupampa", 39, "040509" },
                    { 406, false, "Huanca", 39, "040508" },
                    { 405, false, "Huambo", 39, "040507" },
                    { 404, false, "Coporaque", 39, "040506" },
                    { 403, false, "Caylloma", 39, "040505" },
                    { 402, false, "Callalli", 39, "040504" },
                    { 401, false, "Cabanaconde", 39, "040503" },
                    { 400, false, "Achoma", 39, "040502" },
                    { 399, false, "Chivay", 39, "040501" },
                    { 398, false, "Viraco", 38, "040414" },
                    { 397, false, "Uraca", 38, "040413" },
                    { 352, false, "San Juan de Siguas", 35, "040118" },
                    { 396, false, "Uñon", 38, "040412" },
                    { 394, false, "Pampacolca", 38, "040410" },
                    { 393, false, "Orcopampa", 38, "040409" },
                    { 392, false, "Machaguay", 38, "040408" },
                    { 391, false, "Huancarqui", 38, "040407" },
                    { 390, false, "Choco", 38, "040406" },
                    { 389, false, "Chilcaymarca", 38, "040405" },
                    { 388, false, "Chachas", 38, "040404" },
                    { 387, false, "Ayo", 38, "040403" },
                    { 386, false, "Andagua", 38, "040402" },
                    { 385, false, "Aplao", 38, "040401" },
                    { 384, false, "Yauca", 37, "040313" },
                    { 383, false, "Quicacha", 37, "040312" },
                    { 395, false, "Tipan", 38, "040411" },
                    { 235, false, "Alfonso Ugarte", 26, "021903" },
                    { 234, false, "Acobamba", 26, "021902" },
                    { 233, false, "Sihuas", 26, "021901" },
                    { 85, false, "Huaraz", 8, "020101" },
                    { 84, false, "Yamon", 7, "010707" },
                    { 83, false, "Lonya Grande", 7, "010706" },
                    { 82, false, "Jamalca", 7, "010705" },
                    { 81, false, "El Milagro", 7, "010704" },
                    { 80, false, "Cumba", 7, "010703" },
                    { 79, false, "Cajaruro", 7, "010702" },
                    { 78, false, "Bagua Grande", 7, "010701" },
                    { 77, false, "Vista Alegre", 6, "010612" },
                    { 76, false, "Totora", 6, "010611" },
                    { 75, false, "Santa Rosa", 6, "010610" },
                    { 74, false, "Omia", 6, "010609" },
                    { 73, false, "Milpuc", 6, "010608" },
                    { 72, false, "Mariscal Benavides", 6, "010607" },
                    { 71, false, "Longar", 6, "010606" },
                    { 70, false, "Limabamba", 6, "010605" },
                    { 69, false, "Huambo", 6, "010604" },
                    { 68, false, "Cochamal", 6, "010603" },
                    { 67, false, "Chirimoto", 6, "010602" },
                    { 66, false, "San Nicolás", 6, "010601" },
                    { 65, false, "Trita", 5, "010523" },
                    { 64, false, "Tingo", 5, "010522" },
                    { 63, false, "Santo Tomas", 5, "010521" },
                    { 62, false, "Santa Catalina", 5, "010520" },
                    { 61, false, "San Juan de Lopecancha", 5, "010519" },
                    { 86, false, "Cochabamba", 8, "020102" },
                    { 60, false, "San Jerónimo", 5, "010518" },
                    { 87, false, "Colcabamba", 8, "020103" },
                    { 89, false, "Independencia", 8, "020105" },
                    { 114, false, "Cajacay", 12, "020505" },
                    { 113, false, "Aquia", 12, "020504" },
                    { 112, false, "Antonio Raymondi", 12, "020503" },
                    { 111, false, "Abelardo Pardo Lezameta", 12, "020502" },
                    { 110, false, "Chiquian", 12, "020501" },
                    { 109, false, "Acochaca", 11, "020402" },
                    { 108, false, "Chacas", 11, "020401" },
                    { 107, false, "San Juan de Rontoy", 10, "020306" },
                    { 106, false, "Mirgas", 10, "020305" },
                    { 105, false, "Chingas", 10, "020304" },
                    { 104, false, "Chaccho", 10, "020303" },
                    { 103, false, "Aczo", 10, "020302" },
                    { 102, false, "Llamellin", 10, "020301" },
                    { 101, false, "Succha", 9, "020205" },
                    { 100, false, "La Merced", 9, "020204" },
                    { 99, false, "Huacllan", 9, "020203" },
                    { 98, false, "Coris", 9, "020202" },
                    { 97, false, "Aija", 9, "020201" },
                    { 96, false, "Tarica", 8, "020112" },
                    { 95, false, "Pira", 8, "020111" },
                    { 94, false, "Pariacoto", 8, "020110" },
                    { 93, false, "Pampas Grande", 8, "020109" },
                    { 92, false, "Olleros", 8, "020108" },
                    { 91, false, "La Libertad", 8, "020107" },
                    { 90, false, "Jangas", 8, "020106" },
                    { 88, false, "Huanchay", 8, "020104" },
                    { 59, false, "San Francisco de Yeso", 5, "010517" },
                    { 58, false, "San Cristóbal", 5, "010516" },
                    { 57, false, "Providencia", 5, "010515" },
                    { 26, false, "Imaza", 2, "010205" },
                    { 25, false, "El Parco", 2, "010204" },
                    { 24, false, "Copallin", 2, "010203" },
                    { 23, false, "Aramango", 2, "010202" },
                    { 22, false, "Bagua", 2, "010201" },
                    { 21, false, "Sonche", 1, "010121" },
                    { 20, false, "Soloco", 1, "010120" },
                    { 19, false, "San Isidro de Maino", 1, "010119" },
                    { 18, false, "San Francisco de Daguas", 1, "010118" },
                    { 17, false, "Quinjalca", 1, "010117" },
                    { 16, false, "Olleros", 1, "010116" },
                    { 15, false, "Montevideo", 1, "010115" },
                    { 14, false, "Molinopampa", 1, "010114" },
                    { 13, false, "Mariscal Castilla", 1, "010113" },
                    { 12, false, "Magdalena", 1, "010112" },
                    { 11, false, "Levanto", 1, "010111" },
                    { 10, false, "Leimebamba", 1, "010110" },
                    { 9, false, "La Jalca", 1, "010109" },
                    { 8, false, "Huancas", 1, "010108" },
                    { 7, false, "Granada", 1, "010107" },
                    { 6, false, "Chuquibamba", 1, "010106" },
                    { 5, false, "Chiliquin", 1, "010105" },
                    { 4, false, "Cheto", 1, "010104" },
                    { 3, false, "Balsas", 1, "010103" },
                    { 2, false, "Asunción", 1, "010102" },
                    { 27, false, "La Peca", 2, "010206" },
                    { 28, false, "Jumbilla", 3, "010301" }
                });

            migrationBuilder.InsertData(
                schema: "common",
                table: "Distrito",
                columns: new[] { "DistritoId", "Eliminado", "Nombre", "ProvinciaId", "Ubigeo" },
                values: new object[,]
                {
                    { 29, false, "Chisquilla", 3, "010302" },
                    { 30, false, "Churuja", 3, "010303" },
                    { 56, false, "Pisuquia", 5, "010514" },
                    { 55, false, "Ocumal", 5, "010513" },
                    { 54, false, "Ocalli", 5, "010512" },
                    { 53, false, "María", 5, "010511" },
                    { 52, false, "Luya Viejo", 5, "010510" },
                    { 51, false, "Luya", 5, "010509" },
                    { 50, false, "Lonya Chico", 5, "010508" },
                    { 49, false, "Longuita", 5, "010507" },
                    { 48, false, "Inguilpata", 5, "010506" },
                    { 47, false, "Conila", 5, "010505" },
                    { 46, false, "Colcamar", 5, "010504" },
                    { 45, false, "Cocabamba", 5, "010503" },
                    { 115, false, "Canis", 12, "020506" },
                    { 44, false, "Camporredondo", 5, "010502" },
                    { 42, false, "Río Santiago", 4, "010403" },
                    { 41, false, "El Cenepa", 4, "010402" },
                    { 40, false, "Nieva", 4, "010401" },
                    { 39, false, "Yambrasbamba", 3, "010312" },
                    { 38, false, "Valera", 3, "010311" },
                    { 37, false, "Shipasbamba", 3, "010310" },
                    { 36, false, "San Carlos", 3, "010309" },
                    { 35, false, "Recta", 3, "010308" },
                    { 34, false, "Jazan", 3, "010307" },
                    { 33, false, "Florida", 3, "010306" },
                    { 32, false, "Cuispes", 3, "010305" },
                    { 31, false, "Corosha", 3, "010304" },
                    { 43, false, "Lamud", 5, "010501" },
                    { 116, false, "Colquioc", 12, "020507" },
                    { 117, false, "Huallanca", 12, "020508" },
                    { 118, false, "Huasta", 12, "020509" },
                    { 202, false, "Huacaschuque", 22, "021504" },
                    { 201, false, "Conchucos", 22, "021503" },
                    { 200, false, "Bolognesi", 22, "021502" },
                    { 199, false, "Cabana", 22, "021501" },
                    { 198, false, "Santiago de Chilcas", 21, "021410" },
                    { 197, false, "San Pedro", 21, "021409" },
                    { 196, false, "San Cristóbal de Rajan", 21, "021408" },
                    { 195, false, "Llipa", 21, "021407" },
                    { 194, false, "Congas", 21, "021406" },
                    { 193, false, "Cochas", 21, "021405" },
                    { 192, false, "Carhuapampa", 21, "021404" },
                    { 191, false, "Cajamarquilla", 21, "021403" },
                    { 190, false, "Acas", 21, "021402" },
                    { 189, false, "Ocros", 21, "021401" },
                    { 188, false, "Musga", 20, "021308" },
                    { 187, false, "Lucma", 20, "021307" },
                    { 186, false, "Llumpa", 20, "021306" },
                    { 185, false, "Llama", 20, "021305" },
                    { 184, false, "Fidel Olivas Escudero", 20, "021304" },
                    { 183, false, "Eleazar Guzmán Barron", 20, "021303" },
                    { 182, false, "Casca", 20, "021302" },
                    { 181, false, "Piscobamba", 20, "021301" },
                    { 180, false, "Yuracmarca", 19, "021210" },
                    { 179, false, "Santo Toribio", 19, "021209" },
                    { 178, false, "Santa Cruz", 19, "021208" },
                    { 203, false, "Huandoval", 22, "021505" },
                    { 204, false, "Lacabamba", 22, "021506" },
                    { 205, false, "Llapo", 22, "021507" },
                    { 206, false, "Pallasca", 22, "021508" },
                    { 232, false, "Nuevo Chimbote", 25, "021809" },
                    { 231, false, "Santa", 25, "021808" },
                    { 230, false, "Samanco", 25, "021807" },
                    { 229, false, "Nepeña", 25, "021806" },
                    { 228, false, "Moro", 25, "021805" },
                    { 227, false, "Macate", 25, "021804" },
                    { 226, false, "Coishco", 25, "021803" },
                    { 225, false, "Cáceres del Perú", 25, "021802" },
                    { 224, false, "Chimbote", 25, "021801" },
                    { 223, false, "Ticapampa", 24, "021710" },
                    { 222, false, "Tapacocha", 24, "021709" },
                    { 221, false, "Pararin", 24, "021708" },
                    { 177, false, "Pueblo Libre", 19, "021207" },
                    { 220, false, "Pampas Chico", 24, "021707" },
                    { 218, false, "Llacllin", 24, "021705" },
                    { 217, false, "Huayllapampa", 24, "021704" },
                    { 216, false, "Cotaparaco", 24, "021703" },
                    { 215, false, "Catac", 24, "021702" },
                    { 214, false, "Recuay", 24, "021701" },
                    { 213, false, "Quinuabamba", 23, "021604" },
                    { 212, false, "Parobamba", 23, "021603" },
                    { 211, false, "Huayllan", 23, "021602" },
                    { 210, false, "Pomabamba", 23, "021601" },
                    { 209, false, "Tauca", 22, "021511" },
                    { 208, false, "Santa Rosa", 22, "021510" },
                    { 207, false, "Pampas", 22, "021509" },
                    { 219, false, "Marca", 24, "021706" },
                    { 467, false, "Carapo", 45, "050302" },
                    { 176, false, "Pamparomas", 19, "021206" },
                    { 174, false, "Huaylas", 19, "021204" },
                    { 143, false, "Corongo", 16, "020901" },
                    { 142, false, "Yautan", 15, "020804" },
                    { 141, false, "Comandante Noel", 15, "020803" },
                    { 140, false, "Buena Vista Alta", 15, "020802" },
                    { 139, false, "Casma", 15, "020801" },
                    { 138, false, "Yauya", 14, "020703" },
                    { 137, false, "San Nicolás", 14, "020702" },
                    { 136, false, "San Luis", 14, "020701" },
                    { 135, false, "Yungar", 13, "020611" },
                    { 134, false, "Tinco", 13, "020610" },
                    { 133, false, "Shilla", 13, "020609" },
                    { 132, false, "San Miguel de Aco", 13, "020608" },
                    { 131, false, "Pariahuanca", 13, "020607" },
                    { 130, false, "Marcara", 13, "020606" },
                    { 129, false, "Ataquero", 13, "020605" },
                    { 128, false, "Anta", 13, "020604" },
                    { 127, false, "Amashca", 13, "020603" },
                    { 126, false, "Acopampa", 13, "020602" },
                    { 125, false, "Carhuaz", 13, "020601" },
                    { 124, false, "Ticllos", 12, "020515" },
                    { 123, false, "San Miguel de Corpanqui", 12, "020514" },
                    { 122, false, "Pacllon", 12, "020513" },
                    { 121, false, "Mangas", 12, "020512" },
                    { 120, false, "La Primavera", 12, "020511" },
                    { 119, false, "Huayllacayan", 12, "020510" },
                    { 144, false, "Aco", 16, "020902" },
                    { 145, false, "Bambas", 16, "020903" },
                    { 146, false, "Cusca", 16, "020904" },
                    { 147, false, "La Pampa", 16, "020905" },
                    { 173, false, "Huata", 19, "021203" },
                    { 172, false, "Huallanca", 19, "021202" },
                    { 171, false, "Caraz", 19, "021201" },
                    { 170, false, "Malvas", 18, "021105" },
                    { 169, false, "Huayan", 18, "021104" },
                    { 168, false, "Culebras", 18, "021103" },
                    { 167, false, "Cochapeti", 18, "021102" },
                    { 166, false, "Huarmey", 18, "021101" },
                    { 165, false, "Uco", 17, "021016" },
                    { 164, false, "San Pedro de Chana", 17, "021015" },
                    { 163, false, "San Marcos", 17, "021014" },
                    { 162, false, "Rapayan", 17, "021013" },
                    { 175, false, "Mato", 19, "021205" },
                    { 161, false, "Rahuapampa", 17, "021012" },
                    { 159, false, "Paucas", 17, "021010" },
                    { 158, false, "Masin", 17, "021009" },
                    { 157, false, "Huantar", 17, "021008" },
                    { 156, false, "Huachis", 17, "021007" },
                    { 155, false, "Huacchis", 17, "021006" },
                    { 154, false, "Huacachi", 17, "021005" },
                    { 153, false, "Chavin de Huantar", 17, "021004" },
                    { 152, false, "Cajay", 17, "021003" },
                    { 151, false, "Anra", 17, "021002" },
                    { 150, false, "Huari", 17, "021001" },
                    { 149, false, "Yupan", 16, "020907" },
                    { 148, false, "Yanac", 16, "020906" },
                    { 160, false, "Ponto", 17, "021011" },
                    { 1873, false, "Alexander Von Humboldt", 195, "250305" },
                    { 468, false, "Sacsamarca", 45, "050303" },
                    { 470, false, "Huanta", 46, "050401" },
                    { 789, false, "Kosñipata", 78, "081106" },
                    { 788, false, "Huancarani", 78, "081105" },
                    { 787, false, "Colquepata", 78, "081104" },
                    { 786, false, "Challabamba", 78, "081103" },
                    { 785, false, "Caicay", 78, "081102" },
                    { 784, false, "Paucartambo", 78, "081101" },
                    { 783, false, "Yaurisque", 77, "081009" },
                    { 782, false, "Pillpinto", 77, "081008" },
                    { 781, false, "Paccaritambo", 77, "081007" },
                    { 780, false, "Omacha", 77, "081006" },
                    { 779, false, "Huanoquite", 77, "081005" },
                    { 778, false, "Colcha", 77, "081004" },
                    { 777, false, "Ccapi", 77, "081003" },
                    { 776, false, "Accha", 77, "081002" },
                    { 775, false, "Paruro", 77, "081001" },
                    { 774, false, "Megantoni", 76, "080914" },
                    { 773, false, "Villa Kintiarina", 76, "080913" },
                    { 772, false, "Villa Virgen", 76, "080912" },
                    { 771, false, "Inkawasi", 76, "080911" },
                    { 770, false, "Pichari", 76, "080910" },
                    { 769, false, "Vilcabamba", 76, "080909" },
                    { 768, false, "Santa Teresa", 76, "080908" },
                    { 767, false, "Kimbiri", 76, "080907" },
                    { 766, false, "Quellouno", 76, "080906" },
                    { 765, false, "Ocobamba", 76, "080905" },
                    { 790, false, "Urcos", 79, "081201" },
                    { 764, false, "Maranura", 76, "080904" },
                    { 791, false, "Andahuaylillas", 79, "081202" },
                    { 793, false, "Ccarhuayo", 79, "081204" },
                    { 818, false, "Manta", 81, "090110" },
                    { 817, false, "Laria", 81, "090109" },
                    { 816, false, "Izcuchaca", 81, "090108" },
                    { 815, false, "Huayllahuara", 81, "090107" },
                    { 814, false, "Huachocolpa", 81, "090106" },
                    { 813, false, "Cuenca", 81, "090105" },
                    { 812, false, "Conayca", 81, "090104" },
                    { 811, false, "Acoria", 81, "090103" },
                    { 810, false, "Acobambilla", 81, "090102" },
                    { 809, false, "Huancavelica", 81, "090101" },
                    { 808, false, "Yucay", 80, "081307" },
                    { 807, false, "Ollantaytambo", 80, "081306" },
                    { 806, false, "Maras", 80, "081305" },
                    { 805, false, "Machupicchu", 80, "081304" },
                    { 804, false, "Huayllabamba", 80, "081303" },
                    { 803, false, "Chinchero", 80, "081302" },
                    { 802, false, "Urubamba", 80, "081301" },
                    { 801, false, "Quiquijana", 79, "081212" },
                    { 800, false, "Oropesa", 79, "081211" },
                    { 799, false, "Ocongate", 79, "081210" },
                    { 798, false, "Marcapata", 79, "081209" },
                    { 797, false, "Lucre", 79, "081208" },
                    { 796, false, "Huaro", 79, "081207" },
                    { 795, false, "Cusipata", 79, "081206" },
                    { 794, false, "Ccatca", 79, "081205" },
                    { 792, false, "Camanti", 79, "081203" },
                    { 763, false, "Huayopata", 76, "080903" },
                    { 762, false, "Echarate", 76, "080902" },
                    { 761, false, "Santa Ana", 76, "080901" },
                    { 730, false, "Checca", 72, "080502" },
                    { 729, false, "Yanaoca", 72, "080501" },
                    { 728, false, "Yanatile", 71, "080408" },
                    { 727, false, "Taray", 71, "080407" },
                    { 726, false, "San Salvador", 71, "080406" },
                    { 725, false, "Pisac", 71, "080405" },
                    { 724, false, "Lares", 71, "080404" },
                    { 723, false, "Lamay", 71, "080403" },
                    { 722, false, "Coya", 71, "080402" },
                    { 721, false, "Calca", 71, "080401" },
                    { 720, false, "Zurite", 70, "080309" },
                    { 719, false, "Pucyura", 70, "080308" },
                    { 718, false, "Mollepata", 70, "080307" },
                    { 717, false, "Limatambo", 70, "080306" },
                    { 716, false, "Huarocondo", 70, "080305" },
                    { 715, false, "Chinchaypujio", 70, "080304" },
                    { 714, false, "Cachimayo", 70, "080303" },
                    { 713, false, "Ancahuasi", 70, "080302" },
                    { 712, false, "Anta", 70, "080301" },
                    { 711, false, "Sangarara", 69, "080207" },
                    { 710, false, "Rondocan", 69, "080206" },
                    { 709, false, "Pomacanchi", 69, "080205" },
                    { 708, false, "Mosoc Llacta", 69, "080204" },
                    { 707, false, "Acos", 69, "080203" },
                    { 706, false, "Acopia", 69, "080202" },
                    { 731, false, "Kunturkanki", 72, "080503" },
                    { 732, false, "Langui", 72, "080504" },
                    { 733, false, "Layo", 72, "080505" },
                    { 734, false, "Pampamarca", 72, "080506" },
                    { 760, false, "Alto Pichigua", 75, "080808" },
                    { 759, false, "Suyckutambo", 75, "080807" },
                    { 758, false, "Pichigua", 75, "080806" },
                    { 757, false, "Pallpata", 75, "080805" },
                    { 756, false, "Ocoruro", 75, "080804" },
                    { 755, false, "Coporaque", 75, "080803" },
                    { 754, false, "Condoroma", 75, "080802" },
                    { 753, false, "Espinar", 75, "080801" },
                    { 752, false, "Velille", 74, "080708" },
                    { 751, false, "Quiñota", 74, "080707" },
                    { 750, false, "Llusco", 74, "080706" },
                    { 749, false, "Livitaca", 74, "080705" },
                    { 819, false, "Mariscal Cáceres", 81, "090111" },
                    { 748, false, "Colquemarca", 74, "080704" },
                    { 746, false, "Capacmarca", 74, "080702" },
                    { 745, false, "Santo Tomas", 74, "080701" },
                    { 744, false, "Tinta", 73, "080608" },
                    { 743, false, "San Pedro", 73, "080607" },
                    { 742, false, "San Pablo", 73, "080606" },
                    { 741, false, "Pitumarca", 73, "080605" },
                    { 740, false, "Marangani", 73, "080604" },
                    { 739, false, "Combapata", 73, "080603" },
                    { 738, false, "Checacupe", 73, "080602" },
                    { 737, false, "Sicuani", 73, "080601" },
                    { 736, false, "Tupac Amaru", 72, "080508" },
                    { 735, false, "Quehue", 72, "080507" },
                    { 747, false, "Chamaca", 74, "080703" },
                    { 705, false, "Acomayo", 69, "080201" },
                    { 820, false, "Moya", 81, "090112" },
                    { 822, false, "Palca", 81, "090114" },
                    { 906, false, "Roble", 87, "090721" },
                    { 905, false, "Andaymarca", 87, "090720" },
                    { 904, false, "Quichuas", 87, "090719" },
                    { 903, false, "Tintay Puncu", 87, "090718" },
                    { 902, false, "Surcubamba", 87, "090717" },
                    { 901, false, "San Marcos de Rocchac", 87, "090716" },
                    { 900, false, "Salcahuasi", 87, "090715" },
                    { 899, false, "Salcabamba", 87, "090714" },
                    { 898, false, "Quishuar", 87, "090713" },
                    { 897, false, "Pazos", 87, "090711" },
                    { 896, false, "Ñahuimpuquio", 87, "090710" },
                    { 895, false, "Huaribamba", 87, "090709" },
                    { 894, false, "Huachocolpa", 87, "090707" },
                    { 893, false, "Daniel Hernández", 87, "090706" },
                    { 892, false, "Colcabamba", 87, "090705" },
                    { 891, false, "Ahuaycha", 87, "090704" },
                    { 890, false, "Acraquia", 87, "090703" },
                    { 889, false, "Acostambo", 87, "090702" },
                    { 888, false, "Pampas", 87, "090701" },
                    { 887, false, "Tambo", 86, "090616" },
                    { 886, false, "Santo Domingo de Capillas", 86, "090615" },
                    { 885, false, "Santiago de Quirahuara", 86, "090614" },
                    { 884, false, "Santiago de Chocorvos", 86, "090613" },
                    { 883, false, "San Isidro", 86, "090612" },
                    { 882, false, "San Francisco de Sangayaico", 86, "090611" },
                    { 907, false, "Pichos", 87, "090722" },
                    { 881, false, "San Antonio de Cusicancha", 86, "090610" },
                    { 908, false, "Santiago de Tucuma", 87, "090723" },
                    { 910, false, "Amarilis", 88, "100102" },
                    { 935, false, "Ripan", 90, "100317" },
                    { 934, false, "Quivilla", 90, "100316" },
                    { 933, false, "Pachas", 90, "100313" },
                    { 932, false, "Marías", 90, "100311" },
                    { 931, false, "Chuquis", 90, "100307" },
                    { 930, false, "La Unión", 90, "100301" },
                    { 929, false, "Tomay Kichwa", 89, "100208" },
                    { 928, false, "San Rafael", 89, "100207" },
                    { 927, false, "San Francisco", 89, "100206" },
                    { 926, false, "Huacar", 89, "100205" },
                    { 925, false, "Conchamarca", 89, "100204" },
                    { 924, false, "Colpas", 89, "100203" },
                    { 923, false, "Cayna", 89, "100202" },
                    { 922, false, "Ambo", 89, "100201" },
                    { 921, false, "San Pablo de Pillao", 88, "100113" },
                    { 920, false, "Yacus", 88, "100112" },
                    { 919, false, "Pillco Marca", 88, "100111" },
                    { 918, false, "Yarumayo", 88, "100110" },
                    { 917, false, "Santa María del Valle", 88, "100109" },
                    { 916, false, "San Pedro de Chaulan", 88, "100108" },
                    { 915, false, "San Francisco de Cayran", 88, "100107" },
                    { 914, false, "Quisqui (Kichki)", 88, "100106" },
                    { 913, false, "Margos", 88, "100105" },
                    { 912, false, "Churubamba", 88, "100104" },
                    { 911, false, "Chinchao", 88, "100103" },
                    { 909, false, "Huanuco", 88, "100101" },
                    { 880, false, "Quito-Arma", 86, "090609" },
                    { 879, false, "Querco", 86, "090608" },
                    { 878, false, "Pilpichaca", 86, "090607" },
                    { 847, false, "Secclla", 83, "090312" },
                    { 846, false, "Santo Tomas de Pata", 83, "090311" },
                    { 845, false, "San Antonio de Antaparco", 83, "090310" },
                    { 844, false, "Julcamarca", 83, "090309" },
                    { 843, false, "Huayllay Grande", 83, "090308" },
                    { 842, false, "Huanca-Huanca", 83, "090307" },
                    { 841, false, "Congalla", 83, "090306" },
                    { 840, false, "Chincho", 83, "090305" },
                    { 839, false, "Ccochaccasa", 83, "090304" },
                    { 838, false, "Callanmarca", 83, "090303" },
                    { 837, false, "Anchonga", 83, "090302" },
                    { 836, false, "Lircay", 83, "090301" },
                    { 835, false, "Rosario", 82, "090208" },
                    { 834, false, "Pomacocha", 82, "090207" },
                    { 833, false, "Paucara", 82, "090206" },
                    { 832, false, "Marcas", 82, "090205" },
                    { 831, false, "Caja", 82, "090204" },
                    { 830, false, "Anta", 82, "090203" },
                    { 829, false, "Andabamba", 82, "090202" },
                    { 828, false, "Acobamba", 82, "090201" },
                    { 827, false, "Huando", 81, "090119" },
                    { 826, false, "Ascensión", 81, "090118" },
                    { 825, false, "Yauli", 81, "090117" },
                    { 824, false, "Vilca", 81, "090116" },
                    { 823, false, "Pilchaca", 81, "090115" },
                    { 848, false, "Castrovirreyna", 84, "090401" },
                    { 849, false, "Arma", 84, "090402" },
                    { 850, false, "Aurahua", 84, "090403" },
                    { 851, false, "Capillas", 84, "090404" },
                    { 877, false, "Ocoyo", 86, "090606" },
                    { 876, false, "Laramarca", 86, "090605" },
                    { 875, false, "Huayacundo Arma", 86, "090604" },
                    { 874, false, "Córdova", 86, "090603" },
                    { 873, false, "Ayavi", 86, "090602" },
                    { 872, false, "Huaytara", 86, "090601" },
                    { 871, false, "Cosme", 85, "090511" },
                    { 870, false, "Pachamarca", 85, "090510" },
                    { 869, false, "San Pedro de Coris", 85, "090509" },
                    { 868, false, "San Miguel de Mayocc", 85, "090508" },
                    { 867, false, "Paucarbamba", 85, "090507" },
                    { 866, false, "Locroja", 85, "090506" },
                    { 821, false, "Nuevo Occoro", 81, "090113" },
                    { 865, false, "La Merced", 85, "090505" },
                    { 863, false, "Chinchihuasi", 85, "090503" },
                    { 862, false, "Anco", 85, "090502" },
                    { 861, false, "Churcampa", 85, "090501" },
                    { 860, false, "Ticrapo", 84, "090413" },
                    { 859, false, "Tantara", 84, "090412" },
                    { 858, false, "Santa Ana", 84, "090411" },
                    { 857, false, "San Juan", 84, "090410" },
                    { 856, false, "Mollepampa", 84, "090409" },
                    { 855, false, "Huamatambo", 84, "090408" },
                    { 854, false, "Huachos", 84, "090407" },
                    { 853, false, "Cocas", 84, "090406" },
                    { 852, false, "Chupamarca", 84, "090405" },
                    { 864, false, "El Carmen", 85, "090504" },
                    { 704, false, "Wanchaq", 68, "080108" },
                    { 703, false, "Saylla", 68, "080107" },
                    { 702, false, "Santiago", 68, "080106" },
                    { 554, false, "Vilcanchos", 52, "051012" },
                    { 553, false, "Sarhua", 52, "051011" },
                    { 552, false, "Hualla", 52, "051010" },
                    { 551, false, "Huancaraylla", 52, "051009" },
                    { 550, false, "Huamanquiquia", 52, "051008" },
                    { 549, false, "Colca", 52, "051007" },
                    { 548, false, "Cayara", 52, "051006" },
                    { 547, false, "Canaria", 52, "051005" },
                    { 546, false, "Asquipata", 52, "051004" },
                    { 545, false, "Apongo", 52, "051003" },
                    { 544, false, "Alcamenca", 52, "051002" },
                    { 543, false, "Huancapi", 52, "051001" },
                    { 542, false, "Soras", 51, "050911" },
                    { 541, false, "Santiago de Paucaray", 51, "050910" },
                    { 540, false, "San Salvador de Quije", 51, "050909" },
                    { 539, false, "San Pedro de Larcay", 51, "050908" },
                    { 538, false, "Paico", 51, "050907" },
                    { 537, false, "Morcolla", 51, "050906" },
                    { 536, false, "Huacaña", 51, "050905" },
                    { 535, false, "Chilcayoc", 51, "050904" },
                    { 534, false, "Chalcos", 51, "050903" },
                    { 533, false, "Belén", 51, "050902" },
                    { 532, false, "Querobamba", 51, "050901" },
                    { 531, false, "Sara Sara", 50, "050810" },
                    { 530, false, "San José de Ushua", 50, "050809" },
                    { 555, false, "Vilcas Huaman", 53, "051101" },
                    { 529, false, "San Javier de Alpabamba", 50, "050808" },
                    { 556, false, "Accomarca", 53, "051102" },
                    { 558, false, "Concepción", 53, "051104" },
                    { 583, false, "Jorge Chávez", 56, "060305" },
                    { 582, false, "Huasmin", 56, "060304" },
                    { 581, false, "Cortegana", 56, "060303" },
                    { 580, false, "Chumuch", 56, "060302" },
                    { 579, false, "Celendín", 56, "060301" },
                    { 578, false, "Sitacocha", 55, "060204" }
                });

            migrationBuilder.InsertData(
                schema: "common",
                table: "Distrito",
                columns: new[] { "DistritoId", "Eliminado", "Nombre", "ProvinciaId", "Ubigeo" },
                values: new object[,]
                {
                    { 577, false, "Condebamba", 55, "060203" },
                    { 576, false, "Cachachi", 55, "060202" },
                    { 575, false, "Cajabamba", 55, "060201" },
                    { 574, false, "San Juan", 54, "060112" },
                    { 573, false, "Namora", 54, "060111" },
                    { 572, false, "Matara", 54, "060110" },
                    { 571, false, "Magdalena", 54, "060109" },
                    { 570, false, "Los Baños del Inca", 54, "060108" },
                    { 569, false, "Llacanora", 54, "060107" },
                    { 568, false, "Jesús", 54, "060106" },
                    { 567, false, "Encañada", 54, "060105" },
                    { 566, false, "Cospan", 54, "060104" },
                    { 565, false, "Chetilla", 54, "060103" },
                    { 564, false, "Asunción", 54, "060102" },
                    { 563, false, "Cajamarca", 54, "060101" },
                    { 562, false, "Vischongo", 53, "051108" },
                    { 561, false, "Saurama", 53, "051107" },
                    { 560, false, "Independencia", 53, "051106" },
                    { 559, false, "Huambalpa", 53, "051105" },
                    { 557, false, "Carhuanca", 53, "051103" },
                    { 528, false, "Pararca", 50, "050807" },
                    { 527, false, "Oyolo", 50, "050806" },
                    { 526, false, "Marcabamba", 50, "050805" },
                    { 495, false, "Cabana", 48, "050603" },
                    { 494, false, "Aucara", 48, "050602" },
                    { 493, false, "Puquio", 48, "050601" },
                    { 492, false, "Oronccoy", 47, "050511" },
                    { 491, false, "Anchihuay", 47, "050510" },
                    { 490, false, "Samugari", 47, "050509" },
                    { 489, false, "Tambo", 47, "050508" },
                    { 488, false, "Santa Rosa", 47, "050507" },
                    { 487, false, "Luis Carranza", 47, "050506" },
                    { 486, false, "Chungui", 47, "050505" },
                    { 485, false, "Chilcas", 47, "050504" },
                    { 484, false, "Ayna", 47, "050503" },
                    { 483, false, "Anco", 47, "050502" },
                    { 482, false, "San Miguel", 47, "050501" },
                    { 481, false, "Chaca", 46, "050412" },
                    { 480, false, "Pucacolpa", 46, "050411" },
                    { 479, false, "Uchuraccay", 46, "050410" },
                    { 478, false, "Canayre", 46, "050409" },
                    { 477, false, "Llochegua", 46, "050408" },
                    { 476, false, "Sivia", 46, "050407" },
                    { 475, false, "Santillana", 46, "050406" },
                    { 474, false, "Luricocha", 46, "050405" },
                    { 473, false, "Iguain", 46, "050404" },
                    { 472, false, "Huamanguilla", 46, "050403" },
                    { 471, false, "Ayahuanco", 46, "050402" },
                    { 496, false, "Carmen Salcedo", 48, "050604" },
                    { 497, false, "Chaviña", 48, "050605" },
                    { 498, false, "Chipao", 48, "050606" },
                    { 499, false, "Huac-Huas", 48, "050607" },
                    { 525, false, "Lampa", 50, "050804" },
                    { 524, false, "Corculla", 50, "050803" },
                    { 523, false, "Colta", 50, "050802" },
                    { 522, false, "Pausa", 50, "050801" },
                    { 521, false, "Upahuacho", 49, "050708" },
                    { 520, false, "San Francisco de Ravacayco", 49, "050707" },
                    { 519, false, "Puyusca", 49, "050706" },
                    { 518, false, "Pullo", 49, "050705" },
                    { 517, false, "Pacapausa", 49, "050704" },
                    { 516, false, "Coronel Castañeda", 49, "050703" },
                    { 515, false, "Chumpi", 49, "050702" },
                    { 514, false, "Coracora", 49, "050701" },
                    { 584, false, "José Gálvez", 56, "060306" },
                    { 513, false, "Santa Lucia", 48, "050621" },
                    { 511, false, "Sancos", 48, "050619" },
                    { 510, false, "San Pedro de Palco", 48, "050618" },
                    { 509, false, "San Pedro", 48, "050617" },
                    { 508, false, "San Juan", 48, "050616" },
                    { 507, false, "San Cristóbal", 48, "050615" },
                    { 506, false, "Saisa", 48, "050614" },
                    { 505, false, "Otoca", 48, "050613" },
                    { 504, false, "Ocaña", 48, "050612" },
                    { 503, false, "Lucanas", 48, "050611" },
                    { 502, false, "Llauta", 48, "050610" },
                    { 501, false, "Leoncio Prado", 48, "050609" },
                    { 500, false, "Laramate", 48, "050608" },
                    { 512, false, "Santa Ana de Huaycahuacho", 48, "050620" },
                    { 585, false, "Miguel Iglesias", 56, "060307" },
                    { 586, false, "Oxamarca", 56, "060308" },
                    { 587, false, "Sorochuco", 56, "060309" },
                    { 671, false, "San Gregorio", 64, "061110" },
                    { 670, false, "Niepos", 64, "061109" },
                    { 669, false, "Nanchoc", 64, "061108" },
                    { 668, false, "Llapa", 64, "061107" },
                    { 667, false, "La Florida", 64, "061106" },
                    { 666, false, "El Prado", 64, "061105" },
                    { 665, false, "Catilluc", 64, "061104" },
                    { 664, false, "Calquis", 64, "061103" },
                    { 663, false, "Bolívar", 64, "061102" },
                    { 662, false, "San Miguel", 64, "061101" },
                    { 661, false, "José Sabogal", 63, "061007" },
                    { 660, false, "José Manuel Quiroz", 63, "061006" },
                    { 659, false, "Ichocan", 63, "061005" },
                    { 658, false, "Gregorio Pita", 63, "061004" },
                    { 657, false, "Eduardo Villanueva", 63, "061003" },
                    { 656, false, "Chancay", 63, "061002" },
                    { 655, false, "Pedro Gálvez", 63, "061001" },
                    { 654, false, "Tabaconas", 62, "060907" },
                    { 653, false, "San José de Lourdes", 62, "060906" },
                    { 652, false, "Namballe", 62, "060905" },
                    { 651, false, "La Coipa", 62, "060904" },
                    { 650, false, "Huarango", 62, "060903" },
                    { 649, false, "Chirinos", 62, "060902" },
                    { 648, false, "San Ignacio", 62, "060901" },
                    { 647, false, "Santa Rosa", 61, "060812" },
                    { 672, false, "San Silvestre de Cochan", 64, "061111" },
                    { 673, false, "Tongod", 64, "061112" },
                    { 674, false, "Unión Agua Blanca", 64, "061113" },
                    { 675, false, "San Pablo", 65, "061201" },
                    { 701, false, "San Sebastian", 68, "080105" },
                    { 700, false, "San Jerónimo", 68, "080104" },
                    { 699, false, "Poroy", 68, "080103" },
                    { 698, false, "Ccorca", 68, "080102" },
                    { 697, false, "Cusco", 68, "080101" },
                    { 696, false, "Mi Perú", 67, "070107" },
                    { 695, false, "Ventanilla", 67, "070106" },
                    { 694, false, "La Punta", 67, "070105" },
                    { 693, false, "La Perla", 67, "070104" },
                    { 692, false, "Carmen de la Legua Reynoso", 67, "070103" },
                    { 691, false, "Bellavista", 67, "070102" },
                    { 690, false, "Callao", 67, "070101" },
                    { 646, false, "San José del Alto", 61, "060811" },
                    { 689, false, "Yauyucan", 66, "061311" },
                    { 687, false, "Sexi", 66, "061309" },
                    { 686, false, "Saucepampa", 66, "061308" },
                    { 685, false, "Pulan", 66, "061307" },
                    { 684, false, "Ninabamba", 66, "061306" },
                    { 683, false, "La Esperanza", 66, "061305" },
                    { 682, false, "Chancaybaños", 66, "061304" },
                    { 681, false, "Catache", 66, "061303" },
                    { 680, false, "Andabamba", 66, "061302" },
                    { 679, false, "Santa Cruz", 66, "061301" },
                    { 678, false, "Tumbaden", 65, "061204" },
                    { 677, false, "San Luis", 65, "061203" },
                    { 676, false, "San Bernardino", 65, "061202" },
                    { 688, false, "Uticyacu", 66, "061310" },
                    { 469, false, "Santiago de Lucanamarca", 45, "050304" },
                    { 645, false, "San Felipe", 61, "060810" },
                    { 643, false, "Pucara", 61, "060808" },
                    { 612, false, "Cupisnique", 58, "060503" },
                    { 611, false, "Chilete", 58, "060502" },
                    { 610, false, "Contumaza", 58, "060501" },
                    { 609, false, "Chalamarca", 57, "060419" },
                    { 608, false, "Tocmoche", 57, "060418" },
                    { 607, false, "Tacabamba", 57, "060417" },
                    { 606, false, "San Juan de Licupis", 57, "060416" },
                    { 605, false, "Querocoto", 57, "060415" },
                    { 604, false, "Pion", 57, "060414" },
                    { 603, false, "Paccha", 57, "060413" },
                    { 602, false, "Miracosta", 57, "060412" },
                    { 601, false, "Llama", 57, "060411" },
                    { 600, false, "Lajas", 57, "060410" },
                    { 599, false, "Huambos", 57, "060409" },
                    { 598, false, "Conchan", 57, "060408" },
                    { 597, false, "Cochabamba", 57, "060407" },
                    { 596, false, "Choropampa", 57, "060406" },
                    { 595, false, "Chimban", 57, "060405" },
                    { 594, false, "Chiguirip", 57, "060404" },
                    { 593, false, "Chadin", 57, "060403" },
                    { 592, false, "Anguia", 57, "060402" },
                    { 591, false, "Chota", 57, "060401" },
                    { 590, false, "La Libertad de Pallan", 56, "060312" },
                    { 589, false, "Utco", 56, "060311" },
                    { 588, false, "Sucre", 56, "060310" },
                    { 613, false, "Guzmango", 58, "060504" },
                    { 614, false, "San Benito", 58, "060505" },
                    { 615, false, "Santa Cruz de Toledo", 58, "060506" },
                    { 616, false, "Tantarica", 58, "060507" },
                    { 642, false, "Pomahuaca", 61, "060807" },
                    { 641, false, "Las Pirias", 61, "060806" },
                    { 640, false, "Huabal", 61, "060805" },
                    { 639, false, "Colasay", 61, "060804" },
                    { 638, false, "Chontali", 61, "060803" },
                    { 637, false, "Bellavista", 61, "060802" },
                    { 636, false, "Jaén", 61, "060801" },
                    { 635, false, "Hualgayoc", 60, "060703" },
                    { 634, false, "Chugur", 60, "060702" },
                    { 633, false, "Bambamarca", 60, "060701" },
                    { 632, false, "Toribio Casanova", 59, "060615" },
                    { 631, false, "Socota", 59, "060614" },
                    { 644, false, "Sallique", 61, "060809" },
                    { 630, false, "Santo Tomas", 59, "060613" },
                    { 628, false, "Santa Cruz", 59, "060611" },
                    { 627, false, "San Luis de Lucma", 59, "060610" },
                    { 626, false, "San Juan de Cutervo", 59, "060609" },
                    { 625, false, "San Andrés de Cutervo", 59, "060608" },
                    { 624, false, "Querocotillo", 59, "060607" },
                    { 623, false, "Pimpingos", 59, "060606" },
                    { 622, false, "La Ramada", 59, "060605" },
                    { 621, false, "Cujillo", 59, "060604" },
                    { 620, false, "Choros", 59, "060603" },
                    { 619, false, "Callayuc", 59, "060602" },
                    { 618, false, "Cutervo", 59, "060601" },
                    { 617, false, "Yonan", 58, "060508" },
                    { 629, false, "Santo Domingo de la Capilla", 59, "060612" },
                    { 1874, false, "Purus", 196, "250401" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 2);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 3);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 4);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 5);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 6);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 7);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 8);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 9);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 10);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 11);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 12);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 13);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 14);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 15);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 16);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 17);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 18);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 19);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 20);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 21);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 22);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 23);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 24);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 25);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 26);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 27);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 28);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 29);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 30);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 31);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 32);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 33);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 34);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 35);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 36);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 37);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 38);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 39);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 40);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 41);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 42);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 43);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 44);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 45);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 46);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 47);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 48);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 49);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 50);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 51);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 52);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 53);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 54);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 55);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 56);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 57);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 58);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 59);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 60);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 61);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 62);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 63);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 64);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 65);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 66);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 67);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 68);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 69);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 70);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 71);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 72);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 73);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 74);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 75);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 76);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 77);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 78);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 79);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 80);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 81);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 82);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 83);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 84);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 85);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 86);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 87);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 88);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 89);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 90);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 91);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 92);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 93);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 94);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 95);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 96);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 97);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 98);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 99);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 100);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 101);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 102);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 103);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 104);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 105);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 106);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 107);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 108);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 109);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 110);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 111);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 112);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 113);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 114);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 115);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 116);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 117);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 118);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 119);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 120);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 121);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 122);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 123);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 124);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 125);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 126);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 127);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 128);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 129);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 130);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 131);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 132);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 133);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 134);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 135);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 136);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 137);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 138);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 139);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 140);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 141);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 142);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 143);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 144);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 145);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 146);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 147);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 148);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 149);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 150);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 151);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 152);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 153);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 154);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 155);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 156);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 157);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 158);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 159);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 160);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 161);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 162);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 163);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 164);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 165);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 166);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 167);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 168);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 169);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 170);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 171);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 172);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 173);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 174);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 175);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 176);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 177);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 178);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 179);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 180);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 181);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 182);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 183);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 184);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 185);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 186);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 187);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 188);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 189);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 190);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 191);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 192);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 193);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 194);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 195);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 196);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 197);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 198);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 199);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 200);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 201);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 202);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 203);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 204);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 205);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 206);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 207);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 208);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 209);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 210);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 211);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 212);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 213);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 214);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 215);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 216);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 217);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 218);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 219);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 220);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 221);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 222);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 223);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 224);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 225);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 226);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 227);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 228);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 229);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 230);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 231);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 232);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 233);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 234);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 235);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 236);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 237);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 238);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 239);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 240);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 241);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 242);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 243);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 244);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 245);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 246);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 247);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 248);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 249);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 250);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 251);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 252);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 253);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 254);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 255);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 256);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 257);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 258);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 259);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 260);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 261);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 262);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 263);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 264);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 265);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 266);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 267);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 268);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 269);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 270);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 271);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 272);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 273);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 274);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 275);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 276);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 277);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 278);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 279);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 280);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 281);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 282);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 283);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 284);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 285);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 286);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 287);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 288);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 289);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 290);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 291);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 292);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 293);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 294);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 295);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 296);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 297);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 298);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 299);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 300);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 301);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 302);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 303);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 304);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 305);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 306);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 307);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 308);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 309);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 310);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 311);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 312);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 313);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 314);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 315);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 316);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 317);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 318);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 319);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 320);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 321);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 322);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 323);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 324);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 325);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 326);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 327);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 328);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 329);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 330);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 331);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 332);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 333);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 334);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 335);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 336);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 337);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 338);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 339);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 340);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 341);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 342);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 343);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 344);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 345);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 346);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 347);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 348);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 349);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 350);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 351);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 352);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 353);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 354);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 355);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 356);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 357);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 358);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 359);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 360);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 361);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 362);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 363);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 364);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 365);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 366);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 367);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 368);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 369);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 370);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 371);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 372);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 373);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 374);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 375);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 376);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 377);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 378);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 379);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 380);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 381);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 382);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 383);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 384);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 385);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 386);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 387);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 388);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 389);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 390);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 391);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 392);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 393);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 394);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 395);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 396);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 397);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 398);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 399);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 400);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 401);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 402);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 403);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 404);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 405);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 406);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 407);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 408);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 409);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 410);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 411);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 412);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 413);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 414);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 415);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 416);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 417);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 418);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 419);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 420);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 421);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 422);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 423);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 424);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 425);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 426);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 427);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 428);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 429);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 430);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 431);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 432);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 433);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 434);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 435);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 436);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 437);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 438);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 439);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 440);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 441);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 442);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 443);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 444);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 445);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 446);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 447);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 448);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 449);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 450);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 451);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 452);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 453);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 454);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 455);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 456);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 457);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 458);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 459);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 460);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 461);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 462);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 463);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 464);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 465);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 466);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 467);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 468);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 469);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 470);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 471);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 472);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 473);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 474);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 475);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 476);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 477);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 478);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 479);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 480);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 481);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 482);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 483);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 484);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 485);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 486);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 487);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 488);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 489);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 490);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 491);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 492);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 493);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 494);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 495);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 496);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 497);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 498);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 499);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 500);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 501);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 502);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 503);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 504);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 505);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 506);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 507);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 508);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 509);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 510);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 511);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 512);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 513);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 514);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 515);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 516);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 517);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 518);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 519);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 520);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 521);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 522);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 523);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 524);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 525);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 526);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 527);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 528);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 529);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 530);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 531);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 532);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 533);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 534);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 535);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 536);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 537);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 538);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 539);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 540);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 541);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 542);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 543);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 544);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 545);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 546);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 547);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 548);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 549);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 550);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 551);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 552);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 553);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 554);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 555);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 556);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 557);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 558);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 559);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 560);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 561);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 562);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 563);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 564);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 565);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 566);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 567);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 568);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 569);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 570);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 571);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 572);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 573);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 574);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 575);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 576);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 577);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 578);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 579);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 580);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 581);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 582);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 583);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 584);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 585);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 586);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 587);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 588);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 589);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 590);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 591);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 592);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 593);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 594);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 595);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 596);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 597);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 598);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 599);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 600);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 601);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 602);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 603);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 604);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 605);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 606);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 607);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 608);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 609);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 610);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 611);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 612);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 613);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 614);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 615);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 616);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 617);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 618);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 619);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 620);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 621);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 622);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 623);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 624);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 625);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 626);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 627);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 628);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 629);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 630);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 631);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 632);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 633);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 634);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 635);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 636);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 637);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 638);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 639);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 640);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 641);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 642);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 643);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 644);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 645);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 646);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 647);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 648);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 649);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 650);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 651);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 652);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 653);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 654);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 655);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 656);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 657);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 658);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 659);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 660);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 661);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 662);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 663);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 664);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 665);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 666);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 667);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 668);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 669);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 670);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 671);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 672);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 673);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 674);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 675);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 676);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 677);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 678);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 679);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 680);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 681);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 682);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 683);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 684);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 685);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 686);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 687);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 688);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 689);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 690);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 691);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 692);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 693);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 694);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 695);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 696);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 697);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 698);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 699);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 700);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 701);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 702);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 703);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 704);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 705);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 706);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 707);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 708);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 709);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 710);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 711);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 712);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 713);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 714);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 715);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 716);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 717);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 718);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 719);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 720);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 721);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 722);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 723);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 724);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 725);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 726);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 727);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 728);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 729);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 730);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 731);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 732);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 733);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 734);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 735);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 736);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 737);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 738);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 739);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 740);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 741);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 742);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 743);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 744);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 745);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 746);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 747);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 748);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 749);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 750);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 751);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 752);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 753);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 754);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 755);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 756);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 757);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 758);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 759);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 760);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 761);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 762);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 763);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 764);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 765);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 766);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 767);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 768);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 769);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 770);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 771);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 772);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 773);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 774);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 775);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 776);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 777);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 778);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 779);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 780);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 781);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 782);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 783);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 784);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 785);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 786);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 787);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 788);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 789);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 790);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 791);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 792);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 793);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 794);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 795);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 796);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 797);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 798);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 799);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 800);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 801);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 802);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 803);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 804);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 805);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 806);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 807);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 808);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 809);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 810);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 811);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 812);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 813);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 814);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 815);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 816);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 817);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 818);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 819);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 820);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 821);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 822);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 823);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 824);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 825);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 826);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 827);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 828);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 829);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 830);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 831);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 832);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 833);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 834);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 835);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 836);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 837);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 838);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 839);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 840);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 841);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 842);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 843);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 844);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 845);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 846);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 847);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 848);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 849);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 850);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 851);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 852);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 853);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 854);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 855);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 856);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 857);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 858);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 859);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 860);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 861);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 862);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 863);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 864);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 865);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 866);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 867);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 868);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 869);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 870);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 871);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 872);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 873);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 874);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 875);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 876);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 877);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 878);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 879);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 880);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 881);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 882);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 883);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 884);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 885);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 886);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 887);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 888);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 889);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 890);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 891);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 892);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 893);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 894);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 895);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 896);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 897);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 898);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 899);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 900);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 901);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 902);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 903);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 904);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 905);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 906);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 907);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 908);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 909);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 910);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 911);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 912);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 913);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 914);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 915);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 916);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 917);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 918);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 919);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 920);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 921);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 922);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 923);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 924);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 925);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 926);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 927);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 928);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 929);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 930);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 931);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 932);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 933);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 934);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 935);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 936);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 937);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 938);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 939);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 940);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 941);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 942);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 943);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 944);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 945);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 946);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 947);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 948);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 949);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 950);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 951);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 952);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 953);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 954);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 955);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 956);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 957);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 958);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 959);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 960);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 961);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 962);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 963);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 964);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 965);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 966);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 967);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 968);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 969);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 970);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 971);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 972);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 973);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 974);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 975);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 976);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 977);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 978);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 979);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 980);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 981);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 982);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 983);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 984);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 985);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 986);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 987);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 988);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 989);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 990);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 991);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 992);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 993);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 994);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 995);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 996);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 997);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 998);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 999);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1000);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1001);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1002);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1003);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1004);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1005);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1006);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1007);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1008);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1009);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1010);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1011);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1012);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1013);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1014);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1015);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1016);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1017);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1018);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1019);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1020);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1021);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1022);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1023);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1024);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1025);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1026);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1027);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1028);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1029);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1030);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1031);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1032);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1033);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1034);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1035);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1036);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1037);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1038);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1039);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1040);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1041);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1042);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1043);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1044);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1045);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1046);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1047);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1048);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1049);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1050);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1051);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1052);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1053);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1054);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1055);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1056);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1057);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1058);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1059);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1060);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1061);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1062);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1063);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1064);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1065);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1066);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1067);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1068);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1069);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1070);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1071);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1072);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1073);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1074);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1075);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1076);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1077);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1078);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1079);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1080);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1081);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1082);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1083);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1084);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1085);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1086);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1087);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1088);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1089);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1090);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1091);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1092);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1093);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1094);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1095);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1096);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1097);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1098);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1099);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1100);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1101);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1102);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1103);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1104);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1105);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1106);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1107);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1108);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1109);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1110);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1111);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1112);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1113);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1114);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1115);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1116);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1117);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1118);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1119);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1120);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1121);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1122);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1123);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1124);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1125);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1126);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1127);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1128);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1129);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1130);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1131);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1132);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1133);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1134);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1135);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1136);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1137);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1138);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1139);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1140);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1141);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1142);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1143);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1144);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1145);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1146);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1147);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1148);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1149);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1150);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1151);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1152);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1153);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1154);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1155);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1156);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1157);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1158);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1159);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1160);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1161);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1162);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1163);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1164);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1165);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1166);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1167);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1168);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1169);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1170);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1171);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1172);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1173);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1174);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1175);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1176);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1177);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1178);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1179);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1180);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1181);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1182);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1183);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1184);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1185);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1186);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1187);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1188);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1189);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1190);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1191);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1192);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1193);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1194);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1195);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1196);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1197);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1198);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1199);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1200);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1201);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1202);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1203);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1204);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1205);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1206);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1207);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1208);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1209);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1210);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1211);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1212);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1213);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1214);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1215);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1216);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1217);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1218);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1219);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1220);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1221);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1222);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1223);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1224);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1225);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1226);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1227);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1228);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1229);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1230);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1231);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1232);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1233);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1234);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1235);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1236);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1237);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1238);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1239);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1240);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1241);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1242);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1243);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1244);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1245);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1246);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1247);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1248);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1249);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1250);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1251);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1252);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1253);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1254);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1255);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1256);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1257);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1258);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1259);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1260);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1261);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1262);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1263);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1264);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1265);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1266);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1267);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1268);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1269);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1270);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1271);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1272);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1273);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1274);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1275);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1276);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1277);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1278);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1279);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1280);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1281);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1282);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1283);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1284);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1285);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1286);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1287);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1288);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1289);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1290);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1291);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1292);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1293);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1294);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1295);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1296);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1297);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1298);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1299);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1300);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1301);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1302);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1303);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1304);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1305);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1306);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1307);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1308);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1309);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1310);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1311);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1312);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1313);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1314);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1315);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1316);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1317);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1318);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1319);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1320);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1321);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1322);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1323);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1324);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1325);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1326);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1327);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1328);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1329);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1330);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1331);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1332);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1333);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1334);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1335);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1336);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1337);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1338);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1339);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1340);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1341);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1342);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1343);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1344);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1345);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1346);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1347);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1348);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1349);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1350);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1351);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1352);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1353);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1354);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1355);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1356);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1357);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1358);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1359);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1360);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1361);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1362);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1363);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1364);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1365);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1366);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1367);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1368);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1369);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1370);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1371);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1372);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1373);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1374);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1375);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1376);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1377);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1378);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1379);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1380);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1381);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1382);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1383);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1384);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1385);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1386);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1387);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1388);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1389);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1390);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1391);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1392);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1393);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1394);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1395);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1396);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1397);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1398);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1399);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1400);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1401);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1402);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1403);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1404);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1405);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1406);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1407);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1408);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1409);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1410);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1411);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1412);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1413);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1414);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1415);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1416);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1417);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1418);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1419);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1420);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1421);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1422);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1423);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1424);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1425);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1426);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1427);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1428);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1429);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1430);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1431);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1432);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1433);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1434);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1435);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1436);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1437);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1438);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1439);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1440);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1441);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1442);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1443);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1444);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1445);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1446);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1447);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1448);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1449);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1450);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1451);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1452);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1453);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1454);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1455);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1456);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1457);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1458);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1459);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1460);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1461);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1462);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1463);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1464);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1465);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1466);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1467);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1468);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1469);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1470);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1471);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1472);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1473);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1474);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1475);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1476);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1477);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1478);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1479);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1480);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1481);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1482);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1483);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1484);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1485);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1486);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1487);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1488);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1489);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1490);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1491);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1492);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1493);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1494);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1495);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1496);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1497);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1498);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1499);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1500);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1501);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1502);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1503);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1504);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1505);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1506);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1507);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1508);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1509);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1510);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1511);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1512);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1513);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1514);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1515);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1516);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1517);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1518);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1519);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1520);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1521);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1522);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1523);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1524);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1525);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1526);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1527);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1528);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1529);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1530);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1531);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1532);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1533);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1534);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1535);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1536);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1537);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1538);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1539);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1540);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1541);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1542);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1543);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1544);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1545);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1546);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1547);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1548);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1549);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1550);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1551);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1552);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1553);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1554);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1555);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1556);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1557);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1558);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1559);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1560);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1561);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1562);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1563);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1564);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1565);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1566);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1567);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1568);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1569);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1570);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1571);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1572);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1573);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1574);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1575);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1576);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1577);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1578);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1579);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1580);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1581);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1582);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1583);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1584);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1585);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1586);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1587);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1588);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1589);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1590);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1591);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1592);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1593);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1594);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1595);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1596);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1597);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1598);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1599);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1600);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1601);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1602);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1603);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1604);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1605);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1606);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1607);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1608);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1609);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1610);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1611);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1612);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1613);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1614);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1615);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1616);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1617);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1618);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1619);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1620);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1621);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1622);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1623);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1624);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1625);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1626);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1627);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1628);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1629);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1630);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1631);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1632);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1633);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1634);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1635);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1636);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1637);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1638);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1639);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1640);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1641);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1642);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1643);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1644);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1645);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1646);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1647);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1648);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1649);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1650);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1651);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1652);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1653);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1654);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1655);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1656);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1657);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1658);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1659);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1660);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1661);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1662);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1663);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1664);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1665);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1666);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1667);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1668);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1669);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1670);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1671);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1672);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1673);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1674);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1675);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1676);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1677);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1678);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1679);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1680);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1681);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1682);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1683);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1684);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1685);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1686);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1687);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1688);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1689);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1690);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1691);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1692);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1693);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1694);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1695);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1696);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1697);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1698);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1699);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1700);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1701);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1702);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1703);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1704);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1705);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1706);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1707);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1708);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1709);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1710);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1711);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1712);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1713);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1714);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1715);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1716);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1717);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1718);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1719);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1720);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1721);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1722);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1723);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1724);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1725);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1726);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1727);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1728);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1729);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1730);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1731);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1732);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1733);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1734);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1735);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1736);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1737);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1738);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1739);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1740);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1741);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1742);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1743);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1744);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1745);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1746);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1747);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1748);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1749);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1750);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1751);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1752);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1753);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1754);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1755);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1756);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1757);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1758);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1759);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1760);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1761);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1762);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1763);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1764);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1765);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1766);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1767);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1768);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1769);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1770);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1771);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1772);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1773);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1774);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1775);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1776);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1777);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1778);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1779);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1780);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1781);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1782);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1783);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1784);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1785);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1786);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1787);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1788);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1789);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1790);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1791);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1792);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1793);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1794);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1795);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1796);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1797);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1798);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1799);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1800);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1801);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1802);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1803);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1804);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1805);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1806);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1807);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1808);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1809);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1810);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1811);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1812);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1813);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1814);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1815);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1816);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1817);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1818);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1819);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1820);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1821);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1822);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1823);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1824);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1825);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1826);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1827);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1828);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1829);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1830);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1831);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1832);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1833);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1834);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1835);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1836);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1837);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1838);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1839);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1840);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1841);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1842);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1843);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1844);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1845);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1846);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1847);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1848);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1849);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1850);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1851);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1852);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1853);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1854);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1855);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1856);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1857);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1858);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1859);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1860);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1861);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1862);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1863);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1864);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1865);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1866);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1867);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1868);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1869);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1870);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1871);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1872);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1873);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Distrito",
                keyColumn: "DistritoId",
                keyValue: 1874);
        }
    }
}
