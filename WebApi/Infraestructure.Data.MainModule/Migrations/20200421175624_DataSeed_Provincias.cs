﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infraestructure.Data.MainModule.Migrations
{
    public partial class DataSeed_Provincias : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                schema: "common",
                table: "Provincia",
                columns: new[] { "ProvinciaId", "DepartamentoId", "Eliminado", "Nombre", "Ubigeo" },
                values: new object[,]
                {
                    { 1, 1, false, "CHACHAPOYAS", "0101" },
                    { 126, 14, false, "FERREÑAFE", "1402" },
                    { 127, 14, false, "LAMBAYEQUE", "1403" },
                    { 128, 15, false, "LIMA", "1501" },
                    { 129, 15, false, "BARRANCA", "1502" },
                    { 130, 15, false, "CAJATAMBO", "1503" },
                    { 131, 15, false, "CANTA", "1504" },
                    { 132, 15, false, "CAÑETE", "1505" },
                    { 133, 15, false, "HUARAL", "1506" },
                    { 134, 15, false, "HUAROCHIRÍ", "1507" },
                    { 125, 14, false, "CHICLAYO", "1401" },
                    { 135, 15, false, "HUAURA", "1508" },
                    { 137, 15, false, "YAUYOS", "1510" },
                    { 138, 16, false, "MAYNAS", "1601" },
                    { 139, 16, false, "ALTO AMAZONAS", "1602" },
                    { 140, 16, false, "LORETO", "1603" },
                    { 141, 16, false, "MARISCAL RAMÓN CASTILLA", "1604" },
                    { 142, 16, false, "REQUENA", "1605" },
                    { 143, 16, false, "UCAYALI", "1606" },
                    { 144, 16, false, "DATEM DEL MARAÑÓN", "1607" },
                    { 145, 16, false, "PUTUMAYO", "1608" },
                    { 136, 15, false, "OYÓN", "1509" },
                    { 124, 13, false, "VIRÚ", "1312" },
                    { 123, 13, false, "GRAN CHIMÚ", "1311" },
                    { 122, 13, false, "SANTIAGO DE CHUCO", "1310" },
                    { 101, 11, false, "NAZCA", "1103" },
                    { 102, 11, false, "PALPA", "1104" },
                    { 103, 11, false, "PISCO", "1105" },
                    { 104, 12, false, "HUANCAYO", "1201" },
                    { 105, 12, false, "CONCEPCIÓN", "1202" },
                    { 106, 12, false, "CHANCHAMAYO", "1203" },
                    { 107, 12, false, "JAUJA", "1204" },
                    { 108, 12, false, "JUNÍN", "1205" },
                    { 109, 12, false, "SATIPO", "1206" },
                    { 110, 12, false, "TARMA", "1207" },
                    { 111, 12, false, "YAULI", "1208" },
                    { 112, 12, false, "CHUPACA", "1209" },
                    { 113, 13, false, "TRUJILLO", "1301" },
                    { 114, 13, false, "ASCOPE", "1302" },
                    { 115, 13, false, "BOLÍVAR", "1303" },
                    { 116, 13, false, "CHEPÉN", "1304" },
                    { 117, 13, false, "JULCÁN", "1305" },
                    { 118, 13, false, "OTUZCO", "1306" },
                    { 119, 13, false, "PACASMAYO", "1307" },
                    { 120, 13, false, "PATAZ", "1308" },
                    { 121, 13, false, "SÁNCHEZ CARRIÓN", "1309" },
                    { 146, 17, false, "TAMBOPATA", "1701" },
                    { 100, 11, false, "CHINCHA", "1102" },
                    { 147, 17, false, "MANU", "1702" },
                    { 149, 18, false, "MARISCAL NIETO", "1801" },
                    { 175, 21, false, "YUNGUYO", "2113" },
                    { 176, 22, false, "MOYOBAMBA", "2201" },
                    { 177, 22, false, "BELLAVISTA", "2202" },
                    { 178, 22, false, "EL DORADO", "2203" },
                    { 179, 22, false, "HUALLAGA", "2204" },
                    { 180, 22, false, "LAMAS", "2205" },
                    { 181, 22, false, "MARISCAL CÁCERES", "2206" },
                    { 182, 22, false, "PICOTA", "2207" },
                    { 183, 22, false, "RIOJA", "2208" },
                    { 174, 21, false, "SANDIA", "2112" },
                    { 184, 22, false, "SAN MARTÍN", "2209" },
                    { 186, 23, false, "TACNA", "2301" },
                    { 187, 23, false, "CANDARAVE", "2302" },
                    { 188, 23, false, "JORGE BASADRE", "2303" },
                    { 189, 23, false, "TARATA", "2304" },
                    { 190, 24, false, "TUMBES", "2401" },
                    { 191, 24, false, "CONTRALMIRANTE VILLAR", "2402" },
                    { 192, 24, false, "ZARUMILLA", "2403" },
                    { 193, 25, false, "CORONEL PORTILLO", "2501" },
                    { 194, 25, false, "ATALAYA", "2502" },
                    { 185, 22, false, "TOCACHE", "2210" },
                    { 173, 21, false, "SAN ROMÁN", "2111" },
                    { 172, 21, false, "SAN ANTONIO DE PUTINA", "2110" },
                    { 171, 21, false, "MOHO", "2109" },
                    { 150, 18, false, "GENERAL SÁNCHEZ CERRO", "1802" },
                    { 151, 18, false, "ILO", "1803" },
                    { 152, 19, false, "PASCO", "1901" },
                    { 153, 19, false, "DANIEL ALCIDES CARRIÓN", "1902" },
                    { 154, 19, false, "OXAPAMPA", "1903" },
                    { 155, 20, false, "PIURA", "2001" },
                    { 156, 20, false, "AYABACA", "2002" },
                    { 157, 20, false, "HUANCABAMBA", "2003" },
                    { 158, 20, false, "MORROPÓN", "2004" },
                    { 159, 20, false, "PAITA", "2005" },
                    { 160, 20, false, "SULLANA", "2006" },
                    { 161, 20, false, "TALARA", "2007" },
                    { 162, 20, false, "SECHURA", "2008" },
                    { 163, 21, false, "PUNO", "2101" },
                    { 164, 21, false, "AZÁNGARO", "2102" },
                    { 165, 21, false, "CARABAYA", "2103" },
                    { 166, 21, false, "CHUCUITO", "2104" },
                    { 167, 21, false, "EL COLLAO", "2105" },
                    { 168, 21, false, "HUANCANÉ", "2106" },
                    { 169, 21, false, "LAMPA", "2107" },
                    { 170, 21, false, "MELGAR", "2108" },
                    { 148, 17, false, "TAHUAMANU", "1703" },
                    { 99, 11, false, "ICA", "1101" },
                    { 98, 10, false, "YAROWILCA", "1011" },
                    { 97, 10, false, "LAURICOCHA", "1010" },
                    { 27, 2, false, "YUNGAY", "0220" },
                    { 28, 3, false, "ABANCAY", "0301" },
                    { 29, 3, false, "ANDAHUAYLAS", "0302" },
                    { 30, 3, false, "ANTABAMBA", "0303" },
                    { 31, 3, false, "AYMARAES", "0304" },
                    { 32, 3, false, "COTABAMBAS", "0305" },
                    { 33, 3, false, "CHINCHEROS", "0306" },
                    { 34, 3, false, "GRAU", "0307" },
                    { 35, 4, false, "AREQUIPA", "0401" },
                    { 26, 2, false, "SIHUAS", "0219" },
                    { 36, 4, false, "CAMANÁ", "0402" },
                    { 38, 4, false, "CASTILLA", "0404" },
                    { 39, 4, false, "CAYLLOMA", "0405" },
                    { 40, 4, false, "CONDESUYOS", "0406" },
                    { 41, 4, false, "ISLAY", "0407" },
                    { 42, 4, false, "LA UNIÓN", "0408" },
                    { 43, 5, false, "HUAMANGA", "0501" },
                    { 44, 5, false, "CANGALLO", "0502" },
                    { 45, 5, false, "HUANCA SANCOS", "0503" },
                    { 46, 5, false, "HUANTA", "0504" },
                    { 37, 4, false, "CARAVELÍ", "0403" },
                    { 25, 2, false, "SANTA", "0218" },
                    { 24, 2, false, "RECUAY", "0217" },
                    { 23, 2, false, "POMABAMBA", "0216" },
                    { 2, 1, false, "BAGUA", "0102" },
                    { 3, 1, false, "BONGARA", "0103" },
                    { 4, 1, false, "CONDORCANQUI", "0104" },
                    { 5, 1, false, "LUYA", "0105" },
                    { 6, 1, false, "RODRÍGUEZ DE MENDOZA", "0106" },
                    { 7, 1, false, "UTCUBAMBA", "0107" },
                    { 8, 2, false, "HUARAZ", "0201" },
                    { 9, 2, false, "AIJA", "0202" },
                    { 10, 2, false, "ANTONIO RAYMONDI", "0203" },
                    { 11, 2, false, "ASUNCION", "0204" },
                    { 12, 2, false, "BOLOGNESI", "0205" },
                    { 13, 2, false, "CARHUAZ", "0206" },
                    { 14, 2, false, "CARLOS FERMIN FITZCARRALD", "0207" },
                    { 15, 2, false, "CASMA", "0208" },
                    { 16, 2, false, "CORONGO", "0209" },
                    { 17, 2, false, "HUARI", "0210" },
                    { 18, 2, false, "HUARMEY", "0211" },
                    { 19, 2, false, "HUAYLAS", "0212" },
                    { 20, 2, false, "MARISCAL LUZURIAGA", "0213" },
                    { 21, 2, false, "OCROS", "0214" },
                    { 22, 2, false, "PALLASCA", "0215" },
                    { 47, 5, false, "LA MAR", "0505" },
                    { 48, 5, false, "LUCANAS", "0506" },
                    { 49, 5, false, "PARINACOCHAS", "0507" },
                    { 50, 5, false, "PÁUCAR DEL SARA SARA", "0508" },
                    { 76, 8, false, "LA CONVENCIÓN", "0809" },
                    { 77, 8, false, "PARURO", "0810" },
                    { 78, 8, false, "PAUCARTAMBO", "0811" },
                    { 79, 8, false, "QUISPICANCHI", "0812" },
                    { 80, 8, false, "URUBAMBA", "0813" },
                    { 81, 9, false, "HUANCAVELICA", "0901" },
                    { 82, 9, false, "ACOBAMBA", "0902" },
                    { 83, 9, false, "ANGARAES", "0903" },
                    { 84, 9, false, "CASTROVIRREYNA", "0904" },
                    { 85, 9, false, "CHURCAMPA", "0905" },
                    { 86, 9, false, "HUAYTARÁ", "0906" },
                    { 87, 9, false, "TAYACAJA", "0907" },
                    { 88, 10, false, "HUÁNUCO", "1001" },
                    { 89, 10, false, "AMBO", "1002" },
                    { 90, 10, false, "DOS DE MAYO", "1003" },
                    { 91, 10, false, "HUACAYBAMBA", "1004" },
                    { 92, 10, false, "HUAMALÍES", "1005" },
                    { 93, 10, false, "LEONCIO PRADO", "1006" },
                    { 94, 10, false, "MARAÑÓN", "1007" },
                    { 95, 10, false, "PACHITEA", "1008" },
                    { 96, 10, false, "PUERTO INCA", "1009" },
                    { 75, 8, false, "ESPINAR", "0808" },
                    { 195, 25, false, "PADRE ABAD", "2503" },
                    { 74, 8, false, "CHUMBIVILCAS", "0807" },
                    { 72, 8, false, "CANAS", "0805" },
                    { 51, 5, false, "SUCRE", "0509" },
                    { 52, 5, false, "VÍCTOR FAJARDO", "0510" },
                    { 53, 5, false, "VILCAS HUAMÁN", "0511" },
                    { 54, 6, false, "CAJAMARCA", "0601" },
                    { 55, 6, false, "CAJABAMBA", "0602" },
                    { 56, 6, false, "CELENDÍN", "0603" },
                    { 57, 6, false, "CHOTA", "0604" },
                    { 58, 6, false, "CONTUMAZÁ", "0605" },
                    { 59, 6, false, "CUTERVO", "0606" },
                    { 60, 6, false, "HUALGAYOC", "0607" },
                    { 61, 6, false, "JAÉN", "0608" },
                    { 62, 6, false, "SAN IGNACIO", "0609" },
                    { 63, 6, false, "SAN MARCOS", "0610" },
                    { 64, 6, false, "SAN MIGUEL", "0611" },
                    { 65, 6, false, "SAN PABLO", "0612" },
                    { 66, 6, false, "SANTA CRUZ", "0613" },
                    { 67, 7, false, "CALLAO", "0701" },
                    { 68, 8, false, "CUSCO", "0801" },
                    { 69, 8, false, "ACOMAYO", "0802" },
                    { 70, 8, false, "ANTA", "0803" },
                    { 71, 8, false, "CALCA", "0804" },
                    { 73, 8, false, "CANCHIS", "0806" },
                    { 196, 25, false, "PURÚS", "2504" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 1);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 2);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 3);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 4);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 5);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 6);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 7);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 8);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 9);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 10);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 11);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 12);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 13);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 14);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 15);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 16);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 17);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 18);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 19);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 20);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 21);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 22);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 23);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 24);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 25);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 26);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 27);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 28);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 29);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 30);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 31);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 32);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 33);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 34);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 35);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 36);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 37);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 38);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 39);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 40);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 41);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 42);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 43);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 44);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 45);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 46);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 47);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 48);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 49);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 50);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 51);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 52);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 53);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 54);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 55);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 56);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 57);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 58);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 59);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 60);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 61);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 62);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 63);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 64);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 65);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 66);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 67);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 68);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 69);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 70);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 71);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 72);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 73);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 74);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 75);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 76);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 77);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 78);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 79);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 80);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 81);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 82);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 83);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 84);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 85);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 86);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 87);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 88);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 89);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 90);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 91);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 92);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 93);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 94);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 95);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 96);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 97);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 98);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 99);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 100);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 101);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 102);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 103);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 104);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 105);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 106);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 107);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 108);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 109);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 110);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 111);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 112);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 113);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 114);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 115);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 116);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 117);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 118);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 119);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 120);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 121);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 122);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 123);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 124);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 125);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 126);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 127);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 128);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 129);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 130);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 131);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 132);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 133);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 134);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 135);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 136);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 137);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 138);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 139);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 140);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 141);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 142);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 143);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 144);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 145);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 146);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 147);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 148);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 149);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 150);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 151);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 152);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 153);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 154);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 155);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 156);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 157);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 158);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 159);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 160);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 161);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 162);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 163);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 164);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 165);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 166);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 167);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 168);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 169);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 170);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 171);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 172);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 173);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 174);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 175);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 176);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 177);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 178);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 179);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 180);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 181);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 182);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 183);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 184);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 185);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 186);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 187);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 188);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 189);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 190);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 191);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 192);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 193);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 194);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 195);

            migrationBuilder.DeleteData(
                schema: "common",
                table: "Provincia",
                keyColumn: "ProvinciaId",
                keyValue: 196);
        }
    }
}
