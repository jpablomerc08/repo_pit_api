﻿// <auto-generated />
using System;
using Infraestructure.Data.MainModule.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace Infraestructure.Data.MainModule.Migrations
{
    [DbContext(typeof(DataContext))]
    [Migration("20200421175139_Initial")]
    partial class Initial
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasDefaultSchema("seguridad")
                .HasAnnotation("ProductVersion", "3.1.2")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Domain.Core.Entities.Common.Departamento", b =>
                {
                    b.Property<int>("DepartamentoId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<bool>("Eliminado")
                        .HasColumnType("bit");

                    b.Property<string>("Nombre")
                        .IsRequired()
                        .HasColumnType("varchar(50)")
                        .HasMaxLength(50)
                        .IsUnicode(false);

                    b.Property<string>("Ubigeo")
                        .IsRequired()
                        .HasColumnType("char(2)")
                        .HasMaxLength(2)
                        .IsUnicode(false);

                    b.HasKey("DepartamentoId");

                    b.HasIndex("Ubigeo")
                        .IsUnique()
                        .HasName("IX_Departamento_Ubigeo")
                        .HasAnnotation("SqlServer:Clustered", false);

                    b.ToTable("Departamento","common");
                });

            modelBuilder.Entity("Domain.Core.Entities.Common.Distrito", b =>
                {
                    b.Property<int>("DistritoId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<bool>("Eliminado")
                        .HasColumnType("bit");

                    b.Property<string>("Nombre")
                        .IsRequired()
                        .HasColumnType("varchar(100)")
                        .HasMaxLength(100)
                        .IsUnicode(false);

                    b.Property<int>("ProvinciaId")
                        .HasColumnType("int");

                    b.Property<string>("Ubigeo")
                        .IsRequired()
                        .HasColumnType("char(6)")
                        .HasMaxLength(6)
                        .IsUnicode(false);

                    b.HasKey("DistritoId");

                    b.HasIndex("ProvinciaId");

                    b.HasIndex("Ubigeo")
                        .IsUnique()
                        .HasName("IX_Distrito_Ubigeo")
                        .HasAnnotation("SqlServer:Clustered", false);

                    b.ToTable("Distrito","common");
                });

            modelBuilder.Entity("Domain.Core.Entities.Common.Pais", b =>
                {
                    b.Property<int>("PaisId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("CodigoISO")
                        .IsRequired()
                        .HasColumnType("char(2)")
                        .HasMaxLength(2)
                        .IsUnicode(false);

                    b.Property<bool>("Eliminado")
                        .HasColumnType("bit");

                    b.Property<string>("Nombre")
                        .IsRequired()
                        .HasColumnType("varchar(100)")
                        .HasMaxLength(100)
                        .IsUnicode(false);

                    b.HasKey("PaisId");

                    b.HasIndex("CodigoISO")
                        .IsUnique()
                        .HasName("IX_Pais_CodigoISO")
                        .HasAnnotation("SqlServer:Clustered", false);

                    b.ToTable("Pais","common");
                });

            modelBuilder.Entity("Domain.Core.Entities.Common.Provincia", b =>
                {
                    b.Property<int>("ProvinciaId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("DepartamentoId")
                        .HasColumnType("int");

                    b.Property<bool>("Eliminado")
                        .HasColumnType("bit");

                    b.Property<string>("Nombre")
                        .IsRequired()
                        .HasColumnType("varchar(100)")
                        .HasMaxLength(100)
                        .IsUnicode(false);

                    b.Property<string>("Ubigeo")
                        .IsRequired()
                        .HasColumnType("char(4)")
                        .HasMaxLength(4)
                        .IsUnicode(false);

                    b.HasKey("ProvinciaId");

                    b.HasIndex("DepartamentoId");

                    b.HasIndex("Ubigeo")
                        .IsUnique()
                        .HasName("IX_Provincia_Ubigeo")
                        .HasAnnotation("SqlServer:Clustered", false);

                    b.ToTable("Provincia","common");
                });

            modelBuilder.Entity("Domain.Core.Entities.Common.TipoDocumentoIdentidad", b =>
                {
                    b.Property<int>("TipoDocumentoIdentidadId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("DescripcionCorta")
                        .IsRequired()
                        .HasColumnType("varchar(20)")
                        .HasMaxLength(20)
                        .IsUnicode(false);

                    b.Property<string>("DescripcionLarga")
                        .IsRequired()
                        .HasColumnType("varchar(50)")
                        .HasMaxLength(50)
                        .IsUnicode(false);

                    b.Property<bool>("Eliminado")
                        .HasColumnType("bit");

                    b.HasKey("TipoDocumentoIdentidadId");

                    b.ToTable("TipoDocumento","common");
                });

            modelBuilder.Entity("Domain.Core.Entities.Security.Perfil", b =>
                {
                    b.Property<int>("PerfilId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime?>("Audit_FechaActualizacion")
                        .HasColumnType("datetime2");

                    b.Property<DateTime>("Audit_FechaCreacion")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("datetime2")
                        .HasDefaultValueSql("GETDATE()");

                    b.Property<string>("Audit_UsuarioActualizacion")
                        .HasColumnType("varchar(50)")
                        .HasMaxLength(50)
                        .IsUnicode(false);

                    b.Property<string>("Audit_UsuarioCreacion")
                        .IsRequired()
                        .ValueGeneratedOnAdd()
                        .HasColumnType("varchar(50)")
                        .HasDefaultValueSql("SYSTEM_USER")
                        .HasMaxLength(50)
                        .IsUnicode(false);

                    b.Property<bool>("Eliminado")
                        .HasColumnType("bit");

                    b.Property<string>("Nombre")
                        .IsRequired()
                        .HasColumnType("varchar(50)")
                        .HasMaxLength(50)
                        .IsUnicode(false);

                    b.HasKey("PerfilId");

                    b.ToTable("Perfil","seguridad");
                });

            modelBuilder.Entity("Domain.Core.Entities.Security.Usuario", b =>
                {
                    b.Property<int>("PerfilId")
                        .HasColumnType("int");

                    b.Property<string>("ApellidoMaterno")
                        .HasColumnType("varchar(50)")
                        .HasMaxLength(50)
                        .IsUnicode(false);

                    b.Property<string>("ApellidoPaterno")
                        .IsRequired()
                        .HasColumnType("varchar(50)")
                        .HasMaxLength(50)
                        .IsUnicode(false);

                    b.Property<DateTime?>("Audit_FechaActualizacion")
                        .HasColumnType("datetime2");

                    b.Property<DateTime>("Audit_FechaCreacion")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("datetime2")
                        .HasDefaultValueSql("GETDATE()");

                    b.Property<string>("Audit_UsuarioActualizacion")
                        .HasColumnType("varchar(50)")
                        .HasMaxLength(50)
                        .IsUnicode(false);

                    b.Property<string>("Audit_UsuarioCreacion")
                        .IsRequired()
                        .ValueGeneratedOnAdd()
                        .HasColumnType("varchar(50)")
                        .HasDefaultValueSql("SYSTEM_USER")
                        .HasMaxLength(50)
                        .IsUnicode(false);

                    b.Property<bool>("Eliminado")
                        .HasColumnType("bit");

                    b.Property<string>("Email")
                        .IsRequired()
                        .HasColumnType("varchar(100)")
                        .HasMaxLength(100)
                        .IsUnicode(false);

                    b.Property<int>("Estado")
                        .HasColumnType("int");

                    b.Property<string>("Nombre")
                        .IsRequired()
                        .HasColumnType("varchar(50)")
                        .HasMaxLength(50)
                        .IsUnicode(false);

                    b.Property<string>("Password")
                        .IsRequired()
                        .HasColumnType("varchar(512)")
                        .HasMaxLength(512)
                        .IsUnicode(false);

                    b.Property<string>("Telefono")
                        .IsRequired()
                        .HasColumnType("varchar(30)")
                        .HasMaxLength(30)
                        .IsUnicode(false);

                    b.Property<string>("Username")
                        .IsRequired()
                        .HasColumnType("varchar(25)")
                        .HasMaxLength(25)
                        .IsUnicode(false);

                    b.Property<int>("UsuarioId")
                        .HasColumnType("int");

                    b.HasKey("PerfilId");

                    b.ToTable("Usuario","seguridad");
                });

            modelBuilder.Entity("Domain.Core.Entities.Common.Distrito", b =>
                {
                    b.HasOne("Domain.Core.Entities.Common.Provincia", "Provincia")
                        .WithMany("Distritos")
                        .HasForeignKey("ProvinciaId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Domain.Core.Entities.Common.Provincia", b =>
                {
                    b.HasOne("Domain.Core.Entities.Common.Departamento", "Departamento")
                        .WithMany("Provincias")
                        .HasForeignKey("DepartamentoId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Domain.Core.Entities.Security.Usuario", b =>
                {
                    b.HasOne("Domain.Core.Entities.Security.Perfil", "Perfil")
                        .WithMany("Usuario")
                        .HasForeignKey("PerfilId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });
#pragma warning restore 612, 618
        }
    }
}
