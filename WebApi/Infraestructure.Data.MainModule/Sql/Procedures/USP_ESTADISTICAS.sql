﻿CREATE PROCEDURE covid.USP_ESTADISTICAS
(
	@PI_FECHA DATE,
	@PI_DEPARTAMENTO_ID INT = NULL,
	@PI_PROVINCIA_ID INT = NULL,
	@PI_DISTRITO_ID INT = NULL
)
AS
BEGIN
	;WITH ACTUAL AS (
		SELECT
			EST.EstadoId,
			EST.Nombre,
			COUNT(C.EstadoId) AS Cantidad
		FROM COVID.Ciudadano C
		INNER JOIN covid.Direccion DIR ON DIR.CiudadanoId = C.CiudadanoId
		INNER JOIN common.Distrito DIS ON DIS.DistritoId = DIR.DistritoId
		INNER JOIN common.Provincia PROV ON PROV.ProvinciaId = DIS.ProvinciaId
		INNER JOIN common.Departamento DEP ON DEP.DepartamentoId = PROV.DepartamentoId
		INNER JOIN covid.Estado EST ON EST.EstadoId = C.EstadoId
		WHERE
			CAST(C.Audit_FechaCreacion AS DATE) <= @PI_FECHA AND
			(@PI_DEPARTAMENTO_ID IS NULL OR (DEP.DepartamentoId = @PI_DEPARTAMENTO_ID)) AND
			(@PI_PROVINCIA_ID IS NULL OR (PROV.ProvinciaId = @PI_PROVINCIA_ID)) AND
			(@PI_DISTRITO_ID IS NULL OR (DIS.DistritoId = @PI_DISTRITO_ID))
		GROUP BY EST.Estadoid, EST.Nombre
	),

	ANTERIOR AS (
		SELECT
			EST.EstadoId,
			EST.Nombre,
			COUNT(C.EstadoId) AS Cantidad
		FROM COVID.Ciudadano C
		INNER JOIN covid.Direccion DIR ON DIR.CiudadanoId = C.CiudadanoId
		INNER JOIN common.Distrito DIS ON DIS.DistritoId = DIR.DistritoId
		INNER JOIN common.Provincia PROV ON PROV.ProvinciaId = DIS.ProvinciaId
		INNER JOIN common.Departamento DEP ON DEP.DepartamentoId = PROV.DepartamentoId
		INNER JOIN covid.Estado EST ON EST.EstadoId = C.EstadoId
		WHERE
			CAST(C.Audit_FechaCreacion AS DATE) <= DATEADD(DAY, -1, @PI_FECHA) AND
			(@PI_DEPARTAMENTO_ID IS NULL OR (DEP.DepartamentoId = @PI_DEPARTAMENTO_ID)) AND
			(@PI_PROVINCIA_ID IS NULL OR (PROV.ProvinciaId = @PI_PROVINCIA_ID)) AND
			(@PI_DISTRITO_ID IS NULL OR (DIS.DistritoId = @PI_DISTRITO_ID))
		GROUP BY EST.Estadoid, EST.Nombre
	)
	
	SELECT
		COALESCE(
		(SELECT
			A.Cantidad - B.Cantidad
		FROM ACTUAL A
		INNER JOIN ANTERIOR B ON A.EstadoId = B.EstadoId
		WHERE
			A.EstadoId = 3), 0) As Nuevos,

		COALESCE(
		(SELECT 
			A.Cantidad
		FROM ACTUAL A
		WHERE
			A.EstadoId = 3), 0) As Confirmados,

		COALESCE(
		(SELECT 
			A.Cantidad
		FROM ACTUAL A
		WHERE
			A.EstadoId = 5), 0) As Recuperados,

		COALESCE(
		(SELECT 
			A.Cantidad
		FROM ACTUAL A
		WHERE
			A.EstadoId = 6), 0) As Fallecidos
END