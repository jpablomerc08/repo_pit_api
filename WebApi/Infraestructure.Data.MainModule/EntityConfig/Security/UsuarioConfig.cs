﻿using Domain.Core.Entities.Security;
using Infraestructure.CrossCutting.Utilities.Constantes;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infraestructure.Data.MainModule.EntityConfig.Security
{
    public class UsuarioConfig : IEntityTypeConfiguration<Usuario>
    {
        public void Configure(EntityTypeBuilder<Usuario> entity)
        {
            entity.ToTable("Usuario", Schemas.SEGURIDAD);

            entity.HasKey(e => e.PerfilId);

            entity.Property(e => e.Nombre)
                .HasMaxLength(50)
                .IsRequired();

            entity.Property(e => e.ApellidoPaterno)
                .HasMaxLength(50)
                .IsRequired();

            entity.Property(e => e.ApellidoMaterno)
                .HasMaxLength(50);

            entity.Property(e => e.Email)
                .HasMaxLength(100)
                .IsRequired();

            entity.Property(e => e.Telefono)
                .HasMaxLength(30)
                .IsRequired();

            entity.Property(e => e.Username)
                .HasMaxLength(25)
                .IsRequired();

            entity.Property(e => e.Password)
                .HasMaxLength(512)
                .IsRequired();

            entity.HasOne(d => d.Perfil)
                .WithMany(p => p.Usuario)
                .HasForeignKey(d => d.PerfilId);
        }
    }
}
