﻿using Domain.Core.Entities.Covid19;
using Infraestructure.CrossCutting.Utilities.Constantes;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infraestructure.Data.MainModule.EntityConfig.Covid19
{
    public class CiudadanoConfig : IEntityTypeConfiguration<Ciudadano>
    {
        public void Configure(EntityTypeBuilder<Ciudadano> entity)
        {
            entity.ToTable("Ciudadano", Schemas.COVID);

            entity.HasKey(t => t.CiudadanoId);

            entity.Property(t => t.ApellidoPaterno)
                .HasMaxLength(50)
                .IsRequired();

            entity.Property(t => t.ApellidoPaterno)
                .HasMaxLength(50)
                .IsRequired();

            entity.Property(t => t.ApellidoMaterno)
                .HasMaxLength(50);

            entity.Property(t => t.Nombre)
                .HasMaxLength(50)
                .IsRequired();

            entity.Property(t => t.Sexo)
                .IsRequired();

            entity.HasOne(t => t.Nacionalidad)
                .WithMany(t => t.Ciudadanos)
                .HasForeignKey(t => t.NacionalidadId)
                .OnDelete(DeleteBehavior.NoAction);

            entity.HasOne(t => t.TipoDocumentoIdentidad)
                .WithMany(t => t.Ciudadanos)
                .HasForeignKey(t => t.TipoDocumentoIdentidadId)
                .OnDelete(DeleteBehavior.NoAction);

            entity.Property(t => t.NroDocumentoIdentidad)
                .HasMaxLength(12)
                .IsRequired();

            entity.Property(t => t.Celular)
                .HasMaxLength(9)
                .IsRequired();

            entity.Property(t => t.Correo)
                .HasMaxLength(100);

            entity.HasOne(t => t.Estado)
                .WithMany(t => t.Estado_Ciudadanos)
                .HasForeignKey(t => t.EstadoId)
                .OnDelete(DeleteBehavior.NoAction);
            
            entity.HasIndex(t => t.NacionalidadId)
                .HasName("IX_Ciudadano_NacionalidadId")
                .IsClustered(false);

            entity.HasIndex(t => t.TipoDocumentoIdentidadId)
                .HasName("IX_Ciudadano_TipoDocumentoIdentidadId")
                .IsClustered(false);

            entity.HasIndex(t => new { t.TipoDocumentoIdentidadId, t.NroDocumentoIdentidad })
                .HasName("IX_Ciudadano_TipoDocumentoIdentidadId_NroDocumentoIdentidad")
                .IsUnique()
                .IsClustered(false);
        }
    }
}
