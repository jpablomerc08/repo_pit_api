﻿using Domain.Core.Entities.Covid19;
using Infraestructure.CrossCutting.Utilities.Constantes;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infraestructure.Data.MainModule.EntityConfig.Covid19
{
    public class SituacionRiesgoConfig : IEntityTypeConfiguration<SituacionRiesgo>
    {
        public void Configure(EntityTypeBuilder<SituacionRiesgo> builder)
        {
            builder.ToTable("SituacionRiesgo", Schemas.COVID);

            builder.HasKey(t => new { t.SituacionRiesgoId, t.CiudadanoId });

            builder.HasOne(t => t.Ciudadano)
                .WithMany(t => t.SituacionesRiesgo)
                .HasForeignKey(t => t.CiudadanoId)
                .OnDelete(DeleteBehavior.NoAction);
        }
    }
}
