﻿using Domain.Core.Entities.Common;
using Infraestructure.CrossCutting.Utilities.Constantes;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infraestructure.Data.MainModule.EntityConfig.Common
{
    public class TipoDocumentoIdentidadConfig : IEntityTypeConfiguration<TipoDocumentoIdentidad>
    {
        public void Configure(EntityTypeBuilder<TipoDocumentoIdentidad> entity)
        {
            entity.ToTable("TipoDocumento", Schemas.COMMON);

            entity.HasKey(t => t.TipoDocumentoIdentidadId);

            entity.Property(t => t.DescripcionLarga)
                .HasMaxLength(50)
                .IsRequired();

            entity.Property(t => t.DescripcionCorta)
                .HasMaxLength(20)
                .IsRequired();
        }
    }
}
