﻿using Domain.Core.Entities.Common;
using Infraestructure.CrossCutting.Utilities.Constantes;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infraestructure.Data.MainModule.EntityConfig.Common
{
    public class PaisConfig : IEntityTypeConfiguration<Pais>
    {
        public void Configure(EntityTypeBuilder<Pais> entity)
        {
            entity.ToTable("Pais", Schemas.COMMON);

            entity.HasKey(t => t.PaisId);

            entity.Property(t => t.CodigoISO)
                .HasMaxLength(2)
                .HasColumnType("char(2)")
                .IsRequired();

            entity.Property(t => t.Nombre)
                .HasMaxLength(100)
                .IsRequired();

            entity.HasIndex(t => t.CodigoISO)
                .HasName("IX_Pais_CodigoISO")
                .IsUnique()
                .IsClustered(false);
        }
    }
}
