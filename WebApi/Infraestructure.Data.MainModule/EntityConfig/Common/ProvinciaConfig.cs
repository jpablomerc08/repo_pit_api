﻿using Domain.Core.Entities.Common;
using Infraestructure.CrossCutting.Utilities.Constantes;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infraestructure.Data.MainModule.EntityConfig.Common
{
    public class ProvinciaConfig : IEntityTypeConfiguration<Provincia>
    {
        public void Configure(EntityTypeBuilder<Provincia> entity)
        {
            entity.ToTable("Provincia", Schemas.COMMON);

            entity.HasKey(t => t.ProvinciaId);

            entity.Property(t => t.Ubigeo)
                .HasMaxLength(4)
                .HasColumnType("char(4)")
                .IsRequired();

            entity.Property(t => t.Nombre)
                .HasMaxLength(100)
                .IsRequired();

            entity.HasOne(t => t.Departamento)
                .WithMany(t => t.Provincias)
                .HasForeignKey(t => t.DepartamentoId);

            entity.HasIndex(t => t.Ubigeo)
                .HasName("IX_Provincia_Ubigeo")
                .IsUnique()
                .IsClustered(false);
        }
    }
}
