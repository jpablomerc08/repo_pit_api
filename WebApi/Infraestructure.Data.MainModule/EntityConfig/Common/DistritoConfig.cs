﻿using Domain.Core.Entities.Common;
using Infraestructure.CrossCutting.Utilities.Constantes;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infraestructure.Data.MainModule.EntityConfig.Common
{
    public class DistritoConfig : IEntityTypeConfiguration<Distrito>
    {
        public void Configure(EntityTypeBuilder<Distrito> entity)
        {
            entity.ToTable("Distrito", Schemas.COMMON);

            entity.HasKey(t => t.DistritoId);

            entity.Property(t => t.Ubigeo)
                .HasMaxLength(6)
                .HasColumnType("char(6)")
                .IsRequired();

            entity.Property(t => t.Nombre)
                .HasMaxLength(100)
                .IsRequired();

            entity.HasOne(t => t.Provincia)
                .WithMany(t => t.Distritos)
                .HasForeignKey(t => t.ProvinciaId);

            entity.HasIndex(t => t.Ubigeo)
                .HasName("IX_Distrito_Ubigeo")
                .IsUnique()
                .IsClustered(false);
        }
    }
}
