﻿using Application.Core.Services;
using Application.MainModule.Contracts.Services.Covid19;
using Domain.Core.Entities.Covid19;
using Domain.MainModule.Contracts.Services.Covid19;

namespace Application.Services.Covid19
{
    public class SintomaAppService : GenericAppService<Sintoma>, ISintomaAppService
    {
        public SintomaAppService(ISintomaService sintomaService) : base(sintomaService)
        {
        }
    }
}
