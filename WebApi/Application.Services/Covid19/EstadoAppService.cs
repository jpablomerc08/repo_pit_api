﻿using Application.Core.Services;
using Application.MainModule.Contracts.Services.Covid19;
using Domain.Core.Entities.Covid19;
using Domain.MainModule.Contracts.Services.Covid19;

namespace Application.Services.Covid19
{
    public class EstadoAppService : GenericAppService<Estado>, IEstadoAppService
    {
        public EstadoAppService(IEstadoService estadoService) : base(estadoService)
        {
        }
    }
}
