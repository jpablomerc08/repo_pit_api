﻿using Application.Core.Services;
using Application.MainModule.Contracts.Services.Covid19;
using Domain.Core.Entities.Covid19;
using Domain.MainModule.Contracts.Services.Covid19;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Application.Services.Covid19
{
    public class ContactoAppService : GenericAppService<Contacto>, IContactoAppService
    {
        private readonly IContactoService contactoService;

        public ContactoAppService(IContactoService contactoService) : base(contactoService)
        {
            this.contactoService = contactoService;
        }

        public async Task<List<Contacto>> ListarPorCiudadanoAsyc(int CiudadanoId)
        {
            return await contactoService.ListarPorCiudadanoAsyc(CiudadanoId);
        }
    }
}
