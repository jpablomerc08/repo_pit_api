﻿using Application.Core.Services;
using Application.MainModule.Contracts.Services.Security;
using Domain.Core.Entities.Security;
using Domain.MainModule.Contracts.Services.Security;

namespace Application.Services.Security
{
    public class PerfilAppService : GenericAppService<Perfil>, IPerfilAppService
    {
        public PerfilAppService(IPerfilService perfilSevice) : base(perfilSevice)
        {
        }
    }
}
