﻿using Application.Core.Services;
using Application.MainModule.Contracts.Services.Security;
using Domain.Core.Entities.Security;
using Domain.MainModule.Contracts.Services.Security;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Application.Services.Security
{
    public class UsuarioAppService : GenericAppService<Usuario>, IUsuarioAppService
    {
        private readonly IUsuarioService usuarioService;

        public UsuarioAppService(IUsuarioService usuarioService) : base(usuarioService)
        {
            this.usuarioService = usuarioService;
        }

        public async Task<IEnumerable<Usuario>> ListAllWithProfileAsync()
        {
            return await usuarioService.ListAllWithProfileAsync();
        }

        public async Task<Usuario> FindWithProfileAsync(int Id)
        {
            return await usuarioService.FindWithProfileAsync(Id);
        }

        public async Task<int> ChangePasswordAsync(Usuario usuario)
        {
            return await usuarioService.ChangePasswordAsync(usuario);
        }

        public async Task ChangeProfileAsync(Usuario usuario)
        {
            await usuarioService.ChangeProfileAsync(usuario);
        }

        public async Task<(bool resultado, Usuario usuario)> CheckPasswordAsync(Usuario usuario)
        {
            return await usuarioService.CheckPasswordAsync(usuario);
        }
    }
}
