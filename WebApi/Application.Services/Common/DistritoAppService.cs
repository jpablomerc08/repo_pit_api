﻿using Application.Core.Services;
using Application.MainModule.Contracts.Services.Common;
using Domain.Core.Entities.Common;
using Domain.MainModule.Contracts.Services.Common;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Application.Services.Common
{
    public class DistritoAppService : GenericAppService<Distrito>, IDistritoAppService
    {
        private readonly IDistritoService distritoService;

        public DistritoAppService(IDistritoService distritoService) : base(distritoService)
        {
            this.distritoService = distritoService;
        }

        public async Task<List<Distrito>> ListarPorProvincia(int ProvinciaId)
        {
            return await distritoService.ListarPorProvincia(ProvinciaId);
        }
    }
}
