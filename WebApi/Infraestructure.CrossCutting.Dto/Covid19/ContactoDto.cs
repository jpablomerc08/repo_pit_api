﻿namespace Infraestructure.CrossCutting.Dto.Covid19
{
    public class ContactoDto
    {
        public int ContactoId { get; set; }
        public int CiudadanoId { get; set; }
        public string NombreContacto { get; set; }
        public string Parentesco { get; set; }
        public string Telefono { get; set; }
        public string Correo { get; set; }
    }
}
