﻿namespace Infraestructure.CrossCutting.Dto.Covid19
{
    public class EstadoDto
    {
        public int Estadoid { get; set; }

        public string Nombre { get; set; }

        public string Descripcion { get; set; }
    }
}
