﻿namespace Infraestructure.CrossCutting.Dto.Covid19
{
    public class SintomaDto
    {
        public int SintomaId { get; set; }

        public int CiudadanoId { get; set; }
    }
}
