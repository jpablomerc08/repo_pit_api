﻿namespace Infraestructure.CrossCutting.Dto.Covid19
{
    public class DireccionDto
    {
        public int DireccionId { get; set; }
        public int DistritoId { get; set; }
        public int ProvinciaId { get; set; }
        public int DepartamentoId { get; set; }
        public string Descripcion { get; set; }
        public double Latitud { get; set; }
        public double Longitud { get; set; }
        public int CiudadanoId { get; set; }
    }
}
