﻿using System;

namespace Infraestructure.CrossCutting.Dto.Covid19.Criteria
{
    public class EstadisticaCriteria
    {
        public DateTime Fecha { get; set; }

        public int? DepartamentoId { get; set; }

        public int? ProvinciaId { get; set; }

        public int? DistritoId { get; set; }
    }
}
