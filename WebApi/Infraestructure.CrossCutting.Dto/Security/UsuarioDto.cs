﻿namespace Infraestructure.CrossCutting.Dto.Security
{
    public class UsuarioDto
    {
        public int UsuarioId { get; set; }

        public string Nombre { get; set; }

        public string Apellidos { get; set; }

        public string Email { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }

        public int PerfilId { get; set; }

        // Mapper
        public string Perfil { get; set; }
    }
}
