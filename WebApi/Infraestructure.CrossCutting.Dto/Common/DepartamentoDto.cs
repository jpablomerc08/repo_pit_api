﻿using System.ComponentModel.DataAnnotations;

namespace Infraestructure.CrossCutting.Dto.Common
{
    public class DepartamentoDto
    {
        public int DepartamentoId { get; set; }

        [Required(ErrorMessage = "El campo \'{0}\' es requerido")]
        [MaxLength(length: 2, ErrorMessage = "\'{0}\' debe tener como máximo {1} caracteres")]
        public string Ubigeo { get; set; }

        [Required(ErrorMessage = "El campo \'{0}\' es requerido")]
        [MaxLength(length: 50, ErrorMessage = "\'{0}\' debe tener como máximo {1} caracteres")]
        public string Nombre { get; set; }
    }
}
