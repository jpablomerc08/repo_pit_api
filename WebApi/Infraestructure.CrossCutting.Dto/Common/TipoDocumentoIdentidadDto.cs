﻿using System.ComponentModel.DataAnnotations;

namespace Infraestructure.CrossCutting.Dto.Common
{
    public class TipoDocumentoIdentidadDto
    {
        public int TipoDocumentoIdentidadId { get; set; }

        [Required(ErrorMessage = "El campo \'{0}\' es requerido")]
        [MaxLength(length: 50, ErrorMessage = "\'{0}\' debe tener como máximo {1} caracteres")]
        public string DescripcionLarga { get; set; }

        [Required(ErrorMessage = "El campo \'{0}\' es requerido")]
        [MaxLength(length: 20, ErrorMessage = "\'{0}\' debe tener como máximo {1} caracteres")]
        public string DescripcionCorta { get; set; }
    }
}
