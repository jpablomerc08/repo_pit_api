﻿using Application.Core.Contracs;
using Domain.Core.Entities.Security;

namespace Application.MainModule.Contracts.Services.Security
{
    public interface IPerfilAppService : IGenericAppService<Perfil>
    {
    }
}
