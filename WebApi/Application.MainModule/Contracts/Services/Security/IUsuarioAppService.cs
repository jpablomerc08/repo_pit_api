﻿using Application.Core.Contracs;
using Domain.Core.Entities.Security;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Application.MainModule.Contracts.Services.Security
{
    public interface IUsuarioAppService : IGenericAppService<Usuario>
    {
        Task<IEnumerable<Usuario>> ListAllWithProfileAsync();

        Task<Usuario> FindWithProfileAsync(int Id);

        Task<int> ChangePasswordAsync(Usuario usuario);

        Task ChangeProfileAsync(Usuario usuario);

        Task<(bool resultado, Usuario usuario)> CheckPasswordAsync(Usuario usuario);
    }
}
