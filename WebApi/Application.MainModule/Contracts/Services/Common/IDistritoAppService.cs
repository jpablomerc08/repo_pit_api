﻿using Application.Core.Contracs;
using Domain.Core.Entities.Common;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Application.MainModule.Contracts.Services.Common
{
    public interface IDistritoAppService : IGenericAppService<Distrito>
    {
        Task<List<Distrito>> ListarPorProvincia(int ProvinciaId);
    }
}
