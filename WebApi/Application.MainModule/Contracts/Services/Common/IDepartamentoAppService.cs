﻿using Application.Core.Contracs;
using Domain.Core.Entities.Common;

namespace Application.MainModule.Contracts.Services.Common
{
    public interface IDepartamentoAppService : IGenericAppService<Departamento>
    {
    }
}
