﻿namespace Infraestructure.CrossCutting.Encryptor
{
    public class DefaultEncryptor
    {
        public static string Encriptar(string plainText)
        {
            return AesEncryptor.Encriptar(plainText);
        }

        public static string Desencriptar(string encryptext)
        {
            return AesEncryptor.Desencriptar(encryptext);
        }
    }
}
