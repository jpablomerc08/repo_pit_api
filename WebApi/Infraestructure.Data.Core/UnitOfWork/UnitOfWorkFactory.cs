﻿using Domain.Core.Contracts.UnitOfWork;
using Infraestructure.Data.Core.Contracts;

namespace Infraestructure.Data.Core.UnitOfWork
{
    public class UnitOfWorkFactory : IUnitOfWorkFactory
    {
        private IDatabaseContext databaseContext;

        public UnitOfWorkFactory(IDatabaseContext databaseContext)
        {
            this.databaseContext = databaseContext;
        }

        public virtual IUnitOfWork Create()
        {
            return new UnitOfWork(databaseContext);
        }
    }
}
