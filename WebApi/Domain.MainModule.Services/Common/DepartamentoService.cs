﻿using Domain.Core.Contracts.UnitOfWork;
using Domain.Core.Entities.Common;
using Domain.Core.Services;
using Domain.MainModule.Contracts.Repositories.Common;
using Domain.MainModule.Contracts.Services.Common;

namespace Domain.MainModule.Services.Common
{
    public class DepartamentoService : GenericService<Departamento>, IDepartamentoService
    {
        public DepartamentoService(IUnitOfWorkFactory unitOfWorkFactory,
                                   IDepartamentoRepository departamentoRepository) : base(unitOfWorkFactory, departamentoRepository)
        {
        }
    }
}
