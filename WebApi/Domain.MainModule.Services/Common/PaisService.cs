﻿using Domain.Core.Contracts.UnitOfWork;
using Domain.Core.Entities.Common;
using Domain.Core.Services;
using Domain.MainModule.Contracts.Repositories.Common;
using Domain.MainModule.Contracts.Services.Common;

namespace Domain.MainModule.Services.Common
{
    public class PaisService : GenericService<Pais>, IPaisService
    {
        public PaisService(IUnitOfWorkFactory unitOfWorkFactory,
                           IPaisRepository paisRepository) : base(unitOfWorkFactory, paisRepository)
        {
        }
    }
}
