﻿using Domain.Core.Contracts.UnitOfWork;
using Domain.Core.Entities.Covid19;
using Domain.Core.Services;
using Domain.MainModule.Contracts.Repositories.Covid19;
using Domain.MainModule.Contracts.Services.Covid19;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Domain.MainModule.Services.Covid19
{
    public class ContactoService : GenericService<Contacto>, IContactoService
    {
        private readonly IContactoRepository contactoRepository;

        public ContactoService(IUnitOfWorkFactory unitOfWorkFactory,
                               IContactoRepository contactoRepository) : base(unitOfWorkFactory, contactoRepository)
        {
            this.contactoRepository = contactoRepository;
        }

        public async Task<List<Contacto>> ListarPorCiudadanoAsyc(int CiudadanoId)
        {
            return await contactoRepository.ListarPorCiudadanoAsyc(CiudadanoId);
        }
    }
}
