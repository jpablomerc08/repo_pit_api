﻿using Domain.Core.Contracts.UnitOfWork;
using Domain.Core.Entities.Covid19;
using Domain.Core.Services;
using Domain.MainModule.Contracts.Repositories.Covid19;
using Domain.MainModule.Contracts.Services.Covid19;
using NetTopologySuite.Geometries;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Domain.MainModule.Services.Covid19
{
    public class DireccionService : GenericService<Direccion>, IDireccionService
    {
        private readonly IDireccionRepository direccionRepository;

        public DireccionService(IUnitOfWorkFactory unitOfWorkFactory, 
                                IDireccionRepository direccionRepository) : base(unitOfWorkFactory, direccionRepository)
        {
            this.direccionRepository = direccionRepository;
        }

        public async Task<List<Direccion>> ListarPorCiudadanoAsync(int CiudadanoId)
        {
            return await direccionRepository.ListarPorCiudadanoAsync(CiudadanoId);
        }

        public async Task<List<Direccion>> ConfirmadosAsync(Point ubicacion)
        {
            return await direccionRepository.ConfirmadosAsync(ubicacion);
        }
    }
}
