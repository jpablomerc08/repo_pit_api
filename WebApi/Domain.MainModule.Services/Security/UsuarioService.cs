﻿using Domain.Core.Contracts.UnitOfWork;
using Domain.Core.Entities.Security;
using Domain.Core.Services;
using Domain.MainModule.Contracts.Repositories.Security;
using Domain.MainModule.Contracts.Services.Security;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Domain.MainModule.Services.Security
{
    public class UsuarioService : GenericService<Usuario>, IUsuarioService
    {
        private readonly IUnitOfWorkFactory unitOfWorkFactory;
        private readonly IUsuarioRepository usuarioRepository;

        public UsuarioService(IUnitOfWorkFactory unitOfWorkFactory, IUsuarioRepository usuarioRepository) : base(unitOfWorkFactory, usuarioRepository)
        {
            this.unitOfWorkFactory = unitOfWorkFactory;
            this.usuarioRepository = usuarioRepository;
        }

        public async Task<IEnumerable<Usuario>> ListAllWithProfileAsync()
        {
            return await usuarioRepository.ListAllWithProfileAsync();
        }

        public async Task<Usuario> FindWithProfileAsync(int Id)
        {
            return await usuarioRepository.FindWithProfileAsync(Id);
        }

        public async Task<int> ChangePasswordAsync(Usuario usuario)
        {
            using (var uow = unitOfWorkFactory.Create())
            {
                await usuarioRepository.ChangePasswordAsync(usuario);

                return await uow.CommitAsync();
            }
        }

        public async Task ChangeProfileAsync(Usuario usuario)
        {
            await usuarioRepository.ChangeProfileAsync(usuario);
        }

        public async Task<(bool resultado, Usuario usuario)> CheckPasswordAsync(Usuario usuario)
        {
            return await usuarioRepository.CheckPasswordAsync(usuario);
        }
    }
}
