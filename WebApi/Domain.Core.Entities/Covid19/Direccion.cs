﻿using Domain.Core.Entities.Common;
using NetTopologySuite.Geometries;

namespace Domain.Core.Entities.Covid19
{
    public class Direccion
    {
        public int DireccionId { get; set; }
        public int DistritoId { get; set; }
        public string Descripcion { get; set; }
        public int CiudadanoId { get; set; }
        public Point Ubicacion { get; set; }
        public bool Eliminado { get; set; }

        public virtual Distrito Distrito { get; set; }
        public Ciudadano Ciudadano { get; set; }
    }
}
