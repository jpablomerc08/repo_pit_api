﻿using Domain.Core.Entities.Base;
using Domain.Core.Entities.Common;
using System;
using System.Collections.Generic;

namespace Domain.Core.Entities.Covid19
{
    public class Ciudadano : ENBase
    {
        public int CiudadanoId { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
        public string Nombre { get; set; }
        public bool Sexo { get; set; }
        public int NacionalidadId { get; set; }
        public int TipoDocumentoIdentidadId { get; set; }
        public string NroDocumentoIdentidad { get; set; }
        public string Celular { get; set; }
        public string Correo { get; set; }
        public DateTime FechaNacimiento { get; set; }
        public int EstadoId { get; set; }

        public Pais Nacionalidad { get; set; }
        public TipoDocumentoIdentidad TipoDocumentoIdentidad { get; set; }
        public Estado Estado { get; set; }

        public ICollection<Contacto> Contactos { get; set; }

        public ICollection<Direccion> Direcciones { get; set; }

        public ICollection<Sintoma> Sintomas { get; set; }

        public ICollection<SituacionRiesgo> SituacionesRiesgo { get; set; }
    }
}
