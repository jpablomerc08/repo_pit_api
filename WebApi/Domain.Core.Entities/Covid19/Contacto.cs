﻿namespace Domain.Core.Entities.Covid19
{
    public class Contacto
    {
        public int ContactoId { get; set; }
        public int CiudadanoId { get; set; }
        public string NombreContacto { get; set; }
        public string Parentesco { get; set; }
        public string Telefono { get; set; }
        public string Correo { get; set; }
        public bool Eliminado { get; set; }

        public Ciudadano Ciudadano { get; set; }
    }
}
