﻿using System;

namespace Domain.Core.Entities.Base
{
    public class ENBase
    {
        public bool Eliminado { get; set; }
        public string Audit_UsuarioCreacion { get; set; }
        public DateTime Audit_FechaCreacion { get; set; }
        public string Audit_UsuarioActualizacion { get; set; }
        public DateTime? Audit_FechaActualizacion { get; set; }

        // Información sensible encriptada (campo ignorado)
        //public string Encrypt { get; set; }
    }
}
