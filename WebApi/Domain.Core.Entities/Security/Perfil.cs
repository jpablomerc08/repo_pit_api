﻿using Domain.Core.Entities.Base;
using System.Collections.Generic;

namespace Domain.Core.Entities.Security
{
    public class Perfil : ENBase
    {
        public Perfil()
        {
            Usuario = new HashSet<Usuario>();
        }

        public int PerfilId { get; set; }
        public string Nombre { get; set; }

        public ICollection<Usuario> Usuario { get; set; }
    }
}
