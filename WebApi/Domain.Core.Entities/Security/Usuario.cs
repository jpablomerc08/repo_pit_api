﻿using Domain.Core.Entities.Base;
using Infraestructure.CrossCutting.Utilities.Enumerations;

namespace Domain.Core.Entities.Security
{
    public class Usuario : ENBase
    {
        public int UsuarioId { get; set; }
        public string Nombre { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
        public string Email { get; set; }
        public string Telefono { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public EstadoUsuarioEnum Estado { get; set; }
        public int PerfilId { get; set; }

        public Perfil Perfil { get; set; }
    }
}
