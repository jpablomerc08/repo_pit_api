﻿using Domain.Core.Entities.Covid19;
using System.Collections.Generic;

namespace Domain.Core.Entities.Common
{
    public class TipoDocumentoIdentidad
    {
        public int TipoDocumentoIdentidadId { get; set; }
        public string DescripcionLarga { get; set; }
        public string DescripcionCorta { get; set; }
        public bool Eliminado { get; set; }

        public ICollection<Ciudadano> Ciudadanos { get; set; }
    }
}
