﻿using Domain.Core.Entities.Covid19;
using System.Collections.Generic;

namespace Domain.Core.Entities.Common
{
    public class Pais
    {
        public int PaisId { get; set; }
        public string CodigoISO { get; set; }
        public string Nombre { get; set; }
        public bool Eliminado { get; set; }

        public ICollection<Ciudadano> Ciudadanos { get; set; }
    }
}
